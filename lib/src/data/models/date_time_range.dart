class DateTimeRange {
  DateTime _start;
  DateTime _end;
  String _title;
  String _description;

  DateTimeRange(this._title, this._description, this._start, this._end);

  DateTime get start => _start;
  DateTime get end => _end;
  String get title => _title;
  String get description => _description;

  set title(String value) {
    _title = value;
  }

  set start(DateTime value) {
    _start = value;
  }

  set end(DateTime value) {
    _end = value;
  }

  set description(String value) {
    _description = value;
  }
}
