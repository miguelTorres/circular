

//Esta clase es usada para traer el resultado de los eventos en  calendar_page.dart


class ResultNotificationItem {


  int _eventUid;
  int _campusUid;
  int _areaUid;
  int _typeUid;
  bool _hasSpecials;

  ResultNotificationItem(){
    _eventUid = null;
    _campusUid = null;
    _areaUid = null;
    _typeUid = null;
    _hasSpecials = false;
  }

  bool get hasSpecials => _hasSpecials;

  set hasSpecials(bool value) {
    _hasSpecials = value;
  }

  int get typeUid => _typeUid;

  set typeUid(int value) {
    _typeUid = value;
  }

  int get areaUid => _areaUid;

  set areaUid(int value) {
    _areaUid = value;
  }

  int get campusUid => _campusUid;

  set campusUid(int value) {
    _campusUid = value;
  }

  int get eventUid => _eventUid;

  set eventUid(int value) {
    _eventUid = value;
  }
}