


class OrganizerModel{

  String _firstName;
  String _lastName;
  String _position;
  String _institutionalEmail;
  String _phone;
  String _extension;
  String _dependenceName;




  String get dependenceName => _dependenceName;


  set dependenceName(String value) {
    _dependenceName = value;
  }

  String get extension => _extension;

  set extension(String value) {
    _extension = value;
  }

  int _uidDependence;

  String get firstName => _firstName;

  set firstName(String value) {
    _firstName = value;
  }

  String get lastName => _lastName;

  int get uidDependence => _uidDependence;

  set uidDependence(int value) {
    _uidDependence = value;
  }

  String get phone => _phone;

  set phone(String value) {
    _phone = value;
  }

  String get institutionalEmail => _institutionalEmail;

  set institutionalEmail(String value) {
    _institutionalEmail = value;
  }

  String get position => _position;

  set position(String value) {
    _position = value;
  }

  set lastName(String value) {
    _lastName = value;
  }


}