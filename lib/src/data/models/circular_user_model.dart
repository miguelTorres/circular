

import 'dart:convert';

class CircularUserModel{
  int _uid;
  String _name;
  String _username;
  String _type;
  String _mail;
  String _pass; // la contrasenia esta se mantendrá codificada en base64

  int get uid => _uid;
  String get name => _name;
  String get username => _username;
  String get type => _type;
  String get mail => _mail;
  String get pass => _pass;

  void set uid(int uid){_uid = uid;}
  void set name(String name){_name = name;}
  void set username(String username){_username = username;}
  void set type(String type){_type = type;}
  void set mail(String mail){_mail = mail;}
  void set pass(String pass){_pass = pass;}


  //constructor
  CircularUserModel();



  //Retorna  un CircularUser a partir de un JSON ingresado
  //Utilizado para convertir a objetos la respuesta del API de circular
  CircularUserModel.fromJson(Map<String, dynamic> parsedJson) {
    //En la respuesta del API obtenemos el nombre y el correo
    _uid =  parsedJson["uid"];
    _mail = parsedJson["mail"];
    _name = parsedJson["name"];
  }

}