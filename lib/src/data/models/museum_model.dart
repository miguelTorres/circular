import 'package:circular/src/data/models/location_model.dart';

class MuseumModel{
  int _uid;
  String _name;
  String _description;
  String _phone;
  String _address;
  String _email;
  String _officeHours;
  String _moreInformation;
  String _imageUrl;
  String _url;
  String _campus;
  String _imgBase64;
  CircularLocation _circularLocation;

  //Constructor
  MuseumModel(
    uid,
    name,
    description,
    phone,
    address,
    email,
    officeHours,
    moreInformation,
    imageUrl,
    url,
    campus,
    circularLocation
   ){
      this._uid=uid;
      this._name=name;
      this._description=description;
      this._phone=phone;
      this._address=address;
      this._email=email;
      this._officeHours=officeHours;
      this._moreInformation=moreInformation;
      this._imageUrl=imageUrl;
      this._url=url;
      this._campus=campus;
      this._circularLocation=circularLocation;
    }



  int get uid => _uid;
  String get name => _name;
  String get description => _description;
  String get phone => _phone;
  String get address => _address;
  String get email => _email;
  String get officeHours => _officeHours;
  String get moreInformation => _moreInformation;
  String get imageUrl => _imageUrl;
  String get url => _url;
  String get campus => _campus;
  String get imgBase64 => _imgBase64;
  CircularLocation get circularLocation => _circularLocation;

  void set imgBase64(String strBase64){ _imgBase64 = strBase64; }


  //Retorna  un MuseoModel a partir de un JSON ingresado
  //Utilizado en el llamado al API de circular desde los providers
  MuseumModel.fromJson(Map<String, dynamic> parsedJson) {


    _uid = parsedJson["uid"];
    _name = parsedJson["name"];
    _description = parsedJson["description"];
    _phone = parsedJson["phone"];
    _address = parsedJson["address"];
    _email = parsedJson["email"];
    _officeHours = parsedJson["officeHours"];
    _moreInformation = parsedJson["moreInformation"];
    _imageUrl = parsedJson["image"];
    _url = parsedJson["url"];
    _campus = parsedJson["campus"];

    if (parsedJson["location"] != null) {
      Map<String, dynamic> locationJson = parsedJson["location"];

      _circularLocation = new CircularLocation(
          locationJson["uid"],
          locationJson["longitude"],
          locationJson["latitude"],
          locationJson["address"]
      );
    }
  }

  //Retorna un mapa (estructura de datos)
  //Utilizado por el manejo de base de datos de SQflite
  Map<String, dynamic> toMap(){
    var map = Map<String, dynamic>();
    map['uid'] = _uid;
    map['name'] = _name;
    map['description'] = _description;
    map['phone'] = _phone;
    map['address'] = _address;
    map['email'] = _email;
    map['officeHours'] = _officeHours;
    map['moreInformation'] = _moreInformation;
    map['imageUrl'] = _imageUrl;
    map['url'] = _url;
    map['campus'] = _campus;
    map['imgBase64'] = _imgBase64;
    map['id_circularLocation'] = _circularLocation.uid;

    return map;
  }

  //Retorna un MuseoModel a partir de un mapa ingresado
  //Utilizado por el manejo de base de datos de SQflite
  MuseumModel.fromMapObject(Map<String, dynamic> map){

    this._uid = map["uid"];
    this._name = map["name"];
    this._description = map["description"];
    this._phone = map["phone"];
    this._address = map["address"];
    this._email = map["email"];
    this._officeHours = map["officeHours"];
    this._moreInformation = map["moreInformation"];
    this._imageUrl = map["imageUrl"];
    this._url = map["url"];
    this._campus = map["campus"];
    this._imgBase64 = map["imgBase64"];

    if (map["id_circularLocation"] != null ||
        map["latitude"] != null ||
        map["longitude"] != null ||
        map["address"] != null
    ) {

      _circularLocation = new CircularLocation(
          map["id_circularLocation"],
          map["longitude"],
          map["latitude"],
          map["address"]
      );
    }

  }



}