//Clase utilizada para el manejo de las categorias

class RadioModel {
  int _uid;
  int _topId;
  bool _isSelected;
  String _buttonText;
  String _text;
  String _type; //ALL o NOTHING o CAMPUS o AREA o TYPE o ESPECIAL o DATE
  String _campus;




  RadioModel(this._uid, this._isSelected, this._buttonText, this._text, this._type);

  int get uid {return _uid;}
  bool get isSelected {return _isSelected;}
  String get buttonText {return _buttonText;}
  String get text {return _text;}
  String get type {return _type;}
  String get campus => _campus;
  int get topId => _topId;

  set topId(int value) {
    _topId = value;
  }
  set campus(String value) {_campus = value;}
  void set uid (int uid) {_uid=uid;}
  void set isSelected (bool isSelected) {_isSelected=isSelected;}
  void set buttonText (String buttonText) {_buttonText=buttonText;}
  void set text (String text) {_text=text;}
  void set type (String type) {_type=type;}


  //Retorna  un RadioModel a partir de un JSON ingresado
  //Utilizado para convertir a objetos la respuesta del API de circular
  RadioModel.fromJson(Map<String, dynamic> parsedJson) {
    _uid =  parsedJson["uid"];
    _text =  parsedJson["title"];
    _isSelected = false;
    _buttonText = "";
    _campus = parsedJson["campus"];
    _topId = parsedJson["topId"];

  }

  @override
  String toString() {
    return
        "\n uid: $uid"
    "\n isSelected: $isSelected"
    "\n buttonText: $buttonText"
    "\n text: $text"
    "\n type: $type";
  }
}
