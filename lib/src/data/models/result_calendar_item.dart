

//Esta clase es usada para traer el resultado de los eventos en  calendar_page.dart


class ResultCalendarItem {


  int _eventUid;
  String _title;
  DateTime _initialDate;
  DateTime _currentDate;
  DateTime _finalDate;
  String _type;
  String _area;
  String _campus;

  String get campus => _campus;

  set campus(String value) {
    _campus = value;
  }

  int get eventUid => _eventUid;

  set eventUid(int value) {
    _eventUid = value;
  } //Getters y Setters

  String get title => _title;

  set title(String value) {
    _title = value;
  }

  DateTime get initialDate => _initialDate;

  set initialDate(DateTime value) {
    _initialDate = value;
  }

  String get area => _area;

  set area(String value) {
    _area = value;
  }

  String get type => _type;

  set type(String value) {
    _type = value;
  }

  DateTime get finalDate => _finalDate;

  set finalDate(DateTime value) {
    _finalDate = value;
  }

  DateTime get currentDate => _currentDate;

  set currentDate(DateTime value) {
    _currentDate = value;
  }

  ResultCalendarItem(
      this._eventUid,
      this._title,
      this._initialDate,
      this._currentDate,
      this._finalDate,
      this._type,
      this._area,
      this._campus); //Constructor


  //Retorna  un ResultCalendarItem a partir de un JSON ingresado
  //Utilizado para convertir a objeto la respuesta del API de circular
  ResultCalendarItem.fromJson(Map<String, dynamic> parsedJson) {

    _eventUid = parsedJson["id"];
    _title = parsedJson["title"];
    _initialDate = DateTime.parse(parsedJson["date"]["initial"]);
    _currentDate = DateTime.parse(parsedJson["date"]["current"]);
    _finalDate = DateTime.parse(parsedJson["date"]["final"]);
    _area = parsedJson["categories"]["areaTematica"][0]["name"];
    _type = parsedJson["categories"]["tipo"][0]["name"];
    _campus = parsedJson["campus"];

  }

  //Retorna  un ResultCalendarItem a partir de un JSON ingresado
  //Utilizado para convertir a objeto la respuesta de la base de datos local
  ResultCalendarItem.fromJsonDataBase(Map<String, dynamic> parsedJson) {

    /*
    parsedJson.forEach((key, value) {
      print(">> ${key} : ${value}");

    });*/


    _eventUid = parsedJson["uid"];
    _title = parsedJson["title"];
    _initialDate = new DateTime.fromMillisecondsSinceEpoch(parsedJson["initialDate"]);

    _currentDate = new DateTime.fromMillisecondsSinceEpoch(parsedJson["currentDate"]);
    _finalDate = new DateTime.fromMillisecondsSinceEpoch(parsedJson["finalDate"]);
    _area = "";
    _type = "";
    _campus = parsedJson["campus"];

  }

}