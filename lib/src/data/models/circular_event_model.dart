import 'dart:convert';

import 'package:circular/src/data/models/location_model.dart';
import 'package:circular/src/data/models/organizer_model.dart';

import 'date_time_range.dart';

class CircularEventModel {
  int _uid;
  int _pid;

  String _title;
  String _description;
  DateTime _initialDate;
  DateTime _currentDate;
  DateTime _finalDate;
  String _imgUrl;
  String _campus;
  String _dependenciaName;
  int _dependenciaUid;

  String _invitados;
  String _ticketsType;
  String _ticketsUrl;
  String _ticketsCost;


  DateTime _inscriptionDateInitial;
  DateTime _inscriptionDateFinal;
  String _inscriptionWay;
  String _eventUrl;

  //Categorias
  int _campusUid;
  int _areaUid;
  String _areaName;
  int _typeUid;
  String _typeName;
  int _specialUid;
  String _specialName;

  String _keywords;
  String _imgBase64;
  String _imageCredits;
  String _numInstitutionalImage;

  CircularLocation _circularLocation;
  OrganizerModel _organizer;

  List<DateTimeRange> _eventDates;

  int get uid => _uid;
  String get title => _title;
  String get description => _description;
  DateTime get initialDate => _initialDate;
  DateTime get currentDate => _currentDate;
  DateTime get finalDate => _finalDate;
  String get imgUrl => _imgUrl;
  String get campus => _campus;
  String get dependenciaName => _dependenciaName;

  String get invitados => _invitados;
  String get ticketsType => _ticketsType;
  String get ticketsUrl => _ticketsUrl;
  String get ticketsCost => _ticketsCost;

  DateTime get inscriptionDateInitial => _inscriptionDateInitial;
  DateTime get inscriptionDateFinal => _inscriptionDateFinal;
  String get inscriptionWay => _inscriptionWay;
  String get eventUrl => _eventUrl;
  String get keywords => _keywords;
  String get numInstitutionalImage => _numInstitutionalImage;
  String get imgBase64 => _imgBase64;
  CircularLocation get circularLocation => _circularLocation;

  int get campusUid => _campusUid;
  int get areaUid => _areaUid;
  int get typeUid => _typeUid;
  int get specialUid => _specialUid;

  String get imageCredits => _imageCredits;

  OrganizerModel get organizer => _organizer;

  List<DateTimeRange> get eventDates => _eventDates;

  int get dependenciaUid => _dependenciaUid;

  String get typeName => _typeName;

  String get specialName => _specialName;

  String get areaName => _areaName;
  int get pid => _pid;

  set pid(int value) {
    _pid = value;
  }
  set areaName(String value) {
    _areaName = value;
  }

  set specialName(String value) {
    _specialName = value;
  }

  set typeName(String value) {
    _typeName = value;
  }


  set dependenciaUid(int value) {
    _dependenciaUid = value;
  }

  set eventDates(List<DateTimeRange> value) {
    _eventDates = value;
  }

  set organizer(OrganizerModel value) {
    _organizer = value;
  }

  set imageCredits(String value) {
    _imageCredits = value;
  }

  set uid(int value) {
    _uid = value;
  }

  set title(String value) {
    _title = value;
  }

  set description(String value) {
    _description = value;
  }

  set initialDate(DateTime value) {
    _initialDate = value;
  }

  set currentDate(DateTime value) {
    _currentDate = value;
  }

  set finalDate(DateTime value) {
    _finalDate = value;
  }

  set imgUrl(String value) {
    _imgUrl = value;
  }

  set campus(String value) {
    _campus = value;
  }

  set dependenciaName(String value) {
    _dependenciaName = value;
  }

  set invitados(String value) {
    _invitados = value;
  }

  set ticketsType(String value) {
    _ticketsType = value;
  }

  set ticketsUrl(String value) {
    _ticketsUrl = value;
  }

  set ticketsCost(String value) {
    _ticketsCost = value;
  }

  set inscriptionDateInitial(DateTime value) {
    _inscriptionDateInitial = value;
  }

  set inscriptionDateFinal(DateTime value) {
    _inscriptionDateFinal = value;
  }

  set inscriptionWay(String value) {
    _inscriptionWay = value;
  }

  set eventUrl(String value) {
    _eventUrl = value;
  }

  set campusUid(int value) {
    _campusUid = value;
  }

  set areaUid(int value) {
    _areaUid = value;
  }

  set typeUid(int value) {
    _typeUid = value;
  }

  set specialUid(int value) {
    _specialUid = value;
  }

  set keywords(String value) {
    _keywords = value;
  }

  set circularLocation(CircularLocation value) {
    _circularLocation = value;
  }

  void set imgBase64(String strBase64) {
    _imgBase64 = strBase64;
  }

  set numInstitutionalImage(String value) {
    _numInstitutionalImage = value;
  }

  //Constructor
  CircularEventModel();

  //Retorna  un CircularEvent a partir de un JSON ingresado
  //Utilizado para convertir a objetos la respuesta del API de circular
  CircularEventModel.fromJson(Map<String, dynamic> parsedJson) {


    _uid = parsedJson["id"];
    _pid = parsedJson["pid"];
    _title = parsedJson["title"];
    _description = parsedJson["description"];
    _initialDate = DateTime.parse(parsedJson["date"]["initial"]);
    _currentDate = DateTime.parse(parsedJson["date"]["current"]);
    _finalDate = DateTime.parse(parsedJson["date"]["final"]);
    _imgUrl = parsedJson["imagen"];
    _campus = parsedJson["campus"];
    Map<String, dynamic> jsonDependences = parsedJson['dependencia'];
    _dependenciaName = jsonDependences['name'];

    _invitados =
        (parsedJson["invitados"] != null && parsedJson["invitados"] != "")
            ? parsedJson["invitados"]
            : null;
    _ticketsType = parsedJson["ticketsType"];
    _ticketsUrl = parsedJson["ticketsUrl"];

    _ticketsCost = parsedJson["ticketsCost"].toString();
    if (parsedJson["inscriptionDateInitial"] != null &&
        parsedJson["inscriptionDateInitial"] != "") {
      _inscriptionDateInitial =
          DateTime.parse(parsedJson["inscriptionDateInitial"]);
    }
    if (parsedJson["inscriptionDateFinal"] != null &&
        parsedJson["inscriptionDateInitial"] != "") {
      _inscriptionDateFinal =
          DateTime.parse(parsedJson["inscriptionDateFinal"]);
    }
    if (parsedJson["inscriptionWay"] != null &&
        parsedJson["inscriptionWay"] != "") {
      _inscriptionWay = parsedJson["inscriptionWay"];
    }
    _eventUrl = parsedJson["url"];

    if (parsedJson["campusUid"] != null && parsedJson["campusUid"] != "") {
      _campusUid = parsedJson["campusUid"];
    } else {
      _campusUid = -1;
    }

    if (parsedJson["categories"] != null && parsedJson["categories"] != "") {
      if (parsedJson["categories"]["areaTematica"] != null &&
          parsedJson["categories"]["areaTematica"] != "") {
        _areaUid = parsedJson["categories"]["areaTematica"][0]["uid"];
      } else {
        _areaUid = -1;
      }

      if (parsedJson["categories"]["tipo"] != null &&
          parsedJson["categories"]["tipo"] != "") {
        _typeUid = parsedJson["categories"]["tipo"][0]["uid"];
      } else {
        _typeUid = -1;
      }

      if (parsedJson["categories"]["especial"] != null &&
          parsedJson["categories"]["especial"] != "") {

        _specialUid = parsedJson["categories"]["especial"][0]["uid"];
      } else {
        _specialUid = -1;
      }
    }
    if (parsedJson["location"] != null) {
      Map<String, dynamic> locationJson = parsedJson["location"];

      _circularLocation = new CircularLocation(
          locationJson["uid"],
          locationJson["longitude"].toDouble(),
          locationJson["latitude"].toDouble(),
          locationJson["address"]);
    }

    _keywords = parsedJson["keywords"];
  }

  //Retorna un mapa (estructura de datos)
  //Utilizado por el manejo de base de datos de SQflite
  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();

    map['uid'] = _uid;
    map['title'] = _title;
    map['description'] = _description;
    map['initialDate'] = _initialDate.millisecondsSinceEpoch;
    map['currentDate'] = _currentDate.millisecondsSinceEpoch;
    map['finalDate'] = _finalDate.millisecondsSinceEpoch;
    map['imgUrl'] = _imgUrl;
    map['imgBase64'] = _imgBase64;
    map['campus'] = _campus;
    map['dependenciaName'] = _dependenciaName;

    map['invitados'] = _invitados;
    map['ticketsType'] = _ticketsType;
    map['ticketsUrl'] = _ticketsUrl;

    if (_inscriptionDateInitial != null) {
      map['inscriptionDateInitial'] =
          _inscriptionDateInitial.millisecondsSinceEpoch;
    } else {
      map['inscriptionDateInitial'] = 0;
    }

    if (_inscriptionDateFinal != null) {
      map['inscriptionDateFinal'] =
          _inscriptionDateFinal.millisecondsSinceEpoch;
    } else {
      map['inscriptionDateFinal'] = 0;
    }
    map['inscriptionWay'] = _inscriptionWay;
    map['eventUrl'] = _eventUrl;

    //Categorias
    map["campusUid"] = _campusUid;
    map["areaUid"] = _areaUid;
    map["typeUid"] = _typeUid;
    map["specialUid"] = _specialUid;

    map['keywords'] = _keywords;
    map['id_circularLocation'] = _circularLocation.uid;

   /* map.forEach((key, value) {
      print(">>> ${key} - ${value}");
    });*/

    return map;
  }

  //Retorna un CircularEventModel a partir de un mapa ingresado
  //Utilizado por el manejo de base de datos de SQflite
  CircularEventModel.fromMapObject(Map<String, dynamic> map) {

/*
    map.forEach((key, value) {
      print(">>> $key : $value");
    });*/


    this._uid = map["uid"];
    this._title = map["title"];
    this._description = map["description"];
    this._initialDate =
        new DateTime.fromMillisecondsSinceEpoch(map["initialDate"]);


    this._currentDate =
        new DateTime.fromMillisecondsSinceEpoch(map["currentDate"]);

    this._finalDate = new DateTime.fromMillisecondsSinceEpoch(map["finalDate"]);

    this._imgUrl = map["imgUrl"];
    this._campus = map["campus"];
    this._dependenciaName = map["dependenciaName"];

    this._invitados = map["invitados"];
    this._ticketsType = map["ticketsType"];
    this._ticketsUrl = map["ticketsUrl"];

    if (map["inscriptionDateInitial"] != null &&
        map["inscriptionDateInitial"] != 0) {
      this._inscriptionDateInitial = new DateTime.fromMillisecondsSinceEpoch(
          map["inscriptionDateInitial"]);
    } else {
      this._inscriptionDateInitial = null;
    }


    if (map["inscriptionDateFinal"] != null &&
        map["inscriptionDateFinal"] != 0) {
      this._inscriptionDateFinal =
          new DateTime.fromMillisecondsSinceEpoch(map["inscriptionDateFinal"]);
    } else {
      this._inscriptionDateFinal = null;
    }

    this._inscriptionWay = map["inscriptionWay"];
    this._eventUrl = map["eventUrl"];

    //Categorias

    this._campusUid = map["campusUid"];
    this._areaUid = map["areaUid"];
    this._typeUid = map["typeUid"];
    this._specialUid = map["specialUid"];

    this._keywords = map["keywords"];
    this._imgBase64 = map["imgBase64"];

    if (map["id_circularLocation"] != null ||
        map["latitude"] != null ||
        map["longitude"] != null ||
        map["address"] != null) {
      _circularLocation = new CircularLocation(map["id_circularLocation"],
          map["longitude"], map["latitude"], map["address"]);
    }
  }


  //Retorna un CircularEvent a partir de un mapa ingresado
  // Este objeto contiene información adicional como los rangos de fecha y
  // la informaicon del responsable del vento
  //Utilizado para la edición del evento
  CircularEventModel.fromMapToCompleteObject(Map<String, dynamic> map) {

    this._uid = map["id"];
    this._title = map["title"];
    this._description = map["description"];

    this._initialDate =
         DateTime.parse(map["date"]["initial"]);

    this._currentDate =
        DateTime.parse(map["date"]["current"]);

    this._finalDate = DateTime.parse(map["date"]["final"]);

    this._imgUrl = map["imgUrl"];
    this._campus = map["campus"];
    this.dependenciaUid = map["dependencia"]["uid"];
    this._dependenciaName = map["dependencia"]["name"];

    this._invitados = map["invitados"];
    this._ticketsType = map["ticketsType"];
    this._ticketsUrl = map["ticketsUrl"];
    _ticketsCost = map["ticketsCost"].toString();



    if (map["inscriptionDateInitial"] != null &&
        map["inscriptionDateInitial"] != "") {
      this._inscriptionDateInitial = DateTime.parse(
          map["inscriptionDateInitial"]);


    } else {
      this._inscriptionDateInitial = null;
    }

    if (map["inscriptionDateFinal"] != null &&
        map["inscriptionDateFinal"] != "") {
      this._inscriptionDateFinal = DateTime.parse(map["inscriptionDateFinal"]);
    } else {
      this._inscriptionDateFinal = null;
    }

    this._inscriptionWay = map["inscriptionWay"];
    this._eventUrl = map["eventUrl"];

    //Categorias

    this._areaUid = map["categories"]["areaTematica"][0]["uid"];
    this._areaName = map["categories"]["areaTematica"][0]["name"];
    this._typeUid = map["categories"]["tipo"][0]["uid"];
    this._typeName = map["categories"]["tipo"][0]["name"];

    if(map["categories"]["especial"] != null){
      this._specialUid = map["categories"]["especial"][0]["uid"];
      this._specialName = map["categories"]["especial"][0]["name"];

    }else{
      this._specialUid = null;
      this._specialName = "";
    }


this.numInstitutionalImage = map["institutionalImageNumber"];
    this._imgUrl = map["imagen"];

    this._keywords = map["keywords"];
    this._imageCredits = map["creditos_imagen"];

    if (map["location"] != null ||
        map["latitude"] != null ||
        map["longitude"] != null ||
        map["address"] != null) {

      _circularLocation = new CircularLocation(0,
          map["location"]["longitude"], map["location"]["latitude"], map["location"]["address"]);

    }

    List event_dates = map["event_dates"];
    if(event_dates!=null){
      this._eventDates = [];
      event_dates.forEach((element) {

        this._eventDates.add(
          new DateTimeRange(
              element["name"],
              element["description"],
              DateTime.parse(element["initial"]),
              DateTime.parse(element["final"]),
              )
        );
      });
    }

    OrganizerModel my_organizer = new OrganizerModel();

    my_organizer.firstName = map["responsable"]["nombre"];
    my_organizer.lastName = map["responsable"]["apellido"];
    my_organizer.uidDependence = map["responsable"]["dependencia"]["uid"];
    my_organizer.dependenceName = map["responsable"]["dependencia"]["name"];
    my_organizer.institutionalEmail = map["responsable"]["correo"];
    my_organizer.position = map["responsable"]["cargo"];
    my_organizer.phone = map["responsable"]["telefono"].toString();
    my_organizer.extension = map["responsable"]["extension"];

    this.organizer = my_organizer;

  }

  //Retorna un JSON para ser enviado al backend mediante el servicio POST de creacion de evento
  Map<String, dynamic> parseToJsonBackend() {
    var map = Map<String, dynamic>();
    if(uid!=null && uid > 0){
      map['eventUid'] = uid;
    }

    map["title"] = title;
    map["keywords"] = keywords;
    map["institutionalImageNumber"] = _numInstitutionalImage;
    map["guests"] = invitados;
    map["description"] = description;
    map["inscriptionWay"] = inscriptionWay;
    if(_inscriptionDateInitial!=null){
      map["initialInscriptionDate"] =
          _inscriptionDateInitial.millisecondsSinceEpoch;
    }else{
      map["initialInscriptionDate"] = "";
    }

    if(_inscriptionDateInitial != null){
      map["finalInscriptionDate"] = _inscriptionDateFinal.millisecondsSinceEpoch;
    }else{
      map["finalInscriptionDate"] = "";
    }
    map["imageDetailCredits"] = imageCredits;
    map["dependence"] = _dependenciaUid;
    map["typeCategory"] = typeUid;
    map["areaCategory"] = areaUid;
    map["especialCategory"] = specialUid;
    map["ticketsType"] = ticketsType;
    map["ticketsUrl"] = ticketsUrl;
    map["ticketsCost"] = ticketsCost;
    //TODO: estas opciones se habilitaran cuando la integraciòn con eventoken estè completa.
    //Por ahora se dejaran en 0;
    map["generateTickets"] = 0;
    map["countTickets"] = 0;

    List<dynamic> calendarElementsJson = [];

    _eventDates.forEach((element) {
      var elementJson = Map<String, dynamic>();

      elementJson["start"] = element.start.millisecondsSinceEpoch;
      elementJson["end"] = element.end.millisecondsSinceEpoch;
      elementJson["title"] = element.title;
      elementJson["description"] = element.description;

      calendarElementsJson.add(elementJson);
    });

    map["calendarElements"] = calendarElementsJson;

    var locationElementsJson = Map<String, dynamic>();

    locationElementsJson["title"] =
        (_circularLocation.address != null) ? _circularLocation.address : "";
    locationElementsJson["lat"] = _circularLocation.latitude;
    locationElementsJson["lng"] = _circularLocation.longitude;
    locationElementsJson["address"] =
        (_circularLocation.address != null) ? _circularLocation.address : "";

    map["locationElements"] = locationElementsJson;

    var responsableJson = Map<String, dynamic>();

    responsableJson["firstName"] = _organizer.firstName;
    responsableJson["lastName"] = _organizer.lastName;
    responsableJson["position"] = _organizer.position;
    responsableJson["institutionalEmail"] = _organizer.institutionalEmail;
    responsableJson["phone"] = _organizer.phone;
    responsableJson["extension"] = _organizer.extension;
    responsableJson["responsableDependence"] = _organizer.uidDependence;

    map["responsable"] = responsableJson;

    map["imageDetail"] = imgBase64;

    return map;
  }

  @override
  String toString() {



    String str = "\n_uid = $_uid"
        "\n_title = $_title"
        "\n_description = $_description"
        "\n_initialDate = $_initialDate"
        "\n_currentDate = $_currentDate"
        "\n_finalDate = $_finalDate"
        "\n_imgUrl = $_imgUrl"
        "\n_campus = $_campus"
        "\n_dependenciaName = $_dependenciaName"
        "\n_invitados = $_invitados"
        "\n_ticketsType = $_ticketsType"
        "\n_ticketsUrl = $_ticketsUrl"
        "\n_inscriptionDateInitial = $_inscriptionDateInitial"
        "\n_inscriptionDateFinal = $_inscriptionDateFinal"
        "\n_inscriptionWay = $_inscriptionWay"
        "\n_eventUrl = $_eventUrl"

        //Categorias
        "\n_campusUid = $_campusUid"
        "\n_areaUid = $_areaUid"
        "\n_typeUid = $_typeUid"
        "\n_specialUid = $_specialUid"
        "\nStr_keywords = $_keywords"

        //ubicacion
        "\n_ubicacion_address = ${circularLocation.address}"
        "\n_ubicacion_lat = ${circularLocation.latitude}"
        "\n_ubicacion_lng = ${circularLocation.longitude}";
/*
    _eventDates.forEach((element) {
      String strDate = "\n inicia:${element.start.millisecondsSinceEpoch}"
          "\n final:${element.end.millisecondsSinceEpoch}"
          "\n title:${element.title}"
          "\n description:${element.description}";
      str = str + "\n" + strDate;
    });*/
    str = "\nStr_imgBase64 = $_imgBase64" + str;


    return str;
  }


}
