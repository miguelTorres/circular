class CircularLocation{
  int _uid;
  double _longitude;
  double _latitude;
  String _address;

 //Constructor
  CircularLocation(this._uid, this._longitude, this._latitude, this._address);

  int get uid => _uid;
  double get longitude => _longitude;
  double get latitude => _latitude;
  String get address => _address;

  set latitude(double value) {
    _latitude = value;
  }

  set address(String value) {
    _address = value;
  }

  set longitude(double value) {
    _longitude = value;
  }

  //Retorna un mapa (estructura de datos)
  //Utilizado por el manejo de base de datos de SQflite
  Map<String, dynamic> toMap(){
    var map = Map<String, dynamic>();
    map['id_circularLocation'] = _uid;
    map['longitude'] = _longitude;
    map['latitude'] = _latitude;
    map['address'] = _address;
    return map;
  }

  //Retorna un MuseoModel a partir de un mapa ingresado
  //Utilizado por el manejo de base de datos de SQflite
  CircularLocation.fromMapObject(Map<String, dynamic> map){
    this._uid = map['id_circularLocation'];
    this._latitude = map['latitude'];
    this._longitude = map['longitude'];
    this._address = map['address'];

  }


}