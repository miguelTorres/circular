import 'dart:io';

import 'package:circular/src/business_logic/enc-dec.dart';
import 'package:circular/src/data/models/circular_event_model.dart';
import 'package:circular/src/data/models/circular_user_model.dart';
import 'package:circular/src/data/models/location_model.dart';
import 'package:circular/src/data/models/radio_model.dart';
import 'package:circular/src/data/models/result_calendar_item.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';

class CircularEventApiProvider {
  final _hostDomain = "dev3.unal.edu.co";
  final _hostDomainElastic = "168.176.236.22:9200";
  final _urlEventos = "/rest/eventos";
  final _urlDependences = "/rest/eventos/dependencias";
  final _urlCategories = "/rest/eventos/categorias";
  final _urlEventosShow = "/rest/eventos/show";
  final _urlEventosSearch = "/rest/eventos/search";
  final _urlEventosByUser = "/rest/usuarios/eventos";
  final _urlDeleteEvent = "/rest/usuarios/eventos";
  final _urlEventosFavoritosByUser = "/rest/usuarios/favoritos";
  final _urlDeleteFavoritoByUser = "/rest/usuarios/favorito";
  final _urlNewEvent = "/rest/usuarios/nuevoevento";
  final _urlUpdateEvent = "/rest/usuarios/editaevento";
  final _urlCalendarItemsByDay = "/rest/eventos/dia";
  final _urlElasticSearch = "/circular_unal/_search";
  final _urlFechasByEventUid = "/rest/eventos/fechas";
  final _urlEventosEdited = "/rest/eventos/edited";
  final _urlEventosDeleted = "/rest/eventos/deleted";
  final _urlEventosHidden = "/rest/eventos/hidden";
  final _urlEventosOverdue = "/rest/eventos/overdue";

  List<CircularEventModel> parseCircularEvents(String responseBody) {
    final parsed = json.decode(responseBody);

    return parsed
        .map<CircularEventModel>((json) => CircularEventModel.fromJson(json))
        .toList();
  }

  List<RadioModel> parseRadioModelList(String responseBody) {
    final parsed = json.decode(responseBody)['values'];

    return parsed.map<RadioModel>((json) => RadioModel.fromJson(json)).toList();
  }

  List<ResultCalendarItem> parseCalendarItemModelList(String responseBody) {
    final parsed = json.decode(responseBody)['values'];

    return parsed
        .map<ResultCalendarItem>((json) => ResultCalendarItem.fromJson(json))
        .toList();
  }

  CircularEventModel parseCircularEvent(String responseBody) {
    final parsed = json.decode(responseBody);
    return CircularEventModel.fromJson(parsed[0]);
  }

  //consume todos los eventos de circular.unal.edu.co/rest/eventos
  Future<List<CircularEventModel>> fetchAllCircularEvent() async {
    var url = 'http://$_hostDomain$_urlEventos';
    // Await the http get response, then decode the json-formatted response.
    var response = await http.get(url);
    if (response.statusCode == 200) {
      var temp = parseCircularEvents(utf8.decode(response.bodyBytes));

      return temp;
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Request failed with status: ${response.statusCode}.');
    }
  }

  //consume los eventos de circular.unal.edu.co/rest/search
  Future<List<CircularEventModel>> fetchAllCircularEventBySize(int size) async {
    var url = 'http://$_hostDomain$_urlEventosSearch';
    var _body = jsonEncode(<String, dynamic>{'q': " ", 'size': size});


    var response = await http.post(
      url,
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: _body,
    );

    var temp = parseCircularEvents(utf8.decode(response.bodyBytes));

    return temp;
  }

  //consume un evento de circular.unal.edu.co/rest/eventos/show/id
  Future<CircularEventModel> fetchCircularEventById(int id) async {
    var url = 'http://$_hostDomain$_urlEventosShow/$id';

    // Await the http get response, then decode the json-formatted response.
    var response = await http.get(url);
    if (response.statusCode == 200) {
      final parsed = json.decode(utf8.decode(response.bodyBytes));
      var temp = CircularEventModel.fromMapToCompleteObject(parsed[0]);

      //Descarga la imagen y la codifica en base64
      String imgEncode;
      var responseImg = await http.get(temp.imgUrl);

      if (responseImg.statusCode == 200) {
        imgEncode = base64Encode(responseImg.bodyBytes);
      } else {
        //TODO: deberia ir una imagen por defecto en caso de que falle la descarga
        imgEncode = "";
      }

      temp.imgBase64 = imgEncode;

      return temp;
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Request failed with status: ${response.statusCode}.');
    }
  }

  //consume los evento de circular.unal.edu.co/rest/search
  //Se invoca este metodo para consultar eventos relacionados al tipo de un evento
  // y para el llamado de los filtros
  Future<List<CircularEventModel>> fetchCircularEventByFilters(
      List<int> sedes, List<int> areas, List<int> tipos, bool isSpecial, String dateLimitType, int size) async {
    var url = 'http://$_hostDomain$_urlEventosSearch';

    var jsonBody = new Map <String, dynamic> ();
    if(sedes != null && sedes.length > 0){
      jsonBody["dependencia"] = sedes;
    }
    if(tipos != null && tipos.length > 0){
      jsonBody["type"] = tipos;
    }
    if(areas != null && areas.length > 0){
      jsonBody["area"] = areas;
    }
    if(size != null && size > 0){
      jsonBody['size'] = size;
    }
    if(dateLimitType != null){
      jsonBody['date'] = <String, dynamic>{
        "type":dateLimitType
      };
    }

    if(isSpecial){
      jsonBody['allSpecialCategory'] = true;
    }

    if(sedes != null && sedes.length > 0){
      jsonBody["dependencia"] = sedes[0];
    }


    var _body = jsonEncode(
      jsonBody
    );


    var response = await http.post(url,
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: _body);

    var temp = parseCircularEvents(utf8.decode(response.bodyBytes));

    return temp;
  }


  Future<List<CircularEventModel>> fetchCircularEventByString(
      String query, int size) async {
    var url = 'http://$_hostDomain$_urlEventosSearch';


    var _body = jsonEncode(
    <String, dynamic>{
      "q": query,
      "size": size
    }
    );


    var response = await http.post(url,
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: _body);

    var temp = parseCircularEvents(utf8.decode(response.bodyBytes));

    return temp;
  }

  //consume un evento de circular.unal.edu.co/rest/usuarios/eventos/{userId}
  Future<List<CircularEventModel>> fetchCircularEventsByUser(
      int userUid, String username, String pass, String type) async {
    var _username64 = base64.encode(utf8.encode(username));
    var url = 'http://$_hostDomain$_urlEventosByUser/$userUid';
    // Await the http get response, then decode the json-formatted response.
    var response = await http.post(
      url,
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, dynamic>{
        "userType": type,
        "credentials": {
          "user": encrypt(_username64),
          "password": encrypt(pass)
        },
      }),
    );
    if (response.statusCode == 200) {
      return parseCircularEvents(utf8.decode(response.bodyBytes));
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Request failed with status: ${response.statusCode}.');
    }
  }

  //consume un evento de circular.unal.edu.co/rest/usuarios/favoritos/show/{userId}
  Future<List<CircularEventModel>> fetchCircularFavoriteEventsByUser(
      int userUid, String username, String pass, String type) async {
    var url = 'http://$_hostDomain$_urlEventosFavoritosByUser/$userUid';
    var _username64 = base64.encode(utf8.encode(username));
    // Await the http get response, then decode the json-formatted response.
    var response = await http.post(
      url,
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, dynamic>{
        "userType": type,
        "credentials": {
          "user": encrypt(_username64),
          "password": encrypt(pass)
        },
      }),
    );
    if (response.statusCode == 200) {
      return parseCircularEvents(utf8.decode(response.bodyBytes));
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Request failed with status: ${response.statusCode}.');
    }
  }

  //retorna un json de tipo {"status":bool}
  //consume un servicio de circular.unal.edu.co/rest/usuarios/favorito/{userId}

  Future<dynamic> deleteCircularFavoriteEventsByUser(int userUid,
      String username, String pass, String type, int eventUid) async {
    var _username64 = base64.encode(utf8.encode(username));
    var url = 'http://$_hostDomain$_urlDeleteFavoritoByUser/$userUid';
    var body = jsonEncode(<String, dynamic>{
      "userType": type,
      "credentials": {"user": encrypt(_username64), "password": encrypt(pass)},
      "eventUid": eventUid
    });

    var client = http.Client();



    var response = await client.send(http.Request(
      "DELETE",
      Uri.parse(url),
    )
      ..headers['Content-Type'] = 'application/json; charset=UTF-8'
      ..body = body);

    final respStr = await response.stream.bytesToString();

    var res = json.decode(respStr);
    return res;
  }

  Future<dynamic> deleteCircularEvent(int userUid, String username, String pass,
      String type, int eventUid) async {
    var _username64 = base64.encode(utf8.encode(username));

    var url = 'http://$_hostDomain$_urlDeleteEvent/$userUid';
    var body = jsonEncode(<String, dynamic>{
      "userType": type,
      "credentials": {"user": encrypt(_username64), "password": encrypt(pass)},
      "eventUid": eventUid
    });

    var client = http.Client();

    var response = await client.send(http.Request(
      "DELETE",
      Uri.parse(url),
    )
      ..headers['Content-Type'] = 'application/json; charset=UTF-8'
      ..body = body);

    final respStr = await response.stream.bytesToString();

    var res = json.decode(respStr);
    return res;
  }

  //consume todos los eventos de circular.unal.edu.co/rest/eventos
  Future<List<RadioModel>> fetchAllDependences() async {
    var url = 'http://$_hostDomain$_urlDependences';

    // Await the http get response, then decode the json-formatted response.
    var response = await http.get(url);

    if (response.statusCode == 200) {
      var jsonTemp = utf8.decode(response.bodyBytes);

      var temp = parseRadioModelList(jsonTemp);
      return temp;
    } else {
      // If that call was not successful, throw an error.
      //print('Request failed with status: ${response.statusCode}.');
      return null;
    }
  }

  Future<List<RadioModel>> postDependencesByString(String query) async {
    var url = 'http://$_hostDomain$_urlDependences';
    var body = jsonEncode(<String, dynamic>{"query": query});

    var response = await http.post(url,
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: body);

    if (response.statusCode == 200) {
      var jsonTemp = utf8.decode(response.bodyBytes);

      var temp = parseRadioModelList(jsonTemp);
      return temp;
    } else {
      // If that call was not successful, throw an error.
      //print('Request failed with status: ${response.statusCode}.');
      return null;
    }
  }

  //consume todos los eventos de circular.unal.edu.co/rest/eventos
  Future<List<RadioModel>> fetchCategoryByType(String type) async {
    var url = 'http://$_hostDomain$_urlCategories/$type';

    // Await the http get response, then decode the json-formatted response.
    var response = await http.get(url);

    if (response.statusCode == 200) {
      var jsonTemp = utf8.decode(response.bodyBytes);

      var temp = parseRadioModelList(jsonTemp);
      return temp;
    } else {
      // If that call was not successful, throw an error.
      //print('Request failed with status: ${response.statusCode}.');
      return null;
    }
  }

  //consume un servicio de circular.unal.edu.co/rest/usuarios/nuevoevento/{userId}
  //Crea un nuevo evento
  Future<dynamic> postNewCircular(int userUid, String username, String pass,
      String type, CircularEventModel event) async {
    var _username64 = base64.encode(utf8.encode(username));

    var url = 'http://$_hostDomain$_urlNewEvent/$userUid';
    var body = jsonEncode(<String, dynamic>{
      "userType": type,
      "credentials": {"user": encrypt(_username64), "password": encrypt(pass)},
      "event": event.parseToJsonBackend(),
    });

    var response = await http.post(url,
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: body);

    if (response.statusCode == 200) {
      return json.decode(utf8.decode(response.bodyBytes));
    } else {
      // If that call was not successful, throw an error.
      print('Request failed with status: ${response.statusCode}.');
      return null;
    }
  }

//consume un servicio de circular.unal.edu.co/rest/usuarios/editavento/{userId}
  //Edita un  evento
  Future<dynamic> putCircularEvent(int userUid, String username, String pass,
      String type, CircularEventModel event) async {
    var _username64 = base64.encode(utf8.encode(username));
    var url = 'http://$_hostDomain$_urlUpdateEvent/$userUid';
    var body = jsonEncode(<String, dynamic>{
      "userType": type,
      "credentials": {"user": encrypt(_username64), "password": encrypt(pass)},
      "event": event.parseToJsonBackend(),
    });

    var response = await http.put(url,
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: body);

    if (response.statusCode == 200) {
      return json.decode(utf8.decode(response.bodyBytes));
    } else {
      // If that call was not successful, throw an error.
      print('Request failed with status: ${response.statusCode}.');
      return null;
    }
  }

  //consume todos los eventos de circular.unal.edu.co/rest/eventos/dia/{yyyy-mm-dd}
  Future<List<ResultCalendarItem>> fetchCalendarItemsByDay(
      String dateDay) async {
    var url = 'http://$_hostDomain$_urlCalendarItemsByDay/$dateDay';

    // Await the http get response, then decode the json-formatted response.
    var response = await http.get(url);

    if (response.statusCode == 200) {
      var jsonTemp = utf8.decode(response.bodyBytes);

      var temp = parseCalendarItemModelList(jsonTemp);

      return temp;
    } else {
      // If that call was not successful, throw an error.
      print('Request failed with status: ${response.statusCode}.');
      return null;
    }
  } //consume todos los eventos de circular.unal.edu.co/rest/eventos/dia/{yyyy-mm-dd}

  //consume todos los eventos de circular.unal.edu.co/rest/eventos/dia/{yyyy-mm-dd}
  Future<List<dynamic>> fetchDatesByEventUid(int eventUid) async {
    var url = 'http://$_hostDomain$_urlFechasByEventUid/$eventUid';

    // Await the http get response, then decode the json-formatted response.
    var response = await http.get(url);

    if (response.statusCode == 200) {
      var temp = json.decode(utf8.decode(response.bodyBytes));
      return temp['values'];
    } else {
      // If that call was not successful, throw an error.
      print('Request failed with status: ${response.statusCode}.');
      return null;
    }
  } //consume todos los eventos de circular.unal.edu.co/rest/eventos/dia/{yyyy-mm-dd}

  //consume los enventos de el servicio buscador ElasticSearch
  Future<List<ResultCalendarItem>> fetchElasticSearchQuery(String query) async {
    var url = 'http://$_urlElasticSearch?q=$query';

    // Await the http get response, then decode the json-formatted response.
    var response = await http.get(url);

    if (response.statusCode == 200) {
      var jsonTemp = utf8.decode(response.bodyBytes);

      var temp = parseCalendarItemModelList(jsonTemp);

      return temp;
    } else {
      // If that call was not successful, throw an error.
      print('Request failed with status: ${response.statusCode}.');
      return null;
    }
  }

  //consume todos los eventos de circular.unal.edu.co/rest/eventos/edited/{timestamp}
  Future<List<CircularEventModel>> fetchCircularEventsEdited(
      DateTime date) async {
    var url =
        'http://$_hostDomain$_urlEventosEdited/${(date.toUtc().millisecondsSinceEpoch / 1000).floor()}';
    // Await the http get response, then decode the json-formatted response.
    var response = await http.get(url);
    print(url);
    print(response.body);
    if (response.statusCode == 200) {
      var temp = parseCircularEvents(utf8.decode(response.bodyBytes));

      return temp;
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Request failed with status: ${response.statusCode}.');
    }
  }

  //consume todos los eventos de circular.unal.edu.co/rest/eventos/deleted/{timestamp}
  Future<List<int>> fetchCircularEventsDeleted(DateTime date) async {
    var url =
        'http://$_hostDomain$_urlEventosDeleted/${(date.toUtc().millisecondsSinceEpoch / 1000).floor()}';
    List<dynamic> list_temp = [];
    List<int> list = [];

    var response = await http.get(url);
    if (response.statusCode == 200) {
      if (response.body != null) {
        list_temp = json.decode(response.body);

        list = list_temp.map((e) => e as int).toList();
      }
      return list;
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Request failed with status: ${response.statusCode}.');
    }
  }

  //consume todos los eventos de circular.unal.edu.co/rest/eventos/hidden/{timestamp}
  Future<List<int>> fetchCircularEventsHidden(DateTime date) async {
    var url =
        'http://$_hostDomain$_urlEventosHidden/${(date.toUtc().millisecondsSinceEpoch / 1000).floor()}';
    List<dynamic> list_temp = [];
    List<int> list = [];

    var response = await http.get(url);
    if (response.statusCode == 200) {
      if (response.body != null) {
        list_temp = json.decode(response.body);

        list = list_temp.map((e) => e as int).toList();
      }
      return list;
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Request failed with status: ${response.statusCode}.');
    }
  }

  //consume todos los eventos de circular.unal.edu.co/rest/eventos/overdue/{timestamp}
  Future<List<int>> fetchCircularEventsOverdue(DateTime date) async {
    var url =
        'http://$_hostDomain$_urlEventosOverdue/${(date.toUtc().millisecondsSinceEpoch / 1000).floor()}';
    List<dynamic> list_temp = [];
    List<int> list = [];

    var response = await http.get(url);
    if (response.statusCode == 200) {
      if (response.body != null) {
        list_temp = json.decode(response.body);

        list = list_temp.map((e) => e as int).toList();
      }
      return list;
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Request failed with status: ${response.statusCode}.');
    }
  }

  //consume todos los eventos de circular.unal.edu.co/rest/edited/{timestamp}
  Future<List<CircularEventModel>> fetchCircularEventsEditedAndCreated(
      DateTime date) async {
    var url =
        'http://$_hostDomain$_urlEventosEdited/${(date.toUtc().millisecondsSinceEpoch / 1000).floor()}';
    // Await the http get response, then decode the json-formatted response.
    var response = await http.get(url);
    if (response.statusCode == 200) {
      var temp = parseCircularEvents(utf8.decode(response.bodyBytes));

      return temp;
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Request failed with status: ${response.statusCode}.');
    }
  }
}
