import 'dart:convert';

import 'package:circular/src/business_logic/enc-dec.dart';
import 'package:circular/src/data/models/circular_event_model.dart';
import 'package:circular/src/data/models/circular_user_model.dart';
import 'package:http/http.dart' as http;

class CircularUserProvider {
  final _hostDomain = "dev3.unal.edu.co";
  final _urlUserLogin = "/rest/usuarios/login";
  final _urlConfigurationsByUser = "/rest/usuarios/configuracion";
  final _urlCircularFavoriteEventsToUser = "/rest/usuarios/favorito";

  CircularUserModel parseCircularUser(String responseBody) {
    final parsed = json.decode(responseBody);

    return CircularUserModel.fromJson(parsed["info"]);
  }

  //Retorna los datos del usuario
  Future<CircularUserModel> postCircularUserLogin(
      String username, String pass, String type) async {

    var url = 'http://$_hostDomain$_urlUserLogin';

    var _username64 = base64.encode(utf8.encode(username));

    var _body = jsonEncode(<String, dynamic>{
      "userType": type,
      "credentials": {"user": encrypt(_username64), "password": encrypt(pass)},
    });


    // Await the http get response, then decode the json-formatted response.
    var response = await http.post(url,
        headers: <String, String>{
          //'Content-Type': 'application/json; charset=UTF-8',
        },
        body: _body);


    if (response.statusCode == 200) {
      // If the server did return a 200 CREATED response,
      // then parse the JSON.

      CircularUserModel newUser =
          parseCircularUser(utf8.decode(response.bodyBytes));
      newUser.type = type;
      newUser.username = username;
      newUser.pass = pass;

      return newUser;
    } else {
      // If that call was not successful, throw an error.
      print('Request failed with status: ${response.statusCode}.');
      return null;
    }
  }

  Future<dynamic> postConfigurationsByUser(
      int userUid, String username, String pass, String type) async {
    var url = 'http://$_hostDomain$_urlConfigurationsByUser/$userUid';
    var _username64 = base64.encode(utf8.encode(username));

    // Await the http get response, then decode the json-formatted response.
    var response = await http.post(
      url,
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, dynamic>{
        "userType": type,
        "credentials": {"user": encrypt(_username64), "password": encrypt(pass)},

      }),
    );
    if (response.statusCode == 200) {
      return json.decode(utf8.decode(response.bodyBytes));
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Request failed with status: ${response.statusCode}.');
    }
  }

  //retorna un json de tipo {"status":bool}
  Future<dynamic> putConfigurationsByUser(
      int userUid,
      String username,
      String pass,
      String type,
      bool flagSuscription,
      bool flagFavorites) async {
    var url = 'http://$_hostDomain$_urlConfigurationsByUser/$userUid';
    var _username64 = base64.encode(utf8.encode(username));

    var body = jsonEncode(<String, dynamic>{
      "userType": type,
      "credentials": {"user": encrypt(_username64), "password": encrypt(pass)},

      "config": {
        "suscripcion": flagSuscription,
        "notificacionesFavoritos": flagFavorites
      }
    });

    // Await the http get response, then decode the json-formatted response.
    var response = await http.put(url,
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: body);

    if (response.statusCode == 200) {
      return json.decode(utf8.decode(response.bodyBytes));
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Request failed with status: ${response.statusCode}.');
    }
  }

  //retorna un json de tipo {"status":bool}
  //consume un servicio de circular.unal.edu.co/rest/usuarios/favorito/{userId}
  //Agrega un evento favorito seleccionado por el usuario
  Future<dynamic> postCircularFavoriteEventsToUser(int userUid, String username,
      String pass, String type, int eventUid) async {
    var _username64 = base64.encode(utf8.encode(username));

    var url = 'http://$_hostDomain$_urlCircularFavoriteEventsToUser/$userUid';
    var body = jsonEncode(<String, dynamic>{
      "userType": type,
      "credentials": {"user": encrypt(_username64), "password": encrypt(pass)},
      "eventUid": eventUid
    });

    var response = await http.post(url,
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: body);

    if (response.statusCode == 200) {
      return json.decode(utf8.decode(response.bodyBytes));
    } else {
      // If that call was not successful, throw an error.
      print('Request failed with status: ${response.statusCode}.');
      return null;
    }
  }
}
