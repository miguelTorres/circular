import 'dart:convert';

import 'package:circular/src/data/models/museum_model.dart';
import 'package:http/http.dart' as http;


class MuseumApiProvider {

  final _hostDomain = "dev3.unal.edu.co";
  final _urlMuesums = "/rest/museos";


  List<MuseumModel> parseMuseums(String responseBody) {
    final parsed = json.decode(responseBody);
    return parsed.map<MuseumModel>((json) => MuseumModel.fromJson(json)).toList();
  }

  //consume todos los eventos de circular.unal.edu.co/rest/museos
  Future<List<MuseumModel>> fetchAllMuseums() async {
    var url = 'http://$_hostDomain$_urlMuesums';
    // Await the http get response, then decode the json-formatted response.
    var response = await http.get(url);
    if (response.statusCode == 200) {
      return parseMuseums(utf8.decode(response.bodyBytes));

    } else {
      // If that call was not successful, throw an error.
      throw Exception('Request failed with status: ${response.statusCode}.');
    }

  }

}