/*
* Contiene los metodos DAO Data Access Object
* Son metodos que permiten hacer las operaciones CRUD sobre la base de datos
* */



import 'package:circular/src/data/models/location_model.dart';
import 'package:circular/src/data/models/museum_model.dart';
import 'database.dart';

class MuseumDao {
  final dbProvider = DatabaseProvider.dbProvider;


  // ##### CRUD  de museos  #######


  //Nuevo registro de Museo
  Future<int> createMuseum(MuseumModel museum) async {
    final db = await dbProvider.database;
    var result = db.insert(museumTable, museum.toMap());
    return result;
  }

  Future<List<MuseumModel>> readMuseums() async {
    final db = await dbProvider.database;

    List<Map<String, dynamic>> result;
    result = await db.rawQuery("SELECT * FROM $museumTable JOIN $locationTable ON $museumTable.$colIDCircularLocation=$locationTable.$colIDCircularLocation");


        List<MuseumModel> museums = result.isNotEmpty
        ? result.map((item) => MuseumModel.fromMapObject(item)).toList()
        : [];

    return museums;
  }


  Future<List<MuseumModel>> readMuseumsByFilters({List<String> columns, String query}) async {
    final db = await dbProvider.database;

    List<Map<String, dynamic>> result;
    if (query != null) {
      if (query.isNotEmpty)
        result = await db.query(museumTable,
        //    columns: columns,
        //    where: 'description LIKE ?',
        //    whereArgs: ["%$query%"]
        );

    } else {
      result = await db.query(museumTable, columns: columns);
    }

    List<MuseumModel> museums = result.isNotEmpty
        ? result.map((item) => MuseumModel.fromMapObject(item)).toList()
        : [];
    return museums;
  }

  Future deleteAll() async {
    final db = await dbProvider.database;
    var result = await db.delete(
        museumTable
    );

    return result;
  }


/*
  //Update Todo record
  Future<int> updateTodo(Todo todo) async {
    final db = await dbProvider.database;

    var result = await db.update(todoTABLE, todo.toDatabaseJson(),
        where: "id = ?", whereArgs: [todo.id]);

    return result;
  }

  //Delete Todo records
  Future<int> deleteTodo(int id) async {
    final db = await dbProvider.database;
    var result = await db.delete(todoTABLE, where: 'id = ?', whereArgs: [id]);

    return result;
  }

  //We are not going to use this in the demo
  Future deleteAllTodos() async {
    final db = await dbProvider.database;
    var result = await db.delete(
      todoTABLE,
    );

    return result;
  }*/

}