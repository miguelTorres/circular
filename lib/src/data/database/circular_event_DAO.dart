/*
* Contiene los metodos DAO Data Access Object
* Son metodos que permiten hacer las operaciones CRUD sobre la base de datos
* */

import 'package:circular/src/data/models/circular_event_model.dart';
import 'package:circular/src/data/models/result_calendar_item.dart';

import 'database.dart';

class CircularEventDao {
  final dbProvider = DatabaseProvider.dbProvider;

  // ##### CRUD  de eventos  #######

  //Nuevo registro de circulasEvent
  Future<int> createCircularEvent(CircularEventModel circularEventModel) async {
    final db = await dbProvider.database;
    var result = db.insert(eventTable, circularEventModel.toMap());
    return result;
  }

  Future<List<CircularEventModel>> readCircularEvents() async {
    final db = await dbProvider.database;

    List<Map<String, dynamic>> result;

    String query =
        "SELECT * FROM $eventTable JOIN $locationTable ON $eventTable.$colIDCircularLocation=$locationTable.$colIDCircularLocation ORDER BY $colCurrentDate ASC";

    result = await db.rawQuery(query);

    List<CircularEventModel> events = result.isNotEmpty
        ? result.map((item) => CircularEventModel.fromMapObject(item)).toList()
        : [];

    return events;
  }

  //sedes = [1,2,3 ...]
  //areas = [1,2,3 ...]
  //tipos = [1,2,3 ...]
  Future<List<CircularEventModel>> readCircularEventsByFilters(
      List<int> sedes,
      List<int> areas,
      List<int> tipos,
      size,
      bool special,
      DateTime dateLimit) async {
    String sedesString =
        sedes.toString().replaceAll('[', '(').replaceAll(']', ')');
    String areasString =
        areas.toString().replaceAll('[', '(').replaceAll(']', ')');
    String tiposString =
        tipos.toString().replaceAll('[', '(').replaceAll(']', ')');

    DateTime today =
        DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day);

    final db = await dbProvider.database;

    String query = "";
    query +=
        "SELECT * FROM $eventTable JOIN $locationTable ON $eventTable.$colIDCircularLocation=$locationTable.$colIDCircularLocation";
    query += " WHERE $colAreaUid IN $areasString";
    query += " OR $colTypeUid IN $tiposString";
    query += " OR $colCampusUid IN $sedesString";

    if (special) {
      query += " OR $colSpecialUid <> -1";
    }

    if (dateLimit != null) {
      query +=
          " OR $colCurrentDate > ${today.millisecondsSinceEpoch} AND $colCurrentDate < ${dateLimit.millisecondsSinceEpoch}";
    }

    query += " ORDER BY $colCurrentDate ASC";

    List<Map<String, dynamic>> result;
    result = await db.rawQuery(query);

    List<CircularEventModel> circularEvents = result.isNotEmpty
        ? result.map((item) => CircularEventModel.fromMapObject(item)).toList()
        : [];

    return circularEvents;
  }

  Future<List<CircularEventModel>> readCircularEventsByString(String q) async {
    final db = await dbProvider.database;

    List<Map<String, dynamic>> result;

    String query =
        "SELECT * FROM $eventTable JOIN $locationTable ON $eventTable.$colIDCircularLocation=$locationTable.$colIDCircularLocation WHERE $colTitle LIKE '%$q%' OR $colCampus LIKE '%$q%' OR $colDescription LIKE '%$q%' OR $colKeywords LIKE '%$q%'";
    result = await db.rawQuery(query);

    List<CircularEventModel> events = result.isNotEmpty
        ? result
            .map((item) => CircularEventModel.fromMapObject(item))
            .toList()
        : [];

    return events;
  }

  //Elimina un evento de la base de datos local
  Future<void> deleteCircularEvent(int eventUid) async {
    final db = await dbProvider.database;

    await db.delete(eventTable, where: "$colUid = ?", whereArgs: [eventUid]);
  }

  Future<bool> verifyExistByUid(int uid) async {
    final db = await dbProvider.database;

    List<Map<String, dynamic>> result;

    String query = "SELECT $colUid FROM $eventTable WHERE $colUid=$uid";
    result = await db.rawQuery(query);

    if (result != null) {
      if (result.length > 0) {
        if (result[0][colUid] != null) {
          if (result[0][colUid] > 0) {
            return true;
          }
        }
      }
    }

    return false;
  }


  //Actualiza registro de ubicacion
  Future<int> updateCircularEvent(CircularEventModel circularEvent) async {
    final db = await dbProvider.database;
    var result = db.update(eventTable, circularEvent.toMap(),
        where: "$colUid = ?", whereArgs: [circularEvent.uid]
    );
    return result;
  }

  Future deleteAll() async {
    final db = await dbProvider.database;
    var result = await db.delete(
        eventTable
    );

    return result;
  }

}
