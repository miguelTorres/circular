import 'dart:io';

import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

//Nombres de las columnas de la tablas
String museumTable = 'museum_table';
String colUid = 'uid';
String colName = 'name';
String colDescription = 'description';
String colPhone = 'phone';
String colAddress = 'address';
String colEmail = 'email';
String colOfficeHours = 'officeHours';
String colMoreInformation = 'moreInformation';
String colImageUrl = 'imageUrl';
String colCampus = 'campus';
String colIDCircularLocation = 'id_circularLocation';
String colImgBase64 = 'imgBase64';

String locationTable = 'location_table';
String colLongitude = 'longitude';
String colLatitude = 'latitude';

String eventTable = 'event_table';
String colTitle = "title";
String colInitialDate = "initialDate";
String colCurrentDate = "currentDate";
String colFinalDate = "finalDate";
String colImgUrl = "imgUrl";
String colDependenciaName = "dependenciaName";
String colInvitados = "invitados";
String colTicketsType = "ticketsType";
String colTicketsUrl = "ticketsUrl";
String colInscriptionDateInitial = "inscriptionDateInitial";
String colInscriptionDateFinal = "inscriptionDateFinal";
String colInscriptionWay = "inscriptionWay";
String colEventUrl = "eventUrl";
String colUrl = "url";
String colKeywords = "keywords";
String colCampusUid = "campusUid";
String colAreaUid = "areaUid";
String colTypeUid = "typeUid";
String colSpecialUid = "specialUid";



class DatabaseProvider {


  static final DatabaseProvider dbProvider = DatabaseProvider();

  Database _database;

  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await createDatabase();
    return _database;
  }

  createDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();

    String path = documentsDirectory.path + "circular.db";
    var database = await openDatabase(path,
        version: 1, onCreate: initDB, onUpgrade: onUpgrade);

    return database;
  }


  void onUpgrade(Database database, int oldVersion, int newVersion) {
    if (newVersion > oldVersion) {}
  }
  void initDB(Database database, int version) async {
    await database.execute('CREATE TABLE $museumTable($colUid INTEGER PRIMARY KEY,'
        '$colName TEXT,'
        '$colDescription TEXT,'
        '$colPhone TEXT,'
        '$colAddress TEXT,'
        '$colEmail TEXT,'
        '$colOfficeHours TEXT,'
        '$colMoreInformation TEXT,'
        '$colImageUrl TEXT,'
        '$colUrl TEXT,'
        '$colCampus TEXT,'
        '$colImgBase64 TEXT,'
        '$colIDCircularLocation INT)');

    await database.execute('CREATE TABLE $locationTable($colIDCircularLocation INTEGER PRIMARY KEY,'
        '$colLatitude DOUBLE,'
        '$colLongitude DOUBLE,'
        '$colAddress TEXT)');


    await database.execute('CREATE TABLE $eventTable($colUid INTEGER PRIMARY KEY,'

        '$colTitle TEXT,'
        '$colDescription TEXT,'
        '$colInitialDate INTEGER,'
        '$colCurrentDate INTEGER,'
        '$colFinalDate INTEGER,'
        '$colImgUrl TEXT,'
        '$colCampus TEXT,'
        '$colDependenciaName TEXT,'
        '$colInvitados TEXT,'
        '$colTicketsType TEXT,'
        '$colTicketsUrl TEXT,'
        '$colInscriptionDateInitial INTEGER,'
        '$colInscriptionDateFinal INTEGER,'
        '$colInscriptionWay TEXT,'
        '$colEventUrl TEXT,'
        '$colKeywords TEXT,'
        '$colImgBase64 TEXT,'
        '$colCampusUid INT,'
        '$colAreaUid INT,'
        '$colTypeUid INT,'
        '$colSpecialUid INT,'
        '$colIDCircularLocation INTEGER)');

  }

}