/*
* Contiene los metodos DAO Data Access Object
* Son metodos que permiten hacer las operaciones CRUD sobre la base de datos
* */



import 'package:circular/src/data/models/location_model.dart';

import 'database.dart';

class CircularLocationDao {
  final dbProvider = DatabaseProvider.dbProvider;

  // ##### CRUD  de museos  #######

  //Nuevo registro de ubicacion
  Future<int> createCircularLocation(CircularLocation circularLocation) async {
    final db = await dbProvider.database;
    var result = db.insert(locationTable, circularLocation.toMap());
    return result;
  }


  Future<CircularLocation> readCircularLocationById(int id) async {
    final db = await dbProvider.database;

    List<Map<String, dynamic>> result;
    result = await db.query(museumTable,
          where: 'uid = $id'
    );

    List<CircularLocation> locations = result.isNotEmpty
        ? result.map((item) => CircularLocation.fromMapObject(item)).toList()
        : [];

    return locations[0];
  }

  Future<bool> verifyExistByUid(int uid) async {
    final db = await dbProvider.database;

    List<Map<String, dynamic>> result;

    String query = "SELECT $colIDCircularLocation FROM $locationTable WHERE $colIDCircularLocation=$uid";
    result = await db.rawQuery(query);

    if (result != null) {
      if (result.length > 0) {
        if (result[0][colIDCircularLocation] != null) {
          if (result[0][colIDCircularLocation] > 0) {
            return true;
          }
        }
      }
    }

    return false;
  }


  //Actualiza registro de ubicacion
  Future<int> updateCircularLocation(CircularLocation circularLocation) async {
    final db = await dbProvider.database;
    var result = db.update(locationTable, circularLocation.toMap(),
    where: "$colIDCircularLocation = ?", whereArgs: [circularLocation.uid]
    );
    return result;
  }


  //Elimina una ubicación de la base de datos local
  Future<int> deleteCircularLocation(int id) async {
    final db = await dbProvider.database;
    var result = await db.delete(locationTable, where: '$colIDCircularLocation = ?', whereArgs: [id]);

    return result;
  }


  //Retorna el Uid de la Ubicación de un evento

  Future<int> readLocationUidByEventUid(int eventUid) async {
    final db = await dbProvider.database;

    List<Map<String, dynamic>> result;

    String query = "";
    query +=
    "SELECT $locationTable.$colIDCircularLocation FROM $eventTable JOIN $locationTable ON $eventTable.$colIDCircularLocation=$locationTable.$colIDCircularLocation";
    query += " WHERE $colUid = $eventUid";

    result = await db.rawQuery(query);

    if (result != null) {
      if (result.length > 0) {
        if (result[0][colIDCircularLocation] != null) {
          return result[0][colIDCircularLocation];
        }
      }
    }

    return null;
  }



  Future deleteAll() async {



    final db = await dbProvider.database;
    var result = await db.delete(
      locationTable
    );

    return result;
  }

}