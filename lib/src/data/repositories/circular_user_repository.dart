


import 'package:circular/src/data/models/circular_user_model.dart';
import 'package:circular/src/data/providers/circular_user_provider.dart';

class CircularUserRepository {
  final circularUserApiProvider = CircularUserProvider();

  Future<CircularUserModel> findUserLogin( String user, String pass, String type  ) =>
      circularUserApiProvider.postCircularUserLogin(user, pass, type);


  Future<dynamic> findConfigurationByUser( int userUid, String username, String pass, String type  ) =>
      circularUserApiProvider.postConfigurationsByUser(userUid, username, pass, type);


  Future<dynamic> updateConfigurationByUser( int userUid, String username, String pass, String type , bool flagSuscription, bool flagFavorites ) =>
      circularUserApiProvider.putConfigurationsByUser(userUid, username, pass, type, flagSuscription, flagFavorites);


  Future<dynamic> addFavoriteToUser( int userUid, String username, String pass, String type , int eventUid ) =>
      circularUserApiProvider.postCircularFavoriteEventsToUser(userUid, username, pass, type, eventUid);




}