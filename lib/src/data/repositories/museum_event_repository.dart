
import 'package:circular/src/data/database/museum_DAO.dart';
import 'package:circular/src/data/models/museum_model.dart';


class MuseumRepository {
  final museumDao = MuseumDao();

  Future<List<MuseumModel>> fetchAllMuseums() async {

    List<MuseumModel> temp = await museumDao.readMuseums();

    return temp;

  }
}