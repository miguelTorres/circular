
import 'package:circular/src/data/database/circular_event_DAO.dart';
import 'package:circular/src/data/models/circular_event_model.dart';
import 'package:circular/src/data/models/circular_user_model.dart';
import 'package:circular/src/data/models/radio_model.dart';
import 'package:circular/src/data/models/result_calendar_item.dart';
import 'package:circular/src/data/providers/circular_event_provider.dart';
import 'package:connectivity/connectivity.dart';

class CircularEventRepository {
  final circularEventApiProvider = CircularEventApiProvider();
  final circularEventDao = CircularEventDao();

    Future<List<CircularEventModel>> fetchAllCircularEvent() async {

      var connectivityResult = await Connectivity().checkConnectivity();

      List<CircularEventModel> temp;


      if (connectivityResult == ConnectivityResult.none) {
        // Si No hay coneccion a internet, carga los eventos de la db local
        temp = await circularEventDao.readCircularEvents();


      } else if (connectivityResult == ConnectivityResult.wifi ||
          connectivityResult == ConnectivityResult.mobile) {
        // Si hay coneccion por wifi o Si hay coneccion por datos
        temp = await circularEventApiProvider.fetchAllCircularEventBySize(20);
      }


    return temp;
  }

  Future<List<CircularEventModel>> findCircularEventByFilters (
      List<int> sedes, List<int> areas, List<int>tipos, size, bool isSpecial, String dateLimitType) async{


    var connectivityResult = await Connectivity().checkConnectivity();

    List<CircularEventModel> temp;
    DateTime dateLimit = null;


    if (connectivityResult == ConnectivityResult.none) {
      // Si No hay coneccion a internet, carga los eventos de la db local
      //Consulta los eventos filtrados en la base de datos local
      DateTime today = DateTime.now();


      if(dateLimitType!=null){
        if(dateLimitType == 'today'){ //Eventos de hoy
          dateLimit = DateTime(today.year, today.month, today.day +1);
        } else if(dateLimitType == 'week'){ //Eventos en los siguientes 7 dias
          dateLimit = DateTime(today.year, today.month, today.day + 7);

        } else if(dateLimitType == 'month'){ //Eventos en los siguientes 30 dias
          dateLimit = DateTime(today.year, today.month, today.day +30);

        }
      }


      temp =  await circularEventDao.readCircularEventsByFilters(sedes, areas, tipos, size, isSpecial, dateLimit);

    } else if (connectivityResult == ConnectivityResult.wifi ||
        connectivityResult == ConnectivityResult.mobile) {

      // Si hay coneccion por wifi o Si hay coneccion por datos
      temp = await circularEventApiProvider.fetchCircularEventByFilters(sedes,areas, tipos, isSpecial, dateLimitType, 20);

    }

       return temp;
  }



  Future<CircularEventModel> findCircularEventById(id) =>
      circularEventApiProvider.fetchCircularEventById(id);

  Future<List<CircularEventModel>> findCircularEventsByUser( int userUid, String username, String pass, String type) =>
      circularEventApiProvider.fetchCircularEventsByUser( userUid, username, pass, type);

  Future<List<CircularEventModel>> findCircularFavoriteEventsByUser( int userUid, String username, String pass, String type ) =>
      circularEventApiProvider.fetchCircularFavoriteEventsByUser(userUid, username, pass, type);


  Future<dynamic> removeCircularFavoriteEventsByUser( int userUid, String username, String pass, String type, int eventUid ) async {
    var temp = await circularEventApiProvider.deleteCircularFavoriteEventsByUser(
        userUid, username, pass, type, eventUid);

    return temp;
  }

  Future<dynamic> deleteCircularEvent( int userUid, String username, String pass, String type, int eventUid ) async {
    var temp = await circularEventApiProvider.deleteCircularEvent(
        userUid, username, pass, type, eventUid);

    return temp;
  }

  Future<List<RadioModel>>  findAllDependences() =>
      circularEventApiProvider.fetchAllDependences();

  Future<List<RadioModel>>  findDependencesByString(String str) =>
      circularEventApiProvider.postDependencesByString(str);

  Future<List<ResultCalendarItem>>  findCalendarItemByDay(String str) =>
      circularEventApiProvider.fetchCalendarItemsByDay(str);

  Future<List<ResultCalendarItem>>  findEventsInElastic(String query) =>
      circularEventApiProvider.fetchElasticSearchQuery(query);

  Future<List<CircularEventModel>>  findEventsByString(String query) async {


    var connectivityResult = await Connectivity().checkConnectivity();

    List<CircularEventModel> temp;


    if (connectivityResult == ConnectivityResult.none) {
      // Si No hay coneccion a internet, carga los eventos de la db local

      //Consulta los eventos en la base de datos local
      temp = await circularEventDao.readCircularEventsByString(query);


    } else if (connectivityResult == ConnectivityResult.wifi ||
        connectivityResult == ConnectivityResult.mobile) {

      // Si hay coneccion por wifi o Si hay coneccion por datos
      temp = await circularEventApiProvider.fetchCircularEventByString(query, 20);

    }

    return temp;
  }

  Future<List<dynamic>>  findDatesByEventUid(int eventUid) async {

    //Consulta los eventos en la base de datos local
    var temp =  circularEventApiProvider.fetchDatesByEventUid(eventUid);

    return temp;

  }


  Future<List<RadioModel>>  findCategoryByType(String type) =>
      circularEventApiProvider.fetchCategoryByType(type);

  Future<dynamic>  createEvent(int userUid, String username, String pass, String type, CircularEventModel event ) =>
      circularEventApiProvider.postNewCircular(userUid, username, pass, type, event);

Future<dynamic>  updateEvent(int userUid, String username, String pass, String type, CircularEventModel event ) =>
      circularEventApiProvider.putCircularEvent(userUid, username, pass, type, event);


  Future<List<CircularEventModel>> findCircularEventsEdited(DateTime date) async {

    //Consulta los eventos en la base de datos local
    List<CircularEventModel> temp = await circularEventApiProvider.fetchCircularEventsEdited(date);

    return temp;
  }


}
