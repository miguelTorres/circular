import 'dart:convert';
import 'package:circular/src/data/database/museum_DAO.dart';
import 'package:circular/src/data/models/circular_event_model.dart';
import 'package:circular/src/data/models/museum_model.dart';
import 'package:circular/src/data/providers/circular_event_provider.dart';
import 'package:circular/src/data/providers/museum_provider.dart';
import 'package:connectivity/connectivity.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../data/database/circular_event_DAO.dart';
import '../data/database/circular_location_DAO.dart';
import 'package:http/http.dart' as http;

class SyncDataBase {
  final circularEventApiProvider = CircularEventApiProvider();
  final circularEventDao = CircularEventDao();
  final circularLocatoinDao = CircularLocationDao();
  final museumApiProvider = MuseumApiProvider();
  final museumDao = MuseumDao();
  List<int> hiddenUidList = [];
  List<int> deletedUidList = [];
  List<int> overdueUidList = [];
  List<CircularEventModel> circularEventsEditedList = [];

  Future<String> syncApiToDataBase() async {
    var connectivityResult = await Connectivity().checkConnectivity();

    if (connectivityResult == ConnectivityResult.none) {
      // Si No hay coneccion a internet
      return 'NO_CONECTION';
    } else if (connectivityResult == ConnectivityResult.wifi ||
        connectivityResult == ConnectivityResult.mobile) {
      // Si hay coneccion por wifi o Si hay coneccion por datos

      syncEvents();
      syncMuseums();

      return 'COMPLETED';
    }
  }

  syncEvents() async {
    List<CircularEventModel> circularEventListApi;

    //####################################
    //# Limpia la base de datos
    //####################################

    await circularLocatoinDao.deleteAll();
    await circularEventDao.deleteAll();

    //####################################
    //# Guarda los eventos mas recientes en base de datos
    //####################################

    //Guarda los ultimos 20 eventos
    circularEventListApi =
        await circularEventApiProvider.fetchAllCircularEventBySize(20);

    for (int i = 0; i < circularEventListApi.length; i++) {
      //Descarga la imagen y la codifica en base64
      String imgEncode;
      var response = await http.get(circularEventListApi[i].imgUrl);

      if (response.statusCode == 200) {
        imgEncode = base64Encode(response.bodyBytes);
      } else {
        //TODO: deberia ir una imagen por defecto en caso de que falle la descarga
        imgEncode = "";
      }

      if (imgEncode.length < 800000) {
        //Guarda la ubicacion en base de datos local
        var locationResult = circularLocatoinDao
            .createCircularLocation(circularEventListApi[i].circularLocation);

        circularEventListApi[i].imgBase64 = imgEncode;
        //Guarda el evento en base de datos
        circularEventDao.createCircularEvent(circularEventListApi[i]);
      }
    }
  }

  syncMuseums() async {
    List<MuseumModel> museumListApi;

    //####################################
    //# Limpia la base de datos
    //####################################

    await museumDao.deleteAll();

    //####################################
    //# Guarda los museos en base de datos
    //####################################

    museumListApi = await museumApiProvider.fetchAllMuseums();

    for (int i = 0; i < museumListApi.length; i++) {
      //Guarda la ubicación en base de datos local
      var locationResult = circularLocatoinDao
          .createCircularLocation(museumListApi[i].circularLocation);

      //Descarga la imagen y la codifica en base64
      String imgEncode;
      var response = await http.get(museumListApi[i].imageUrl);

      if (response.statusCode == 200) {
        imgEncode = base64Encode(response.bodyBytes);
      } else {
        //TODO: deberia ir una imagen por defecto en caso de que falle la descarga
        imgEncode = "";
      }
      museumListApi[i].imgBase64 = imgEncode;
      museumDao.createMuseum(museumListApi[i]);
    }
  }
}
