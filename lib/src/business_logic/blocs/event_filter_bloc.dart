
import 'package:circular/src/data/models/circular_event_model.dart';
import 'package:circular/src/data/repositories/circular_event_repository.dart';
import 'package:rxdart/rxdart.dart';

class EventsFilterBloc{

  final _repository = CircularEventRepository();
  final _circularEventSubject = BehaviorSubject<List<CircularEventModel>>();

  getEventByFilter(sedes, areas, tipos, size, isSpecial, dateLimit) async {

    List<CircularEventModel> circularEventList = await _repository.findCircularEventByFilters(sedes, areas, tipos, size, isSpecial, dateLimit);
    _circularEventSubject.sink.add(circularEventList);
  }

  dispose() {
    _circularEventSubject.close();
  }

  BehaviorSubject<List<CircularEventModel>> get circularEventSubject => _circularEventSubject;

}