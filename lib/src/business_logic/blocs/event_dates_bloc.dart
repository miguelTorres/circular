import 'package:circular/src/data/models/circular_event_model.dart';
import 'package:circular/src/data/models/circular_user_model.dart';
import 'package:circular/src/data/repositories/circular_event_repository.dart';
import 'package:rxdart/rxdart.dart';

class EventDatesBloc{

  final _repository = CircularEventRepository();
  final _circularEventSubject = BehaviorSubject<List<dynamic>>();

  getDatesByEventUid(int eventUid) async {

    List<dynamic> result = await _repository.findDatesByEventUid(eventUid);
    _circularEventSubject.sink.add(result);
  }

  dispose() {
    _circularEventSubject.close();
  }

  BehaviorSubject<List<dynamic>> get circularEventSubject => _circularEventSubject;

}