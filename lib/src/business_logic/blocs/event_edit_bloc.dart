import 'package:circular/src/data/models/circular_event_model.dart';
import 'package:circular/src/data/models/circular_user_model.dart';
import 'package:circular/src/data/repositories/circular_event_repository.dart';
import 'package:circular/src/data/repositories/circular_user_repository.dart';
import 'package:rxdart/rxdart.dart';

class EventEditBloc{

  final _repository = CircularEventRepository();
  final _subject = BehaviorSubject<dynamic>();

  updateEvent(int userUid, String username, String pass, String type, CircularEventModel event) async {
    dynamic result = await _repository.updateEvent( userUid, username, pass, type, event);
    _subject.sink.add(result);
  }

  dispose() {
    _subject.close();
  }

  BehaviorSubject<dynamic> get subject => _subject;

}