import 'package:circular/src/data/models/circular_event_model.dart';
import 'package:circular/src/data/models/circular_user_model.dart';
import 'package:circular/src/data/repositories/circular_event_repository.dart';
import 'package:circular/src/data/repositories/circular_user_repository.dart';
import 'package:rxdart/rxdart.dart';

class UserConfigurationsBloc{

  final _repository = CircularUserRepository();
  final _circularEventSubject = BehaviorSubject<dynamic>();

  getConfigurationByUser(int userUid, String username, String pass, String type) async {
    dynamic result = await _repository.findConfigurationByUser( userUid, username, pass, type);
    _circularEventSubject.sink.add(result);
  }

  dispose() {
    _circularEventSubject.close();
  }

  BehaviorSubject<dynamic> get subject => _circularEventSubject;

}