import 'package:circular/src/data/models/circular_event_model.dart';
import 'package:circular/src/data/models/circular_user_model.dart';
import 'package:circular/src/data/repositories/circular_event_repository.dart';
import 'package:rxdart/rxdart.dart';

class UserEventsBloc{

  final _repository = CircularEventRepository();
  final _circularEventSubject = BehaviorSubject<List<CircularEventModel>>();

  getEventByUser(int userUid, String username, String pass, String type) async {

    List<CircularEventModel> circularEventModel = await _repository.findCircularEventsByUser(userUid, username, pass, type);
    _circularEventSubject.sink.add(circularEventModel);
  }

  dispose() {
    _circularEventSubject.close();
  }

  BehaviorSubject<List<CircularEventModel>> get circularEventSubject => _circularEventSubject;

}