import 'package:circular/src/data/models/museum_model.dart';
import 'package:circular/src/data/repositories/museum_event_repository.dart';
import 'package:rxdart/rxdart.dart';


class MuseumListBloc{

  final _repository = MuseumRepository();
  final _museumEventSubject = BehaviorSubject<List<MuseumModel>>();

  fetchAllMuseums() async {

    List<MuseumModel> museumModel = await _repository.fetchAllMuseums();
    _museumEventSubject.sink.add(museumModel);
  }

  dispose() {
    _museumEventSubject.close();
  }

  BehaviorSubject<List<MuseumModel>> get museumEventSubject => _museumEventSubject;
}