import 'package:circular/src/data/models/circular_event_model.dart';
import 'package:circular/src/data/models/circular_user_model.dart';
import 'package:circular/src/data/models/radio_model.dart';
import 'package:circular/src/data/repositories/circular_event_repository.dart';
import 'package:circular/src/data/repositories/circular_user_repository.dart';
import 'package:rxdart/rxdart.dart';

class CategoriesBloc{

  final _repository = CircularEventRepository();
  final _subject = BehaviorSubject<List<RadioModel>>();

  findCategories(String type) async {

    dynamic result = await _repository.findCategoryByType(type);
    _subject.sink.add(result);
  }

  dispose() {
    _subject.close();
  }

  BehaviorSubject<List<RadioModel>> get subject => _subject;

}