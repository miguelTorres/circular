import 'package:circular/src/data/models/circular_event_model.dart';
import 'package:circular/src/data/models/result_calendar_item.dart';
import 'package:circular/src/data/repositories/circular_event_repository.dart';
import 'package:rxdart/rxdart.dart';

class LocalSearchBloc{
  final _repository = CircularEventRepository();
  final _circularEventSubject = BehaviorSubject<List<CircularEventModel>>();

  searchEventsByString(String query) async {

    List<CircularEventModel> circularEventModel = await _repository.findEventsByString(query);
    _circularEventSubject.sink.add(circularEventModel);

  }

  dispose() {
    _circularEventSubject.close();
  }

  BehaviorSubject<List<CircularEventModel>> get circularEventSubject => _circularEventSubject;

}