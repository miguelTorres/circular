import 'package:circular/src/data/models/circular_event_model.dart';
import 'package:circular/src/data/repositories/circular_event_repository.dart';
import 'package:rxdart/rxdart.dart';

class LoadEventBloc{
  final _repository = CircularEventRepository();
  final _circularEventSubject = BehaviorSubject<CircularEventModel>();

  getEventById(int id) async {


    CircularEventModel circularEventModel = await _repository.findCircularEventById(id);
    _circularEventSubject.sink.add(circularEventModel);

  }

  dispose() {
    _circularEventSubject.close();
  }

  BehaviorSubject<CircularEventModel> get circularEventSubject => _circularEventSubject;

}