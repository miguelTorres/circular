import 'package:circular/src/data/models/circular_event_model.dart';
import 'package:circular/src/data/models/circular_user_model.dart';
import 'package:circular/src/data/repositories/circular_event_repository.dart';
import 'package:circular/src/data/repositories/circular_user_repository.dart';
import 'package:rxdart/rxdart.dart';

class EventRemoveFavoriteBloc{

  final _repository = CircularEventRepository();
  final _subject = BehaviorSubject<dynamic>();

  removeFavoriteByUser(int userUid, String username, String pass, String type, int eventUid) async {
    dynamic result = await _repository.removeCircularFavoriteEventsByUser( userUid, username, pass, type, eventUid);
    _subject.sink.add(result);
  }

  dispose() {
    _subject.close();
  }

  BehaviorSubject<dynamic> get subject => _subject;

}