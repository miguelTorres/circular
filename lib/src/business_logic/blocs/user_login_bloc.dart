


import 'package:circular/src/data/models/circular_event_model.dart';
import 'package:circular/src/data/models/circular_user_model.dart';
import 'package:circular/src/data/models/radio_model.dart';
import 'package:circular/src/data/repositories/circular_event_repository.dart';
import 'package:circular/src/data/repositories/circular_user_repository.dart';
import 'package:rxdart/rxdart.dart';

class UserLoginBloc{
  final _repository = CircularUserRepository();
  final _subject = BehaviorSubject<CircularUserModel>();

  fetchCircularUser(String user, String pass, String type ) async {
    CircularUserModel circularUserModel = await _repository.findUserLogin( user, pass, type );
    _subject.sink.add(circularUserModel);
  }


  dispose() {
    _subject.close();
  }

  BehaviorSubject<CircularUserModel> get subject => _subject;

}