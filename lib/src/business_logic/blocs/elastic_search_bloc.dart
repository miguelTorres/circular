import 'package:circular/src/data/models/circular_event_model.dart';
import 'package:circular/src/data/models/result_calendar_item.dart';
import 'package:circular/src/data/repositories/circular_event_repository.dart';
import 'package:rxdart/rxdart.dart';

class ElasticSearchBloc{
  final _repository = CircularEventRepository();
  final _circularEventSubject = BehaviorSubject<List<ResultCalendarItem>>();

  searchEventInElastic(String query) async {

    List<ResultCalendarItem> circularEventModel = await _repository.findEventsInElastic(query);
    _circularEventSubject.sink.add(circularEventModel);

  }

  dispose() {
    _circularEventSubject.close();
  }

  BehaviorSubject<List<ResultCalendarItem>> get circularEventSubject => _circularEventSubject;

}