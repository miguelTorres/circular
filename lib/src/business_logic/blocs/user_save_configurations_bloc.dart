import 'package:circular/src/data/models/circular_event_model.dart';
import 'package:circular/src/data/models/circular_user_model.dart';
import 'package:circular/src/data/repositories/circular_event_repository.dart';
import 'package:circular/src/data/repositories/circular_user_repository.dart';
import 'package:rxdart/rxdart.dart';

class UserSaveConfigurationsBloc{

  final _repository = CircularUserRepository();
  final _subject = BehaviorSubject<dynamic>();

  saveConfigurationByUser(int userUid, String username, String pass, String type, bool flagSuscription, bool flagFavorites) async {
    dynamic result = await _repository.updateConfigurationByUser( userUid, username, pass, type, flagSuscription, flagFavorites);
    _subject.sink.add(result);
  }

  dispose() {
    _subject.close();
  }

  BehaviorSubject<dynamic> get subject => _subject;

}