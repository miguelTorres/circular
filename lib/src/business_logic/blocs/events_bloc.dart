


import 'package:circular/src/data/models/circular_event_model.dart';
import 'package:circular/src/data/models/radio_model.dart';
import 'package:circular/src/data/repositories/circular_event_repository.dart';
import 'package:rxdart/rxdart.dart';

class EventsBloc{
  final _repository = CircularEventRepository();
  final _circularEventSubject = BehaviorSubject<List<CircularEventModel>>();

  fetchAllEvents() async {
    List<CircularEventModel> circularEventModel = await _repository.fetchAllCircularEvent();
    _circularEventSubject.sink.add(circularEventModel);
  }

  fetchEventsByFilters(List<RadioModel> filters) async {

    List<int> sedes = [];
    List<int> areas = [];
    List<int> tipos = [];
    bool allSpecials = false;
    String dateLimitType =  null;
    int size = null;

    filters.forEach((element) {

        if(element.type=="CAMPUS"){
          sedes.add(element.uid);
        }

        if(element.type=="AREA"){
          areas.add(element.uid);
        }

        if(element.type=="TYPE"){
          tipos.add(element.uid);
        }

        if(element.type=="SPECIAL"){
          allSpecials = true;
        }

        if(element.type=="DATE"){
          if(element.uid == 100){ //Eventos de hoy
            dateLimitType = "today";
          } else if(element.uid == 200){ //Eventos en los siguientes 7 dias

            dateLimitType = "week";

          } else if(element.uid == 300){ //Eventos en los siguientes 30 dias
            dateLimitType = "month";
          }
        }

    });


    List<CircularEventModel> circularEventModel = await _repository.findCircularEventByFilters(sedes, areas, tipos, size, allSpecials, dateLimitType);
    _circularEventSubject.sink.add(circularEventModel);
  }

  dispose() {
    _circularEventSubject.close();
  }

  BehaviorSubject<List<CircularEventModel>> get circularEventSubject => _circularEventSubject;

}