import 'dart:io';

import 'package:circular/src/presentation/templates/event_pages/event_detail_page.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'dart:async';

class PushNotification {
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  // ignore: close_sinks
  final _mensajesStreamController = StreamController<dynamic>.broadcast();
  Stream<dynamic> get mensajes => _mensajesStreamController.stream;


  initNotifications() {
    print(">>> voy a pedir el token!!!");

    //Pedimos permisos a firebase para usar push_notifications
    _firebaseMessaging.requestNotificationPermissions();
    //Solicitamos un token. Es unico para cada dispositivo
    _firebaseMessaging.getToken().then((token) {
      print("====  FCM TOKEN =====");
      print(token);
    });

    _firebaseMessaging.configure(

        //este metodo se llama cuando llega una notificación y la
        //app está abierta
        onMessage: (info) {
      print("====  FCM onMessage =====");
      print(info);
      if (Platform.isAndroid) {}
      if (Platform.isIOS) {}
    },

        //este metodo se llama cuando llega una notificación y la
        //app está en segundo plano
        onLaunch: (info) {
      print("====  FCM onLaunch =====");
      print(info);
    },

        //este metodo se llama cuando llega una notificación estando la app
        // en segundo plano, le da click sobre la push notification y se abre
        //la app
        onResume: (info) {
      print("====  FCM onRESUME =====");
      print(info);

      if (Platform.isAndroid) {
        if (info != null) {
          if (info['data'] != null) {

            if (info['data']['eventUid'] != null) {

              var eventUid = info['data']['eventUid'];

              _mensajesStreamController.sink.add(eventUid);

            }
          }
        }

      }

      if (Platform.isIOS) {}
    });
  }
}
