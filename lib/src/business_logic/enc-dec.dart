
//Contiene el proceso de cifrado para usuario y contrasenia

import 'dart:convert';

import 'package:encrypt/encrypt.dart';

String encrypt(String text){
  var date = DateTime.now();
  var today = new DateTime(date.year, date.month, date.day);

  final key = Key.fromUtf8('R2SwTPQVUvbB${base64.encode(utf8.encode(today.millisecondsSinceEpoch.toString()))}');
  final iv = IV.fromUtf8("RtIBovq3Xs8XkC81");//16 chars
  final encrypter = Encrypter(AES(key, mode: AESMode.cbc));
  final encrypted = encrypter.encrypt(text, iv: iv);

 // print(">>> ${text} : ${encrypted.base64}");

  return encrypted.base64;
}