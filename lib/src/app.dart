import 'package:circular/src/presentation/templates/event_pages/event_detail_page.dart';
import 'package:circular/src/presentation/templates/home.dart';
import 'package:circular/src/presentation/templates/preload.dart';
import 'package:circular/src/presentation/templates/splash.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:circular/src/business_logic/push_notifications.dart';

import 'business_logic/blocs/event_detail_bloc.dart';

class MyApp extends StatefulWidget {

  @override
  _MyAppState createState() => _MyAppState();
}


class _MyAppState extends State<MyApp>{
  final blocDetail = EventDetailBloc();
  final GlobalKey<NavigatorState> navigatorKey = new GlobalKey<NavigatorState>();


  @override
  void initState() {
    super.initState();

    final pushNotification = new PushNotification();
    pushNotification.initNotifications();



    pushNotification.mensajes.listen((eventUid) {


      if (eventUid != null) {

        blocDetail.getEventById(int.parse(eventUid));



        bool firstTime = true;

        blocDetail.circularEventSubject.stream.listen((event) {
          if (event != null) {


            if (firstTime) {
              firstTime = false;

              navigatorKey.currentState.push(
                  MaterialPageRoute(
                      builder: (context) =>
                          EventDetailPage(circularEvent: event)));
            }
          }
        });
      }
    });
  }

  @override
  void dispose() {
    blocDetail.dispose();
    this.dispose();
  }


  @override
  Widget build(BuildContext context) {
    return MaterialApp(

      debugShowCheckedModeBanner: false,

      routes: {
        '/home':(context) => Home(),
      },



      navigatorKey: navigatorKey,
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate
      ],
      supportedLocales: [
        const Locale('en'),
        const Locale('es'),
      ],

      theme: ThemeData(

        // Define el Brightness y Colores por defecto
        brightness: Brightness.dark,

        primaryColor: Color(0xffffcc5c), //Color del header
        accentColor: Color(0xffffcc5c), // color del float button

        // Define la Familia de fuente por defecto
        fontFamily: 'AncizarSans',

        textTheme: TextTheme(

          headline1: TextStyle(color: Colors.white),
          headline6: TextStyle(color: Colors.black),
          bodyText1: TextStyle(fontSize: 17.0), //items del menu
          bodyText2: TextStyle(color: Colors.white, fontSize: 17.0),
          button: TextStyle(fontSize: 17.0),
        ),
      ),
      home: Scaffold(
        backgroundColor: Color(0xffffcc5c),
        body: Splash(),
      ),
    );
  }


}
