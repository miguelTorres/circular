import 'package:circular/src/presentation/partials/event_partials/event_form.dart';
import 'package:flutter/material.dart';

class UserCreateEventPage extends StatefulWidget {
  @override
  _UserCreateEventPageState createState() => _UserCreateEventPageState();
}

class _UserCreateEventPageState extends State<UserCreateEventPage> {

  final _scaffoldKey = GlobalKey<ScaffoldState>();


  @override
  Widget build(BuildContext context) {
    return Container(
        child: Scaffold(
            key: _scaffoldKey,
            appBar: AppBar(
              leading: Builder(
                builder: (BuildContext context) {
                  return IconButton(
                    icon: const Icon(Icons.arrow_back_ios),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  );
                },
              ),
              centerTitle: true,
              title: Container(
                height: 23,
                child: Image.asset('assets/images/logoCircular.png',
                    fit: BoxFit.cover),
              ),
            ),
            body: Container(
              //color: Colors.white,
              child: EventForm("CREATE", null),

            )
        )
    );
  }
}
