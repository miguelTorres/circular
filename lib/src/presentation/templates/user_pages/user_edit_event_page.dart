import 'package:circular/src/data/models/circular_event_model.dart';
import 'package:circular/src/business_logic/blocs/load_event_bloc.dart';
import 'package:circular/src/presentation/partials/commons/loading_animation.dart';
import 'package:circular/src/presentation/partials/event_partials/event_form.dart';
import 'package:flutter/material.dart';

class UserEditEventPage extends StatefulWidget {
  int eventUid;

  UserEditEventPage(this.eventUid);

  @override
  _UserEditEventPageState createState() =>
      _UserEditEventPageState(this.eventUid);
}

class _UserEditEventPageState extends State<UserEditEventPage> {
  int eventUid;
  CircularEventModel circularEvent;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final bloc = LoadEventBloc();

  _UserEditEventPageState(this.eventUid);

  @override
  void initState() {
    bloc.getEventById(eventUid);

  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Scaffold(
            key: _scaffoldKey,
            appBar: AppBar(
              leading: Builder(
                builder: (BuildContext context) {
                  return IconButton(
                    icon: const Icon(Icons.arrow_back_ios),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  );
                },
              ),
              centerTitle: true,
              title: Container(
                height: 23,
                child: Image.asset('assets/images/logoCircular.png',
                    fit: BoxFit.cover),
              ),
            ),
            body: buildBody()));
  }

  Widget buildBody() {
    return Container(
        //color: Color(0xff636363),
        child: StreamBuilder(
            stream: bloc.circularEventSubject.stream,
            builder: (BuildContext context,
                AsyncSnapshot<CircularEventModel> snapshot) {
              Widget child;
              if (snapshot.hasData) {
                this.circularEvent = snapshot.data;

                child = EventForm("EDIT", circularEvent);
              } else if (snapshot.hasError) {
                child = Container(
                  child: Column(
                    children: <Widget>[
                      Icon(
                        Icons.error_outline,
                        color: Colors.red,
                        size: 60,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 16),
                        child: Text('Error: ${snapshot.error}'),
                      )
                    ],
                  ),
                );
              } else {
                child = Container(
                  color: Color(0xff636363),
                  child: LoadingAnimation(),
                );
              }

              return child;
            }));
  }
}
