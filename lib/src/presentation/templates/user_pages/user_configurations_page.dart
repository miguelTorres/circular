import 'package:circular/src/business_logic/blocs/user_configurations_bloc.dart';
import 'package:circular/src/business_logic/blocs/user_save_configurations_bloc.dart';
import 'package:circular/src/presentation/templates/user_pages/user_login_page.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserConfigurationsPage extends StatefulWidget {
  @override
  _UserConfigurationsPageState createState() => _UserConfigurationsPageState();
}

class _UserConfigurationsPageState extends State<UserConfigurationsPage> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  var bloc = UserConfigurationsBloc();
  var blocConfig = UserSaveConfigurationsBloc();
  bool _flagSuscription=false;
  bool _flagFavorites=false;

  int _userUid;
  String _username;
  String _pass;
  String _type;

  @override
  void initState() {
    super.initState();
    requestForEvents();
  }

  Future<void> requestForEvents() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    if (prefs.getInt("uid") != null) {
      _userUid = prefs.getInt("uid");
      _username = prefs.getString("username");
      _pass = prefs.getString("pass");
      _type = prefs.getString("type");

      bloc.getConfigurationByUser(_userUid, _username, _pass, _type);

      bloc.subject.stream.listen(
              (data) {

              setState(() {
                _flagFavorites = data["notificacionesFavoritos"];
                _flagSuscription = data["suscripcion"];
              });

              }
      );

    } else {
      //No hay sesión de usuario lo redirige

      Navigator.pop(context);
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => UserLoginPage()),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
        appBar: AppBar(
          leading: Builder(
            builder: (BuildContext context) {
              return IconButton(
                icon: const Icon(Icons.arrow_back_ios),
                onPressed: () {
                  Navigator.pop(context);
                },
              );
            },
          ),
          centerTitle: true,
          title:  Container(
            height: 23,
            child: Image.asset('assets/images/logoCircular.png',
                fit: BoxFit.cover),
          ),
        ),
        body: Container(
          child: Column(
            children: <Widget>[
              SwitchListTile(
                activeColor: Color(0xffffcc5c),
                title: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text('Suscripción'),
                    Text(
                      'Envia un email semanal con los eventos recientes',
                      style: TextStyle(fontSize: 13),
                    ),
                  ],
                ),
                value: _flagSuscription,
                onChanged: (bool valueSuscription) {
                  setState(() {
                    _flagSuscription = valueSuscription;
                  });
                },
                //secondary: const Icon(Icons.lightbulb_outline),
              ),
              SwitchListTile(
                activeColor: Color(0xffffcc5c),
                title: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text('Notificaciones de mis favoritos'),
                      Text(
                        'Envia un email 8 diás antes de iniciar un evento favorito',
                        style: TextStyle(fontSize: 13),
                      ),
                    ]),
                value: _flagFavorites,
                onChanged: (bool valueFavorites) {
                  setState(() {
                    _flagFavorites = valueFavorites;
                  });
                },
                //secondary: const Icon(Icons.lightbulb_outline),
              ),
              Center(
                child: RaisedButton(
                  color: Color(0xffffcc5c),
                  onPressed: () {

                    blocConfig.saveConfigurationByUser(_userUid, _username, _pass, _type, _flagSuscription, _flagFavorites);
                    blocConfig.subject.stream.listen((data) {

                      if (data != null) {

                        if (data["status"] != null && data["status"] == true) {

                          _displaySnackBar('Cambios guardados con éxito');
                        }else{
                          _displaySnackBar('No se han podido guardar cambios');
                        }
                      }else{
                          _displaySnackBar('No se han podido guardar cambios');
                      }
                    });
                  },
                  child: Text(
                    'Enviar',
                    style: TextStyle(color: Colors.black),
                  ),
                ),
              ),
            ],
          ),
        ));
  }

  _displaySnackBar(String msg){
    final snackbar = SnackBar(
        backgroundColor: Color(0xffffcc5c),
        content: Text(msg,
          style: TextStyle(
              color: Colors.black
          ),
        ));
    _scaffoldKey.currentState.showSnackBar(snackbar);
  }

}
