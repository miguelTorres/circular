import 'package:circular/src/business_logic/blocs/event_delete_bloc.dart';
import 'package:circular/src/business_logic/blocs/user_events_bloc.dart';
import 'package:circular/src/data/models/circular_event_model.dart';
import 'package:circular/src/presentation/partials/event_partials/item_event_partial.dart';
import 'package:circular/src/presentation/templates/event_pages/event_detail_page.dart';
import 'package:circular/src/presentation/templates/user_pages/user_create_event_page.dart';
import 'package:circular/src/presentation/templates/user_pages/user_edit_event_page.dart';
import 'package:circular/src/presentation/templates/user_pages/user_login_page.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserEventsPage extends StatefulWidget {
  @override
  _UserEventsPageState createState() => _UserEventsPageState();
}

class _UserEventsPageState extends State<UserEventsPage> {
  final bloc = UserEventsBloc();
  final blocDelete = EventDeleteBloc();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  int _userUid;
  String _username;
  String _pass;
  String _type;

  @override
  void initState() {
    super.initState();
    requestForEvents();
  }

  Future<void> requestForEvents() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    if (prefs.getInt("uid") != null) {
      _userUid = prefs.getInt("uid");
      _username = prefs.getString("username");
      _pass = prefs.getString("pass");
      _type = prefs.getString("type");

      bloc.getEventByUser(_userUid, _username, _pass, _type);
    } else {
      //No hay sesión de usuario lo redirige

      Navigator.pop(context);
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => UserLoginPage()),
      );
    }
  }

  @override
  void dispose() {
    bloc.dispose();
    blocDelete.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.arrow_back_ios),
              onPressed: () {
                Navigator.pop(context);
              },
            );
          },
        ),
        centerTitle: true,
        title: Container(
          height: 23,
          child:
              Image.asset('assets/images/logoCircular.png', fit: BoxFit.cover),
        ),
      ),
      body: Container(
        //color: Colors.white,
        child: StreamBuilder(
            stream: bloc.circularEventSubject.stream,
            builder: (BuildContext context,
                AsyncSnapshot<List<CircularEventModel>> snapshot) {
              Widget child;

              if (snapshot.hasData) {
                child = buildList(snapshot);
              } else if (snapshot.hasError) {
                child = Container(
                  child: Column(
                    children: <Widget>[
                      Icon(
                        Icons.error_outline,
                        color: Colors.red,
                        size: 60,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 16),
                        child: Text('Error: ${snapshot.error}'),
                      )
                    ],
                  ),
                );
              } else {
                child = Container(
                    //child: Text("en progreso..."),
                    );
              }

              return child;
            }),
      ),
      floatingActionButton: FloatingActionButton.extended(
        heroTag: "btnCreateEvent",
        icon: Icon(Icons.add),
        label: Text('Crear Evento'),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => UserCreateEventPage()),
          );
        },
      ),
    ));
  }

  Widget buildList(AsyncSnapshot<List<CircularEventModel>> snapshot) {
    var eventList = snapshot.data;

    return ListView.builder(
        padding: const EdgeInsets.all(8),
        itemCount: eventList.length,
        itemBuilder: (BuildContext context, int index) {
          return Container(
            margin: EdgeInsets.only(top: 20.0),
              height: MediaQuery.of(context).size.width,
              child: Stack(
                children: <Widget>[

                  ItemEventPartial(eventList[index], "BOTTOM", false),

                  Positioned(
                      right: MediaQuery.of(context).size.width / 3.5,
                      bottom: 30.0,
                      child: Row(
                        //mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            width: 50.0,
                            height: 50.0,
                            margin: EdgeInsets.all(5.0),
                            child: FloatingActionButton(
                                heroTag: 'btnEdit$index',
                                backgroundColor: (eventList[index].pid != 37)?Color(0xffffcc5c):Colors.grey,
                                child: Icon(
                                  Icons.edit,
                                  color: (eventList[index].pid != 37)?Color(0xff333333):Colors.grey[600],
                                ),
                                onPressed: () {
                                  if (eventList[index].pid != 37) {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              UserEditEventPage(
                                                  eventList[index].uid)),
                                    );
                                  } else {
                                    _displayEventeAceptedMsg();
                                  }
                                }),
                          ),
                          Container(
                            width: 50.0,
                            height: 50.0,
                            margin: EdgeInsets.all(5.0),
                            child: FloatingActionButton(

                              heroTag: 'btnDeleteEvent$index',
                              backgroundColor: (eventList[index].pid != 37)?Colors.red:Colors.grey,
                              child: Icon(
                                Icons.delete,
                                color: (eventList[index].pid != 37)?Colors.white:Colors.grey[600],
                              ),
                              onPressed: () {
                              if (eventList[index].pid != 37) {
                                  showDialog(
                                      context: context,
                                      builder: (_) =>
                                      new AlertDialog(
                                        backgroundColor: Color(0xffffcc5c),
                                        title: new Text(
                                            "¿Desea eliminar su evento?"),
                                        actions: <Widget>[
                                          FlatButton(
                                            child: Text('Si'),
                                            textColor: Colors.black,
                                            onPressed: () {
                                              Navigator.of(context).pop();

                                              blocDelete.deleteEvent(
                                                  _userUid,
                                                  _username,
                                                  _pass,
                                                  _type,
                                                  eventList[index].uid);
                                              blocDelete.subject.stream
                                                  .listen((data) {
                                                //TODO: si remueve de los favoritos, pero data llega en null, por revisar
                                                if (data != null) {
                                                  if (data["status"] !=
                                                      null &&
                                                      data["status"] ==
                                                          true) {
                                                    _displaySnackBar(
                                                        'Evento eliminado con éxito');
                                                  } else {
                                                    _displaySnackBar(
                                                        'No se han podido guardar cambios');
                                                  }
                                                } else {
                                                  _displaySnackBar(
                                                      'No se han podido guardar cambios');
                                                }

                                                bloc.getEventByUser(_userUid,
                                                    _username, _pass, _type);
                                              });
                                            },
                                          ),
                                          FlatButton(
                                            child: Text('No'),
                                            textColor: Colors.black,
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                            },
                                          ),
                                        ],
                                      ));
                                }else {
                                _displayEventeAceptedMsg();
                              }
                              },
                            ),
                          ),
                        ],
                      )),
                  Positioned(
                    bottom: 0.0,
                    left: 10.0,

                    child:  Container(
                      color: Color(0xffffcc5c),
                      child: Image(
                        height: 40,
                        image: AssetImage(
                            'assets/images/${getIconName(eventList[index].areaUid)}'
                        ),
                      ),

                    ),
                  )
                ],
              ));
        });
  }

  _displaySnackBar(String msg) {
    final snackbar = SnackBar(
        backgroundColor: Color(0xffffcc5c),
        content: Text(
          msg,
          style: TextStyle(color: Colors.black),
        ));
    _scaffoldKey.currentState.showSnackBar(snackbar);
  }

  _displayEventeAceptedMsg() {
    showDialog(
        context: context,
        builder: (_) => new AlertDialog(
              backgroundColor: Color(0xffffcc5c),
              title: new Text("Su evento ha sido aprobado"),
              content: new Text(
                  "Su evento ha sido aceptado para ser publicado. "
                  "Por lo tanto no podrá modificarlo ni eliminarlo. Si necesita "
                  "modificarlo por favor escriba a circluar_nal@unal.edu.co",

                style: TextStyle(
                    color: Colors.black
                ),
              ),
              actions: <Widget>[
                FlatButton(
                  child: Text('Continuar',
                  style: TextStyle(
                    color: Colors.black
                  ),
                  ),
                  textColor: Colors.black,
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ));
  }

  //id: int - ID de la categoria
  String getIconName(id) {

    switch (id) {
    //area y cultura
      case 4:
        {
          return "areas-04.png";
        }
        break;

    //Ciencia y tecnologia
      case 5:
        {
          return "areas-07.png";
        }
        break;

    //Desarrollo rural
      case 6:
        {
          return "areas-07.png";
        }
        break;

    //Economia y organizaciones
      case 7:
        {
          return "areas-03.png";
        }
        break;

    //Educación
      case 8:
        {
          return "areas-05.png";
        }
        break;

    //Educación
      case 10:
        {
          return "areas-11.png";
        }
        break;

    //Politica y sociedad
      case 11:
        {
          return "areas-09.png";
        }
        break;

    //Salud
      case 12:
        {
          return "areas-10.png";
        }
        break;

    //Recreación y deporte
      case 13:
        {
          return "areas-06.png";
        }
        break;

    //Medio ambiente
      case 14:
        {
          return "areas-08.png";
        }
        break;

    //Ciudad y territorio
      case 31:
        {
          return "areas-02.png";
        }
        break;

      default:
        {
          //statements;
        }
        break;
    }
  }

}
