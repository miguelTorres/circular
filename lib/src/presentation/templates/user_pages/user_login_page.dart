
import 'package:circular/src/business_logic/blocs/user_login_bloc.dart';
import 'package:circular/src/presentation/templates/home.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

class UserLoginPage extends StatefulWidget {
  @override
  _State createState() => _State();
}

class _State extends State<UserLoginPage> with SingleTickerProviderStateMixin {
  TabController _controller;

  @override
  void initState() {
    super.initState();
    _controller = new TabController(length: 1, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        //resizeToAvoidBottomPadding: true,
        appBar: AppBar(
          leading: Builder(
            builder: (BuildContext context) {
              return IconButton(
                icon: const Icon(Icons.arrow_back_ios),
                onPressed: () {
                  Navigator.pop(context);
                },
              );
            },
          ),
          centerTitle: true,
          title: Container(
            height: 23,
            child: Image.asset('assets/images/logoCircular.png',
                fit: BoxFit.cover),
          ),
        ),
        body: Container(
          color: Color(0xffffcc5c),
          margin: EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
          child: new ListView(
            children: <Widget>[
              new Container(
                height: 60.0,
                decoration: new BoxDecoration(color: Color(0xff333333)),
                child: new TabBar(
                  controller: _controller,
                  tabs: [
                    Text(
                      "INICIO DE SESIÓN",
                      style: TextStyle(
                        fontSize: 14.0,
                        fontWeight: FontWeight.bold,
                        color: Color(0xffffcc5c),
                      ),
                    ),

                    /*Text(
                      "Externo",
                      style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                        color: Color(0xffffcc5c),
                      ),
                    ),*/

                  ],
                ),
              ),
              new Container(
                height: MediaQuery.of(context).size.height,
                child: new TabBarView(
                  controller: _controller,
                  children: <Widget>[
                    new Container(
                      margin: EdgeInsets.all(10.0),
                      child: MyCustomForm(0),
                    ),
                    /*
                    new Container(
                      margin: EdgeInsets.all(10.0),
                      child: MyCustomForm(1),
                    ),*/
                  ],
                ),
              ),
            ],
          ),
        ));
  }
}

// Crea un Widget Form
class MyCustomForm extends StatefulWidget {
  int indexType;

  MyCustomForm(this.indexType);

  @override
  MyCustomFormState createState() {
    return MyCustomFormState(indexType);
  }
}

// Crea una clase State correspondiente. Esta clase contendrá los datos relacionados con
// el formulario.
class MyCustomFormState extends State<MyCustomForm> {
  // 0:indica que es usuaurio unal
  // 1:indica que es usuaurio externo
  int indexType;

  // Crea una clave global que identificará de manera única el widget Form
  // y nos permita validar el formulario
  //
  // Nota: Esto es un GlobalKey<FormState>, no un GlobalKey<MyCustomFormState>!
  final _formKey = GlobalKey<FormState>();
  SharedPreferences prefs;
  String _user;
  String _pass;
  String _passEncode;
  List<String> _types = ["Unal", "Externos"];
  String _userType;
  final bloc = UserLoginBloc();

  MyCustomFormState(this.indexType);

  @override
  void initState() {
    _userType = _types[indexType];
    super.initState();
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  List<DropdownMenuItem<String>> getDropdownMenuItems() {
    List<DropdownMenuItem<String>> items = new List();
    for (String type in _types) {
      items.add(new DropdownMenuItem(value: type, child: new Text(type)));
    }

    return items;
  }

  @override
  Widget build(BuildContext context) {
    // Crea un widget Form usando el _formKey que creamos anteriormente
    return getLoginForm();
  }

  Widget getLoginForm() {
    return Form(
        key: _formKey,
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
            Widget>[
          Column(
            children: <Widget>[
              TextFormField(
                style: TextStyle(color: Colors.black),
                decoration: InputDecoration(
                    labelText: (indexType == 0)
                        ? "Usuario (sin @unal.edu.co)"
                        : "e-mail",
                    labelStyle: TextStyle(color: Colors.black)),
                onChanged: (text) {
                  _user = text;
                },
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Ingrese un texto';
                  }
                  return null;
                },
              ),
              TextFormField(
                obscureText: true,
                style: TextStyle(color: Colors.black),
                decoration: InputDecoration(
                    labelText: "Contraseña",
                    labelStyle: TextStyle(color: Colors.black)),
                onChanged: (text) {
                  _pass = text;
                },
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Ingrese un texto';
                  }
                  return null;
                },
              ),
              if (indexType == 1)
                Center(
                    child: FlatButton(
                  onPressed: () async {},
                  child: Text(
                    'Registrate',
                    style: TextStyle(decoration: TextDecoration.underline),
                  ),
                )),
              if (indexType == 1)
                Center(
                    child: FlatButton(
                  onPressed: () async {},
                  child: Text(
                    '¿Olvidaste tu contraseña?',
                    style: TextStyle(decoration: TextDecoration.underline),
                  ),
                )),
              Center(
                child: RaisedButton(
                  color: Colors.black,
                  onPressed: () async {
                    // devolverá true si el formulario es válido, o falso si
                    // el formulario no es válido.
                    if (_formKey.currentState.validate()) {
                      _passEncode = base64.encode(utf8.encode(_pass));
                      //_passEncode = _pass;

                      bloc.fetchCircularUser(
                          _user, _passEncode, _userType.toLowerCase());
                      bloc.subject.stream.listen((circularUser) async {
                        if (circularUser != null) {
                          //Guardar el usuario en la memoria local (preferencias)
                          prefs = await SharedPreferences.getInstance();
                          prefs.setInt("uid", circularUser.uid);
                          prefs.setString("name", circularUser.name);
                          prefs.setString("username", circularUser.username);
                          prefs.setString("type", circularUser.type);
                          prefs.setString("mail", circularUser.mail);
                          prefs.setString("pass", circularUser.pass);


                          showDialog(
                              context: context,
                              builder: (_) => new AlertDialog(
                                backgroundColor: Color(0xffffcc5c),
                                title: new Text(
                                    "Ingreso con éxito. Bienvenido"),
                                actions: <Widget>[
                                  FlatButton(
                                    child:
                                    Text('Continuar',
                                      style: TextStyle(
                                          color: Colors.black
                                      ),),
                                    //color: Colors.black,
                                    onPressed: () {
                                      Navigator.of(context).pop(); // Cierra popup de confirmación
                                      Navigator.of(context).pop(); // Cierra pagina de login
                                      Navigator.of(context).pop(); // Cierra El Home
                                      //Redirige al Home para que se actualice estado del menù


                                      Navigator.pushNamed(context, '/home');

                                    },
                                  ),

                                ],
                              ));



                        } else {
                          Scaffold.of(context).showSnackBar(SnackBar(
                              content: Text(
                                  'Usuario o contraseña inválida')));
                        }
                      });
                    }
                  },
                  child: Text('Enviar'),
                ),
              ),
            ],
          )
        ]));
  }
}
