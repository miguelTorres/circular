
import 'package:circular/src/presentation/templates/home.dart';
import 'package:circular/src/presentation/templates/preload.dart';
import 'package:flutter/material.dart';

class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {

  _SplashState(){
    Future.delayed(const Duration(milliseconds: 4000),(){
      Navigator.push(context,
          MaterialPageRoute(
              builder: (context) => Preload()
          ));
    });
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
    body:

      Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      color: Color(0xffffcc5c),
      child: Center(
        child: Image(
          image: AssetImage('assets/images/splash_image.png'),
          width: MediaQuery.of(context).size.width*0.5,
        )
      )
      ),

    );
  }

}
