import 'dart:convert';
import 'dart:io';

import 'package:circular/src/data/models/museum_model.dart';
import 'package:circular/src/presentation/templates/event_pages/event_map_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:share/share.dart';


class MuseumDetailPage extends StatefulWidget {

  MuseumModel museum;

  MuseumDetailPage({this.museum});

  @override
  _MuseumDetailPageState createState() => _MuseumDetailPageState(this.museum);
}

class _MuseumDetailPageState extends State<MuseumDetailPage> {

  MuseumModel museum;
  String museumName = "";
  String museumUrl = "";

  _MuseumDetailPageState(museum){
    this.museum = museum;
    this.museumName = museum.name;
    this.museumUrl = museum.url;
  }

  GoogleMapController mapController;

  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.arrow_back_ios),
              onPressed: () {
                Navigator.pop(context);
              },
            );
          },
        ),
        centerTitle: true,
        title:
        Container(
          height: 23,
          child:
          Image.asset('assets/images/logoCircular.png', fit: BoxFit.cover),
        ),
        backgroundColor: Color(0xffffcc5c),
        actions: <Widget>[
          IconButton(
            color: Colors.black,
            icon: Icon(Icons.filter_list),
            onPressed: () {},
          ),
          // action button
          IconButton(
            color: Colors.black,
            icon: Icon(Icons.notifications_none),
            onPressed: () {},
          ),
          IconButton(
            color: Colors.black,
            icon: Icon(Icons.info_outline),
            onPressed: () {},
          )
        ],
      ),
      body: Container(
          color: Colors.grey[800],
          child: buildDetail(museum)
      ),
      floatingActionButton: FloatingActionButton(
        heroTag: 'btnShareMuseum',
        backgroundColor: Color(0xffffcc5c),
        child:Icon(Icons.share),
        onPressed: (){

          Share.share("$museumName $museumUrl", subject: 'Circular Unal: $museumName');

        },
      ),
    );
  }

  Widget buildDetail(MuseumModel museum) {


    LatLng _center = LatLng(museum.circularLocation.latitude,
        museum.circularLocation.longitude);

    List<Widget> list = [
      Container(
        padding:
        EdgeInsets.only(left: 20.0, right: 20.0, top: 20.0, bottom: 20.0),
        child: Align(
          alignment: Alignment.centerLeft,
          child: Text(
            museum.name,
            style: TextStyle(
                fontSize: 23.0,
                color: Colors.white,
                fontWeight: FontWeight.bold),
          ),
        ),
      ),
      Container(
        color: Color(0xffffcc5c),
        child: Row(
          children: <Widget>[
            Expanded(
              child: Container(
                  padding: EdgeInsets.only(right: 10.0, left: 10.0),
                  child: Column(
                    children: <Widget>[
                      Text(
                        museum.address,
                        textAlign: TextAlign.right,
                        style: TextStyle(
                            fontStyle: FontStyle.italic,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  )),
            ),
            Container(
              color: Color(0xff333333),

              child: SizedBox(
                  width: 120, // or use fixed size like 200
                  height: 100,
                  child:
                  IconButton(icon: Icon(Icons.location_on, size: 50.0,
                  ),
                      onPressed: (){
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => EventMapPage(
                                    museum.circularLocation)));
                      }
                  )
              ),
            )          ],
        ),
      ),
      if(museum.description!=null)
        Container(
          padding: EdgeInsets.all(15.0),
          child: Html(
            data: museum.description,
          ),
        ),

      if(museum.phone!=null)
        getItemInfo("Teléfonos", museum.phone),
      if(museum.email!=null)
        getItemInfo("e-mail", museum.email),
      if(museum.moreInformation!=null)
        Container(
          padding: EdgeInsets.all(20.0),

          child:

          Column(
            children: <Widget>[
              Align(
                alignment: Alignment.centerLeft,
                child: Text("Más información",
                  style: TextStyle(
                    color: Color(0xffffcc5c),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.centerLeft,
                child:  Html(
                  data: museum.moreInformation,
                ),
              ),

            ],
          )
        ),

      if(museum.officeHours!=null)
        getItemInfo("Horario de atención", museum.officeHours),
      if(museum.campus!=null)
        getItemInfo("Dependencia", museum.campus),

    ];


    return Container(
        child: Stack(
          children: <Widget>[
            Image.memory(
              base64Decode(museum.imgBase64),
              fit: BoxFit.cover,
              height: double.infinity,
              width: double.infinity,
              alignment: Alignment.center,
            ),
            Positioned(
                top: 0.0,
                child: Container(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    color: Color(0x00333333).withOpacity(0.9),
                    child:


                    SingleChildScrollView(
                        scrollDirection: Axis.vertical,
                        child:
                        Container(
                          margin: EdgeInsets.only(bottom: 200.0),
                        child:
                        Column(
                          children: list,

                        ))
                    )
                )
            )
          ],

        )
    );
  }


  Widget getItemInfo(String label, dynamic info){
    return Container(
      padding: EdgeInsets.only(left: 20.0, top: 10.0),
      child:
      Column(
        children: <Widget>[
          Align(
            alignment: Alignment.centerLeft,
            child: Text(label,
              style: TextStyle(
                color: Color(0xffffcc5c),
              ),
            ),
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: Text(info),
          ),
        ],
      ),
    );
  }

}
