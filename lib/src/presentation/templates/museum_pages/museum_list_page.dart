import 'dart:convert';
import 'dart:math';

import 'package:circular/src/business_logic/blocs/museum_list_bloc.dart';
import 'package:circular/src/data/models/museum_model.dart';
import 'package:circular/src/presentation/partials/museum_partials/item_museum_partial.dart';
import 'package:circular/src/presentation/templates/museum_pages/museum_detail_page.dart';
import 'package:flutter/material.dart';

class MuseumListPage extends StatefulWidget {
  @override
  _MuseumListPageState createState() => _MuseumListPageState();
}

class _MuseumListPageState extends State<MuseumListPage> {

  final bloc = MuseumListBloc();
  GlobalKey _bodyMuseumKey = GlobalKey();


  @override
  void initState() {
    super.initState();
    bloc.fetchAllMuseums();
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Container(
        key: _bodyMuseumKey,
        height: MediaQuery.of(context).size.height,
        child: StreamBuilder(
            stream: bloc.museumEventSubject.stream,
            builder: (BuildContext context,
                AsyncSnapshot<List<MuseumModel>> snapshot) {


              Widget child;
              if (snapshot.hasData) {


                child = buildList(snapshot);
              } else if (snapshot.hasError) {
                child = Container(
                  child: Column(
                    children: <Widget>[
                      Icon(
                        Icons.error_outline,
                        color: Colors.red,
                        size: 60,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 16),
                        child: Text('Error: ${snapshot.error}'),
                      )
                    ],
                  ),
                );
              } else {
                child = Container(
                  //child: Text("en progreso..."),
                );
              }

              return child;
            }));

  }

  //Genera la estructura de la lista en fomra de linea de tiempo
  Widget buildList(AsyncSnapshot<List<MuseumModel>> snapshot) {
    var eventList = snapshot.data;


    List<Widget> listWidget = [];

    // >> Variables  que indican la posicion de cada tarjeta en la linea de tiempo
    int _countAdjust = 0;
    double _heightCard = (_getBodySizes()/2)+3;
    double _widthCard = MediaQuery.of(context).size.width * 0.5;
    double _marginLeftUpCount = (_widthCard) + 0.0;
    double _marginLeftDownCount = (_widthCard) + (_widthCard / 3);

    //Agrega el ancho del listado
    listWidget.add(Container(
      width: ((eventList.length + 4) * _widthCard) / 2,
    ));

    //Se agrega la linea de tiempo central
    listWidget.add(Center(
        child: Container(
          color: Colors.grey,
          width: ((eventList.length + 4) * _widthCard) / 2,
          height: 1,
          child: Text(""),
        )));

    //Agrega imagen principal del home
    listWidget.add(
      Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width / 2,
        child: Image.asset(
          'assets/images/portadacircular.jpg',
          fit: BoxFit.cover,
        ),
      ),
    );

    //Agraga mensaje d el a imagen principal del home
    listWidget.add(Positioned(
      child: Container(
        margin: EdgeInsets.only(left: 5.0),
        padding: EdgeInsets.all(5.0),
        color: Color(0xff333333),
        width: MediaQuery.of(context).size.width * 0.47,
        child: Text(
          "Agenda Nacional de eventos y actividades académicas. \n\n Universidad Nacional de Colomba",
          textAlign: TextAlign.center,
        ),
      ),
    ));

    //Agrega texto de Museos
    listWidget.add(

        Container
        (
            width: MediaQuery.of(context).size.width / 2,

            child:
        Center(
      child: Row(
        children: <Widget>[
          Expanded(
            child:
          Container(
              color: Color(0xffffcc5c),
              child: Text(
                "Museos",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 20.0,
                  color: Color(0xff333333),
                ),
              ))),
          Container(
            child: Image(
              image: AssetImage(
                'assets/images/triangulo.png',
              ),
            ),
          ),
        ],
      ),
    )
    )
    );

    //Agrega el listado de eventos
    eventList.asMap().forEach((i, event) {
      if (_countAdjust % 2 == 0 && _countAdjust > 0) {
        _marginLeftUpCount += _widthCard;
        _marginLeftDownCount += _widthCard;
      }

      //Si es par, acomoda el evento arriba
      if (i % 2 == 0) {
        listWidget.add(Positioned(
            bottom: 0.0,
            child: Container(
              margin: EdgeInsets.only(left: _marginLeftUpCount),
              height: _heightCard,
              width: _widthCard,
              child: buildItemMuseumList(event, 'DOWN'),
            )));
      } else {
        //Si el indice es impar, acomoda el evento abajo

        listWidget.add(Positioned(
            child: Container(
              //padding: EdgeInsets.only(bottom: 5.0),

              margin: EdgeInsets.only(left: _marginLeftDownCount),
              height: _heightCard,
              width: _widthCard,
              child: buildItemMuseumList(event, 'UP'),
            )));
      }

      _countAdjust++;
    });

    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Stack(children: listWidget),
    );
  }

  //Retorna un Card de evento para ser pintado en la lista
  //position: "UP", "DOWN"
  Widget buildItemMuseumList(MuseumModel museum, String position) {
    return Stack(
      children: <Widget>[
        Container(
          padding: (position == "UP")
              ? EdgeInsets.only(bottom: 12.0)
              : EdgeInsets.only(top: 12.0),
          child: InkWell(
            child:
            ItemMuseumPartial(museum, (position == "UP") ? "BOTTOM" : "TOP"),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>
                          MuseumDetailPage(museum: museum)
                  )
              );
            },
          ),
        ),

        //Si el Item (card) esta en la parte superior, el icono coloca en la parte inferior
        if (position == "UP")
          Positioned(
            bottom: 0.0,
            child: Container(
                margin: EdgeInsets.only(left: 10.0),
                child: Column(children: <Widget>[
                  Container(
                    child: Image(
                      height: 30,
                      image: AssetImage(
                          'assets/images/home-areas-11-up.png'),
                    ),

                  )
                ])),
          ),

        //Si el Item (card) esta en la parte inferio, el icono se ubica en la parte superior
        if (position == "DOWN")
          Positioned(
            top: 0.0,
            child: Container(
                margin: EdgeInsets.only(left: 10.0),
                child: Column(children: <Widget>[
                  Container(
                    child: Image(
                      height: 30,
                      image: AssetImage(
                          'assets/images/home-areas-11-down.png'),
                    ),
                  ),
                ])),
          ),
      ],
    );
  }




  double _getBodySizes() {
    final RenderBox renderBoxRed = _bodyMuseumKey.currentContext.findRenderObject();
    final sizeRed = renderBoxRed.size;
    return sizeRed.height;
  }


}
