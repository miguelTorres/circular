import 'package:circular/src/business_logic/blocs/event_detail_bloc.dart';
import 'package:circular/src/presentation/partials/commons/loading_animation.dart';
import 'package:flutter/material.dart';

import 'event_detail_page.dart';


class PreloadDetail extends StatefulWidget {

  int eventUid;


  PreloadDetail(this.eventUid);

  @override
  _PreloadDetailState createState() => _PreloadDetailState(eventUid);
}

class _PreloadDetailState extends State<PreloadDetail> {

 int eventUid;
  final blocDetail = EventDetailBloc();


 _PreloadDetailState(this.eventUid);


 @override
  void initState() {

   blocDetail.getEventById(eventUid);

   blocDetail.circularEventSubject.stream.listen((event) {

     if (event != null) {

       //Navigator.pop(context);
       Navigator.push(
           context,
           MaterialPageRoute(
               builder: (context) =>
                   EventDetailPage(circularEvent: event)));
     }
   }

   );



 }


 @override
  void dispose() {

   blocDetail.dispose();

 }

  @override
  Widget build(BuildContext context) {

    return  Container(
      color: Color(0xff666666),
      child: LoadingAnimation(),
    );
  }
}
