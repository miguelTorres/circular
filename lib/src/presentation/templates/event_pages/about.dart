import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class About extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.arrow_back_ios),
              onPressed: () {
                Navigator.pop(context);
              },
            );
          },
        ),
        centerTitle: true,
        title: Container(
          height: 23,
          child:
              Image.asset('assets/images/logoCircular.png', fit: BoxFit.cover),
        ),
      ),
      body: SingleChildScrollView(
          child: Container(
        padding: EdgeInsets.all(20.0),
        child: Column(
          children: <Widget>[
            Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                child: Text(
                  'Acerca de CircularUNAL',
                  style: TextStyle(
                      color: Color(0xffffcc5c),
                      fontSize: 35.0,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                child: Text(
                  'Versión: 3.0.0',
                  style: TextStyle(
                    color: Color(0xffffcc5c),
                  ),
                ),
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                child: Text(
                  'Política de privacidad',
                  style: TextStyle(
                      color: Color(0xffffcc5c),
                      fontSize: 25.0,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
            Text(
                'CircularUNAL usa información almacenada en el dispositivo donde se encuentra instalada. Esta información consiste en:\n'),
            Container(
              margin: EdgeInsets.only(left: 20.0),
                child: Column(children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text("• "),
                  Expanded(
                    child: Text('Estado del dispositivo: activo, inactivo.'),
                  ),
                ],
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text("• "),
                  Expanded(
                    child: Text(
                        'Estado de conexión a internet, puede usar datos del dispositivo.'),
                  ),
                ],
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text("• "),
                  Expanded(
                    child: Text(
                        'Registro de la consulta de eventos a través de la aplicación.'),
                  ),
                ],
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text("• "),
                  Expanded(
                    child: Text(
                        'Tokens (identificadores generados para la aplicación móvil, que no corresponden al ID de cada dispositivo) para la funcionalidad de notificaciones.'),
                  ),
                ],
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text("• "),
                  Expanded(
                    child: Text(
                        'Preferencias de los usuarios acerca de los eventos (para notificaciones).'),
                  ),
                ],
              ),
            ])),
            Text(
                '\nEsta información es recopilada en la plataforma Firebase, siguiendo los términos de servicio de las API de Google.'),
            Text('\nEsta información es usada con los siguientes fines:\n'),
            Container(
                margin: EdgeInsets.only(left: 20.0),
                child: Column(children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text("• "),
                      Expanded(
                        child: Text(
                            'Advertir sobre nuevos eventos o posibles cambios en los mismos.'),
                      ),
                    ],
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text("• "),
                      Expanded(
                        child: Text(
                            'Llevar estadísticas de los eventos vistos a través de la aplicación.'),
                      ),
                    ],
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text("• "),
                      Expanded(
                        child: Text(
                            'Ofrecer un servicio de notificaciones con base en las preferencias del usuario.'),
                      ),
                    ],
                  ),
                ])),
            Text(
                '\nParte de la información almacenada en Firebase podría usarse en el futuro con fines estadísticos sobre el uso de la aplicación.'),
            Text(
                '\nEn ningún caso se usará la información recopilada con fines comerciales que puedan beneficiar a terceros; tampoco se entregará a personas o entidades externas a la Universidad Nacional de Colombia.'),
            Text(
                '\nSe hace uso de los servicios de localización de cada dispositivo para indicar la ubicación de un evento seleccionado. Las ubicaciones de los dispositivos no son recopiladas; sin embargo, puede almacenarse en la plataforma gestora de mapas del dispositivo (Google Maps) si se tiene habilitada la opción correspondiente.'),
            Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                child: Text(
                  'Propiedad intelectual',
                  style: TextStyle(
                      color: Color(0xffffcc5c),
                      fontSize: 25.0,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
            new RichText(
              text: new TextSpan(
                style: new TextStyle(
                  fontSize: 16.0,
                  fontFamily: 'AncizarSans'

                  ),


            children: <TextSpan>[
                  new TextSpan(
                      text:
                          'Los contenidos (imágenes, textos y videos) provistos a través de '),
                  new TextSpan(
                      text: 'CircularUNAL',
                      style: new TextStyle(fontStyle: FontStyle.italic)),
                  new TextSpan(
                      text:
                          'pertenecen a la Universidad Nacional de Colombia, de conformidad con el Acuerdo 035 de 2003 del Consejo Académico, “Por el cual se expide el reglamento sobre Propiedad Intelectual en la Universidad Nacional de Colombia”'),
                ],
              ),
            ),
            Text(
                '\nTono de notificación: Autor: Pedro Santiago Murillo Moreno. Instrumento: Simbiófono colombiano del maestro Pedro A. Murillo S. Todos los derechos reservados.'),
            Divider(
              color: Colors.white,
            ),
            Container(
              margin: EdgeInsets.only(left: 20.0),
              child: Column(children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("• "),
                    Expanded(
                        child: new RichText(
                      text: new TextSpan(

                         style: new TextStyle(
                              fontSize: 16.0,
                              fontFamily: 'AncizarSans'

                          ),

                        children: <TextSpan>[

                          new TextSpan(
                              text: 'Director Nacional de Unimedios: ',
                              style:
                                  new TextStyle(fontWeight: FontWeight.bold)),

                          new TextSpan(
                              text: 'Fredy Fernando Chaparro Sanabria'),
                        ],
                      ),
                    )),
                  ],
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("• "),
                    Expanded(
                        child: new RichText(
                      text: new TextSpan(
                        children: <TextSpan>[
                          new TextSpan(
                              text: 'Idea conceptual: ',
                              style:
                                  new TextStyle(fontWeight: FontWeight.bold)),
                          new TextSpan(text: 'Jaime Franky Rodríguez'),
                        ],
                      ),
                    )),
                  ],
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("• "),
                    Expanded(
                        child: new RichText(
                      text: new TextSpan(
                        // Note: Styles for TextSpans must be explicitly defined.
                        // Child text spans will inherit styles from parent

                        children: <TextSpan>[
                          new TextSpan(
                              text:
                                  'Diseño, desarrollo, implementación y normativa para web: ',
                              style:
                                  new TextStyle(fontWeight: FontWeight.bold)),
                          new TextSpan(
                              text: 'Oficina de Medios Digitales - Unimedios'),
                        ],
                      ),
                    )),
                  ],
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("• "),
                    Expanded(
                        child: new RichText(
                      text: new TextSpan(
                        // Note: Styles for TextSpans must be explicitly defined.
                        // Child text spans will inherit styles from parent

                        children: <TextSpan>[
                          new TextSpan(
                              text:
                                  'Dirección de la Oficina de Medios Digitales: ',
                              style:
                                  new TextStyle(fontWeight: FontWeight.bold)),
                          new TextSpan(text: 'Martha Lucía Chaves Muñoz'),
                        ],
                      ),
                    )),
                  ],
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("• "),
                    Expanded(
                        child: new RichText(
                      text: new TextSpan(
                        // Note: Styles for TextSpans must be explicitly defined.
                        // Child text spans will inherit styles from parent

                        children: <TextSpan>[
                          new TextSpan(
                              text: 'Ingeniero de desarrollo: ',
                              style:
                                  new TextStyle(fontWeight: FontWeight.bold)),
                          new TextSpan(text: 'Miguel Andrés Torres Chavarro'),
                        ],
                      ),
                    )),
                  ],
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("• "),
                    Expanded(
                        child: new RichText(
                      text: new TextSpan(
                        // Note: Styles for TextSpans must be explicitly defined.
                        // Child text spans will inherit styles from parent

                        children: <TextSpan>[
                          new TextSpan(
                              text: 'Apoyo en desarrollo: ',
                              style:
                                  new TextStyle(fontWeight: FontWeight.bold)),
                          new TextSpan(
                              text:
                                  'César Hansy Dueñas Barragán, Giovanni Romero Pérez'),
                        ],
                      ),
                    )),
                  ],
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("• "),
                    Expanded(
                        child: new RichText(
                      text: new TextSpan(
                        // Note: Styles for TextSpans must be explicitly defined.
                        // Child text spans will inherit styles from parent

                        children: <TextSpan>[
                          new TextSpan(
                              text: 'Diseño gráfico en conjunto: ',
                              style:
                                  new TextStyle(fontWeight: FontWeight.bold)),
                          new TextSpan(
                              text:
                                  'Cristian Orlando Bogotá Rodríguez y Lina Margarita Otálora Melo'),
                        ],
                      ),
                    )),
                  ],
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("• "),
                    Expanded(
                        child: new RichText(
                      text: new TextSpan(
                        children: <TextSpan>[
                          new TextSpan(
                              text: 'Webmaster: ',
                              style:
                                  new TextStyle(fontWeight: FontWeight.bold)),
                          new TextSpan(
                              text: 'Francisco Javier Morales  Ducuara, Aldemar Hernandez Torres'),
                        ],
                      ),
                    )),
                  ],
                ),
              ]),
            ),
            Align(
                alignment: Alignment.centerLeft,
                child: Text('\nPublicado por la Oficina de Medios Digitales')),
            Align(
                alignment: Alignment.centerLeft,
                child: Text('\nUnidad de Medios de Comunicación – Unimedios')),
            Align(
                alignment: Alignment.centerLeft,
                child: InkWell(
                    onTap: () {
                      launch('mailto:mediosdigitales@unal.edu.co');
                    },
                    child: Text(
                      '\nmediosdigitales@unal.edu.co',
                      style: new TextStyle(color: Color(0xffffcc5c)),
                    ))),
            Align(
                alignment: Alignment.centerLeft,
                child: Text('\nPBX: (1) 316 5000 ext. 18280-18120')),
            Align(
                alignment: Alignment.centerLeft,
                child: Text('\nUniversidad Nacional de Colombia')),
          ],
        ),
      )),
    );
  }
}
