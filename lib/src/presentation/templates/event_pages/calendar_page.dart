import 'package:calendarro/default_day_tile.dart';
import 'package:circular/src/business_logic/blocs/calendar_events_bloc.dart';
import 'package:circular/src/business_logic/blocs/event_detail_bloc.dart';
import 'package:circular/src/data/models/circular_event_model.dart';
import 'package:circular/src/data/models/result_calendar_item.dart';
import 'package:flutter/material.dart';
import 'package:calendarro/date_utils.dart';
import 'package:calendarro/calendarro.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';

import 'event_detail_page.dart';

class CalendarPage extends StatefulWidget {
  @override
  _CalendarPageState createState() => _CalendarPageState();
}

class _CalendarPageState extends State<CalendarPage> {
  final String title = "";
  Calendarro monthCalendarro;
  String currentMonthText = "";
  var bloc = CalendarEventBloc();
  final blocDetail = EventDetailBloc();
  var redrawObject;

  Stream _blocDetailStream;

  _CalendarPageState() {
  }

  @override
  void initState() {
    _blocDetailStream = blocDetail.circularEventSubject.stream;

    _initializeCalendar();

    super.initState();
  }

  @override
  void dispose() {
    bloc.dispose();
    blocDetail.dispose();
    this.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: AppBar(
          leading: Builder(
            builder: (BuildContext context) {
              return IconButton(
                icon: const Icon(Icons.arrow_back_ios),
                onPressed: () {
                  Navigator.pop(context);
                },
              );
            },
          ),
          centerTitle: true,
          title: Container(
            height: 23,
            child: Image.asset('assets/images/logoCircular.png',
                fit: BoxFit.cover),
          ),
        ),
        body: Container(
            //color: Colors.white,
            child: Column(
          //key: UniqueKey(),
          children: <Widget>[
            Container(
              color: Color(0xff333333),
              child: Row(children: [
                //Nota: este icono se agrega para que la fecha quede centrda, Pero no hace nada
                IconButton(
                  icon: Icon(Icons.event, color: Color(0xff333333)),
                ),

                Expanded(
                    child: Center(
                        child: Container(
                  margin: EdgeInsets.all(5.0),
                  child: Text(
                    "$currentMonthText".toUpperCase(),
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: Color(0xffffcc5c)),
                  ),
                ))),
                IconButton(
                    icon: Icon(Icons.event, color: Color(0xffffcc5c)),
                    onPressed: () {
                      _initializeCalendar();
                      setState(() {
                        redrawObject = Object();
                      });
                    })
              ]),
            ),
            Container(
              //margin: EdgeInsets.only(top: 4.0, left: 5.0, right: 5.0, bottom: 5.0),
              padding: EdgeInsets.only(
                  top: 20.0, left: 20.0, right: 20.0, bottom: 20.0),
              color: Color(0xffffcc5c),
              child: Theme(
                  data: ThemeData(
                    primaryColor: Colors.red,
                    canvasColor: Colors.red,
                    accentColor: Colors.red,
                    backgroundColor: Colors.red,
                    textTheme: TextTheme(
                      headline1: TextStyle(color: Colors.red),
                      headline2: TextStyle(color: Colors.red),
                      headline3: TextStyle(color: Colors.red),
                      headline4: TextStyle(color: Colors.red),
                      headline5: TextStyle(color: Colors.red),
                      headline6: TextStyle(color: Colors.red),
                      bodyText1: TextStyle(color: Colors.red),
                      bodyText2: TextStyle(color: Colors.red),
                      button: TextStyle(color: Colors.red),
                    ),
                  ),
                  child: monthCalendarro),
            ),
            Expanded(
              child: Container(
                  //color: Colors.white,
                  child: StreamBuilder(
                      stream: bloc.circularEventSubject.stream,
                      builder: (BuildContext context,
                          AsyncSnapshot<List<ResultCalendarItem>> snapshot) {
                        Widget child;
                        if (snapshot.hasData) {
                          if (snapshot.data.length > 0) {
                            child = Container(child: buildList(snapshot));
                          } else {
                            child = Center(
                                  child: Text(
                                "Para esa fecha, no se encuentran eventos en la Unal",
                                style: TextStyle(color: Colors.white),
                              )
                            );
                          }
                        } else if (snapshot.hasError) {
                          child = Container(
                            child: Column(
                              children: <Widget>[
                                Icon(
                                  Icons.error_outline,
                                  color: Colors.red,
                                  size: 60,
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 16),
                                  child: Text('Error: ${snapshot.error}'),
                                )
                              ],
                            ),
                          );
                        } else {
                          child = Container(
                              //child: Text("en progreso..."),
                              );
                        }

                        return child;
                      })),
            )
          ],
        )));
  }

  Widget buildList(AsyncSnapshot<List<ResultCalendarItem>> snapshot) {
    List<ResultCalendarItem> resultList = snapshot.data;

    return ListView.builder(
        padding: EdgeInsets.only(bottom: 20.0),
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemCount: resultList.length,
        itemBuilder: (BuildContext context, int index) {
          return Container(
              margin: EdgeInsets.only(top: 20.0, left: 20.0, right: 20.0),
              padding: EdgeInsets.all(15.0),
              decoration: BoxDecoration(color: Colors.white, boxShadow: [
                BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 3,
                    blurRadius: 7
                    //offset: Offset(0,3)
                    )
              ]),
              child: Row(children: <Widget>[
                Expanded(
                  child: Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(bottom: 10.0),
                          child: Text(
                            resultList[index].campus.toUpperCase(),
                            style: TextStyle(
                                color: Color(0xff848484),
                                fontSize: 13.0,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(bottom: 10.0),
                          child: Text(
                            resultList[index].title,
                            style:
                                TextStyle(color: Colors.black, fontSize: 24.0),
                          ),
                        ),
                        Text(
                          "${resultList[index].area}, ${resultList[index].type}",
                          style: TextStyle(color: Colors.black, fontSize: 13.0),
                        ),
                        Text(
                          "Fecha: ${convertTimeFormat(resultList[index].initialDate)}"
                              .toUpperCase(),
                          style: TextStyle(color: Colors.black, fontSize: 13.0),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                    child: InkWell(
                        child: Icon(
                          Icons.keyboard_arrow_right,
                          color: Color(0xffffcc5c),
                        ),
                        onTap: () {

                          blocDetail.getEventById(resultList[index].eventUid);

                          bool firstTime = true;
                          _blocDetailStream.listen((event) {
                            if (event != null) {
                              if (firstTime) {
                                firstTime = false;

                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) =>
                                        EventDetailPage(circularEvent: event)));
                              }
                            }
                          });
                        })),
              ]));
        });
  }

  convertTimeFormat(DateTime date) {
    return "${(date.day < 10) ? "0" : ""}${date.day}/"
        "${(date.month < 10) ? "0" : ""}${date.month}/"
        "${date.year} "
        "${(date.hour < 10) ? "0" : ""}${date.hour}:"
        "${(date.minute < 10) ? "0" : ""}${date.minute}";
  }

  _getCalendarItems(DateTime date) {
    String dateDay;

    dateDay = "${date.year}-${date.month}-${date.day}";

    bloc.getCalendarItemByDay(dateDay);
  }

  _initializeCalendar() {
    initializeDateFormatting('es_CO', null);
    var date = new DateTime.now();
    var formatter = DateFormat('MMMM y');
    currentMonthText = formatter.format(date);
    _getCalendarItems(date);

    var startDate = DateUtils.getFirstDayOfCurrentMonth();
    var endDate = new DateTime(2050);
    setState(() {
      monthCalendarro = Calendarro(
          key: ValueKey<Object>(redrawObject),
          startDate: startDate,
          endDate: endDate,
          selectedSingleDate: DateTime.now(),
          displayMode: DisplayMode.MONTHS,
          selectionMode: SelectionMode.SINGLE,
          weekdayLabelsRow: CustomWeekdayLabelsRow(),
          onPageSelected: (value1, value2) {
            setState(() {
              var formatter = DateFormat('MMMM y');
              currentMonthText = formatter.format(value1);
            });
          },
          onTap: (date) {
            _getCalendarItems(date);
          });
    });
  }
}

class CustomWeekdayLabelsRow extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Expanded(
            child: Container(
                margin: EdgeInsets.only(bottom: 20.0),
                child: Text(
                  "L",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.black, fontWeight: FontWeight.bold),
                ))),
        Expanded(
            child: Container(
                margin: EdgeInsets.only(bottom: 20.0),
                child: Text(
                  "M",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.black, fontWeight: FontWeight.bold),
                ))),
        Expanded(
            child: Container(
                margin: EdgeInsets.only(bottom: 20.0),
                child: Text(
                  "M",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.black, fontWeight: FontWeight.bold),
                ))),
        Expanded(
            child: Container(
                margin: EdgeInsets.only(bottom: 20.0),
                child: Text(
                  "J",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.black, fontWeight: FontWeight.bold),
                ))),
        Expanded(
            child: Container(
                margin: EdgeInsets.only(bottom: 20.0),
                child: Text(
                  "V",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.black, fontWeight: FontWeight.bold),
                ))),
        Expanded(
            child: Container(
                margin: EdgeInsets.only(bottom: 20.0),
                child: Text(
                  "S",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.black, fontWeight: FontWeight.bold),
                ))),
        Expanded(
            child: Container(
                margin: EdgeInsets.only(bottom: 20.0),
                child: Text(
                  "D",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.black, fontWeight: FontWeight.bold),
                ))),
      ],
    );
  }
}
