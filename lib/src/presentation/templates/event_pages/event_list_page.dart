import 'dart:math';

import 'package:circular/src/business_logic/blocs/events_bloc.dart';
import 'package:circular/src/data/models/circular_event_model.dart';
import 'package:circular/src/data/models/radio_model.dart';
import 'package:circular/src/presentation/partials/event_partials/item_event_partial.dart';
import 'package:circular/src/presentation/templates/event_pages/event_detail_page.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class EventListPage extends StatefulWidget {
  List<RadioModel> params;

  //Constructor
  EventListPage(params) {
    this.params = params;
    if (this.params != null) {
      this.createState();
    }
  }

  @override
  State<StatefulWidget> createState() {
    return _EventListPageState(this.params);
  }
}

class _EventListPageState extends State<EventListPage> {
  final bloc = EventsBloc();
  List<RadioModel> params;
  var _blocStream;

  GlobalKey _bodyKey = GlobalKey();


  _EventListPageState(params) {
    this.params = params;
    if (this.params != null) {
      this.initState();
    }
  }

  @override
  void initState() {
    if (params != null) {
      //Envia un listado de filtros o categorias (RadioModels)
      bloc.fetchEventsByFilters(params);
    } else {
      bloc.fetchAllEvents();
    }

    _blocStream = bloc.circularEventSubject.stream;
    super.initState();
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        key:_bodyKey,

        child: StreamBuilder(
            stream: _blocStream,
            builder: (BuildContext context,
                AsyncSnapshot<List<CircularEventModel>> snapshot) {
              Widget child;
              if (snapshot.hasData) {
                child = Align(
                    alignment: Alignment.topLeft,
                    child: Container(
                        height: MediaQuery.of(context).size.height,
                        child: buildList(snapshot)));




              } else if (snapshot.hasError) {
                child = Container(
                  child: Column(
                    children: <Widget>[
                      Icon(
                        Icons.error_outline,
                        color: Colors.red,
                        size: 60,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 16),
                        child: Text('Error: ${snapshot.error}'),
                      )
                    ],
                  ),
                );
              } else {
                child = Container(
                    //child: Text("en progreso..."),
                    );
              }

              return child;
            }));
  }

  //Genera la estructura de la lista en fomra de linea de tiempo
  Widget buildList(AsyncSnapshot<List<CircularEventModel>> snapshot) {
    var eventList = snapshot.data;
    List<Widget> listWidget = [];

    // >> Variables  que indican la posicion de cada tarjeta en la linea de tiempo
    int _countAdjust = 0;

    double _heightCard = (_getBodySizes()/2)+3;
    double _widthCard = MediaQuery.of(context).size.width * 0.5;
    double _marginLeftUpCount = (_widthCard) + (_widthCard / 3);
    double _marginLeftDownCount = (_widthCard);

    //Agrega el ancho del listado
    listWidget.add(Container(
      width: ((eventList.length + 4) * _widthCard) / 2,
    ));

    //Se agrega la linea de tiempo central
    listWidget.add(Center(
        child: Container(
      color: Colors.grey,
      width: ((eventList.length + 4) * _widthCard) / 2,
      height: 1,
      child: Text(""),
    )));

    //Agrega imagen principal del home
    listWidget.add(
      Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width / 2,
        child: Image.asset(
          'assets/images/portadacircular.jpg',
          fit: BoxFit.cover,
        ),
      ),
    );

    //Agraga mensaje d el a imagen principal del home
    listWidget.add(Positioned(
      child: Container(
        margin: EdgeInsets.only(left: 5.0),
        padding: EdgeInsets.all(5.0),
        color: Color(0xff333333),
        width: MediaQuery.of(context).size.width * 0.47,
        child: Text(
          "Agenda Nacional de eventos y actividades académicas. \n\n Universidad Nacional de Colombia",
          textAlign: TextAlign.center,
        ),
      ),
    ));

    //Agrega texto de Pròximos eventos
    listWidget.add(Container(
        width: MediaQuery.of(context).size.width / 2,
        child: Center(
          child: Row(
            children: <Widget>[
              Expanded(
                  child: Container(
                      color: Color(0xffffcc5c),
                      child: Text(
                        "Próximos Eventos",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 20.0,
                          color: Color(0xff333333),
                        ),
                      ))),
              Container(
                child: Image(
                  image: AssetImage(
                    'assets/images/triangulo.png',
                  ),
                ),
              ),
            ],
          ),
        )));

    //Agrega el listado de eventos
    eventList.asMap().forEach((i, event) {
      if (_countAdjust % 2 == 0 && _countAdjust > 0) {
        _marginLeftUpCount += _widthCard;
        _marginLeftDownCount += _widthCard;
      }

      //Si es par, acomoda el evento arriba
      if (i % 2 == 0) {
        listWidget.add(Positioned(
            bottom: 0.0,
            child: Align(
                alignment: Alignment.topLeft,
                child:

                Container(
                  margin: EdgeInsets.only(left: _marginLeftUpCount),
                  height: _heightCard,
                  width: _widthCard,
                  child: buildItemEventList(event, 'DOWN'),
                )

            )));
      } else {

        //Si el indice es impar, acomoda el evento abajo
        listWidget.add(Positioned(
            child: Align(
                alignment: Alignment.topLeft,
                child: Container(
                  margin: EdgeInsets.only(left: _marginLeftDownCount),
                  height: _heightCard,
                  width: _widthCard,
                  child: buildItemEventList(event, 'UP'),
                ))));
      }

      _countAdjust++;
    });

    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child:

          Align(
            alignment: Alignment.topLeft,
            child:
      Container(
        height: MediaQuery.of(context).size.height,
        child:
      Stack(children: listWidget)
        )
      )
    );
  }

  //Retorna un Card de evento para ser pintado en la lista
  //position: "UP", "DOWN"
  Widget buildItemEventList(CircularEventModel event, String position) {
    return Stack(
      children: <Widget>[
        Container(
          padding: (position == "UP")
              ? EdgeInsets.only(bottom: 19.0)
              : EdgeInsets.only(top: 19.0),
          child: InkWell(
            child: ItemEventPartial(
                event, (position == "UP") ? "BOTTOM" : "TOP", true),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>
                          EventDetailPage(circularEvent: event)));
            },
          ),
        ),

        //Si el Item (card) esta en la parte superior, el icono coloca en la parte inferior
        if (position == "UP")
          Positioned(
            bottom: 0.0,
            child: Container(
                margin: EdgeInsets.only(left: 10.0),
                child: Column(children: <Widget>[
                  Container(
                    child: Image(
                      height: 40,
                      image: AssetImage(
                          'assets/images/${getIconName(event.areaUid, "up")}'),
                    ),
                  )
                ])),
          ),

        //Si el Item (card) esta en la parte inferio, el icono se ubica en la parte superior
        if (position == "DOWN")
          Positioned(
            top: 0.0,
            child: Container(
                margin: EdgeInsets.only(left: 10.0),
                child: Column(children: <Widget>[
                  Container(
                    child: Image(
                      height: 40,
                      image: AssetImage(
                          'assets/images/${getIconName(event.areaUid, "down")}'),
                    ),
                  ),
                ])),
          ),
      ],
    );
  }

  //id: int - ID de la categoria
  //position: String - "up" o "down"
  String getIconName(id, position) {
    switch (id) {
      //area y cultura
      case 4:
        {
          return "home-areas-04-${position}.png";
        }
        break;

      //Ciencia y tecnologia
      case 5:
        {
          return "home-areas-07-${position}.png";
        }
        break;

      //Desarrollo rural
      case 6:
        {
          return "home-areas-07-${position}.png";
        }
        break;

      //Economia y organizaciones
      case 7:
        {
          return "home-areas-03-${position}.png";
        }
        break;

      //Educación
      case 8:
        {
          return "home-areas-05-${position}.png";
        }
        break;

      //Educación
      case 10:
        {
          return "home-areas-11-${position}.png";
        }
        break;

      //Politica y sociedad
      case 11:
        {
          return "home-areas-09-${position}.png";
        }
        break;

      //Salud
      case 12:
        {
          return "home-areas-10-${position}.png";
        }
        break;

      //Recreación y deporte
      case 13:
        {
          return "home-areas-06-${position}.png";
        }
        break;

      //Medio ambiente
      case 14:
        {
          return "home-areas-08-${position}.png";
        }
        break;

      //Ciudad y territorio
      case 31:
        {
          return "home-areas-02-${position}.png";
        }
        break;

      default:
        {
          //statements;
        }
        break;
    }
  }

  double _getBodySizes() {
    final RenderBox renderBoxRed = _bodyKey.currentContext.findRenderObject();
    final sizeRed = renderBoxRed.size;
    return sizeRed.height;
  }
}
