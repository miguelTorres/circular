import 'dart:convert';

import 'package:circular/src/business_logic/blocs/event_dates_bloc.dart';
import 'package:circular/src/business_logic/blocs/event_detail_bloc.dart';
import 'package:circular/src/business_logic/blocs/event_filter_bloc.dart';
import 'package:circular/src/business_logic/blocs/user_add_favorite_bloc.dart';
import 'package:circular/src/data/models/circular_event_model.dart';
import 'package:circular/src/presentation/partials/event_partials/item_event_partial.dart';
import 'package:circular/src/presentation/templates/event_pages/event_map_page.dart';
import 'package:circular/src/presentation/templates/user_pages/user_login_page.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/style.dart';
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'dart:io' show Platform;
import 'package:share/share.dart';


class EventDetailPage extends StatefulWidget {
  CircularEventModel circularEvent;

  EventDetailPage({this.circularEvent});

  @override
  _EventDetailPageState createState() =>
      _EventDetailPageState(this.circularEvent);
}

class _EventDetailPageState extends State<EventDetailPage> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  bool hasInternet=true;

  CircularEventModel circularEvent;
  final bloc = EventDetailBloc();
  final blocAddFavorite = UserAddFavoriteBloc();
  final eventFilterBloc = EventsFilterBloc();
  final blocDates = EventDatesBloc();
  var eventName = "";
  var eventUrl = "";
  bool _showDates = false;
  var connectivityResult;

  int _userUid;
  String _username;
  String _pass;
  String _type;
  bool _isSession;

  var _credentials;


  _EventDetailPageState(circularEvent) {
    this.circularEvent = circularEvent;
    this.eventUrl = circularEvent.eventUrl;
    this.eventName = circularEvent.title;
  }

  GoogleMapController mapController;

  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
  }

  @override
  void initState() {
    super.initState();

    askForInternet().then((value) {
      setState(() {
        this.hasInternet = value;
      });

    });



    _isSession = false;
    initializePreferences();

    _verifyInternet();

    //Trae los eventos relacionados
    eventFilterBloc
        .getEventByFilter(null, null, [circularEvent.typeUid], 10, false, null);
    //Trae las fechas adicionales del evento
    blocDates.getDatesByEventUid(circularEvent.uid);


  }

  Future<void> initializePreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //Verifica si el uid del usuario está almacenado en las preferencias
    //es decir, si ya tenia una sesión abierta
    if (prefs.getInt("uid") != null) {
      setState(() {
        _isSession = true;

        _userUid = prefs.getInt("uid");
        _username = prefs.getString("username");
        _pass = prefs.getString("pass");
        _type = prefs.getString("type");
      });
    }
  }

  @override
  void dispose() {
    bloc.dispose();
    eventFilterBloc.dispose();
    blocDates.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          leading: Builder(
            builder: (BuildContext context) {
              return IconButton(
                icon: const Icon(Icons.arrow_back_ios),
                onPressed: () {
                  Navigator.pop(context);
                },
              );
            },
          ),
          centerTitle: true,
          title: Container(
            height: 23,
            child: Image.asset('assets/images/logoCircular.png',
                fit: BoxFit.cover),
          ),
        ),
        body: Stack(children: <Widget>[
          Container(child: buildDetail()),
          Positioned(
            bottom: 0.0,
            right: 5.0,
            child: Container(
              child: Column(children: [
               /* Container(
                  margin: EdgeInsets.only(bottom: 10.0),
                  child: FloatingActionButton(
                    heroTag: "btnShareGoogleCalendar",
                    child: Icon(Icons.event),
                    onPressed: () {
                      insertEvent(googleEvent);
                    },
                  ),
                ),*/
                Container(
                  margin: EdgeInsets.only(bottom: 10.0),
                  child: FloatingActionButton(
                    heroTag: "btnFavoriteEvent",
                    child: Icon(Icons.favorite),
                    onPressed: () {
                      if (_isSession) {
                        blocAddFavorite.addFavoriteToUser(_userUid, _username,
                            _pass, _type, circularEvent.uid);
                        blocAddFavorite.subject.stream.listen((data) {
                          if (data != null) {
                            if (data["status"] != null &&
                                data["status"] == true) {
                              _displaySnackBar(
                                  'Evento agregado a mis favoritos');
                            } else {
                              _displaySnackBar(
                                  'No se ha podido agregar a favoritos');
                            }
                          } else {
                            _displaySnackBar(
                                'No se ha podido agregar a favoritos');
                          }
                        });
                      } else {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => UserLoginPage()));
                      }
                    },
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 10.0),
                  child: FloatingActionButton(
                    heroTag: "btnShareEvent",
                    child: Icon(Icons.share),
                    onPressed: () async {


                      await Share.share("$eventName $eventUrl", subject: 'Circular Unal: $eventName');

                    },
                  ),
                )
              ]),
            ),
          )
          //floatingActionButtonLocation: FloatingActionButtonLocation.startTop,
        ]));
  }

  Widget buildDetail() {
    LatLng _center = LatLng(circularEvent.circularLocation.latitude,
        circularEvent.circularLocation.longitude);

    List<Widget> list = [
      Container(
        padding:
            EdgeInsets.only(left: 20.0, right: 20.0, top: 20.0, bottom: 20.0),
        child: Align(
          alignment: Alignment.centerLeft,
          child: Text(
            circularEvent.title,
            style: TextStyle(
                fontSize: 23.0,
                color: Colors.white,
                fontWeight: FontWeight.bold),
          ),
        ),
      ),
      Container(
          padding: EdgeInsets.only(left: 20.0, top: 20.0),
          child: Align(
            alignment: Alignment.centerLeft,
            child: getDetailDateFormat(circularEvent.currentDate),
          )),
      if (connectivityResult != ConnectivityResult.none)
        Container(
            padding: EdgeInsets.only(left: 20.0, bottom: 20.0),
            child:
    Align(
    alignment: Alignment.centerLeft,
    child: _getMoreDates(),
    )),


      Container(
        color: Color(0xffffcc5c),
        child: Row(
          children: <Widget>[
            Expanded(
              child: Container(
                  padding: EdgeInsets.only(right: 10.0, left: 10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Text(
                        circularEvent.circularLocation.address,
                        textAlign: TextAlign.right,
                        style: TextStyle(
                            fontStyle: FontStyle.italic,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  )),
            ),
            Container(
              color: Color(0xff333333),

              child: SizedBox(
                  width: 120, // or use fixed size like 200
                  height: 100,
                  child:
                  IconButton(icon: Icon(Icons.location_on, size: 50.0,
                  ),
    onPressed: (){
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => EventMapPage(
                  circularEvent.circularLocation)));
    }

                  )

                  ),
            ),
          ],
        ),
      ),
      Container(
        padding: EdgeInsets.all(15.0),
        child: Html(
          data: circularEvent.description,
          onLinkTap: (url){
            launch(url);
          },
          style: {

            "a":Style(
              color: Color(0xffffcc5c)
            )

          },
        ),
      ),
      if (circularEvent.dependenciaName != null)
        getItemInfo("Publica", circularEvent.dependenciaName),
      if (circularEvent.invitados != null && circularEvent.invitados != "")
        getItemInfo("Invitados", circularEvent.invitados),
      if (circularEvent.ticketsType != null &&
          circularEvent.ticketsType == "ENTRADA_LIBRE")
        getItemInfo("Costo boletería", "ENTRADA LIBRE"),
      if (circularEvent.ticketsType != null &&
          circularEvent.ticketsType == "BOLETA")
        getItemInfo("Costo boletería", circularEvent.ticketsCost),
      if (circularEvent.ticketsUrl != null &&
          circularEvent.ticketsType == "URL")
        getItemInfo(
            "Url de boletería",
            new InkWell(
                onTap: () {
                  launch(circularEvent.ticketsUrl);
                },
                child: Text(
                  circularEvent.ticketsUrl,
                  style: new TextStyle(color: Color(0xffffcc5c)),
                ))),
      if (circularEvent.inscriptionDateInitial != null &&
          circularEvent.inscriptionDateFinal != null)
        getItemInfo(
            "Periodo de inscripción",
            getIncriptionDateFormat(circularEvent.inscriptionDateInitial,
                circularEvent.inscriptionDateFinal)),
      if (circularEvent.inscriptionWay != null &&
          circularEvent.inscriptionWay != "")
        getItemInfo(
            "Forma de inscripción", Html(data: circularEvent.inscriptionWay)),
      if (circularEvent.keywords != null && circularEvent.keywords != "")
        getItemInfo("Palabras clave", circularEvent.keywords),



      Container(
          margin: EdgeInsets.only(top: 20.0, bottom: 100.0),
          child:





                  StreamBuilder(
                      stream: eventFilterBloc.circularEventSubject.stream,
                      builder: (BuildContext context,
                          AsyncSnapshot<List<CircularEventModel>> snapshot) {
                        Widget child;
                        if (snapshot.hasData) {

                          print(snapshot.data.length);


                            child =

                              Column(
                                children: <Widget>[
                                  if(snapshot.data.length>1)
                                  Container (

                                    margin: EdgeInsets.only(left: 20.0, top:20.0, bottom: 20.0),

                                    child:
                                    Align(
                                        alignment: Alignment.centerLeft,
                                        child:
                                        Text(
                                          "Eventos relacionados",
                                          textAlign: TextAlign.left,
                                          style: TextStyle(color: Color(0xffffcc5c)
                                              , fontWeight: FontWeight.bold
                                          ),
                                        )
                                    ),
                                  ),

                                          Container(
                                          height: MediaQuery.of(context).size.height / 3,
                        width: MediaQuery.of(context).size.width,
                        child:

                              buildEventRelatedList(snapshot)
                                          )

                                ],
                              );

                        } else if (snapshot.hasError) {
                          child = Container();
                        } else {
                          child = Container(
                              //child: Text("en progreso..."),
                              );
                        }

                        return child;
                      }),

          )
    ];

    return Container(
        child: Stack(
      children: <Widget>[

        getImageWigget(context),



        Positioned(
            top: 0.0,
            child: Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                color: Color(0x00333333).withOpacity(0.9),
                child: SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: Container(
                        margin: EdgeInsets.only(bottom: 20.0),
                        child: Column(
                          children: list,
                        )))))
      ],
    ));
  }

  Widget getDetailDateFormat(DateTime dateTime) {
    initializeDateFormatting();
    final df = new DateFormat("EEEE, dd 'de'  MMMM 'de' yyyy | hh:mm", 'es');
    var date = df.format(dateTime);

    return Text(
      date,
      textAlign: TextAlign.left,
      style: TextStyle(color: Color(0xffffcc5c), fontStyle: FontStyle.italic),
    );
  }

  String getIncriptionDateFormat(DateTime start, DateTime end) {
    initializeDateFormatting();


    final df = new DateFormat(" MMM dd", 'es');
    var dateInitial = df.format(start);
    var dateFinal = df.format(end);
    return "$dateInitial - $dateFinal";
  }

  Widget getItemInfo(String label, dynamic info) {
    return Container(
      padding: EdgeInsets.only(left: 20.0, top: 10.0),
      child: Column(
        children: <Widget>[
          Align(
            alignment: Alignment.centerLeft,
            child: Text(
              label,
              style: TextStyle(color: Color(0xffffcc5c)),
            ),
          ),
          if (info is String)
            Align(
              alignment: Alignment.centerLeft,
              child: Text(info),
            ),
          if (info is Widget)
            Align(
              alignment: Alignment.centerLeft,
              child: info,
            ),
        ],
      ),
    );
  }

  Widget buildEventRelatedList(
      AsyncSnapshot<List<CircularEventModel>> snapshot) {
    var eventList = snapshot.data;

    return ListView.builder(
      scrollDirection: Axis.horizontal,
      itemCount: eventList.length,
      itemBuilder: (BuildContext ctxt, int index) {
        if (circularEvent.uid != eventList[index].uid) {

          return Container(
              width: MediaQuery.of(context).size.width * 0.6,
              child: InkWell(
                  child: ItemEventPartial(eventList[index], "BOTTOM", false),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => EventDetailPage(
                                circularEvent: eventList[index])));
                  }));
        } else {
          return Container();
        }
      },
    );
  }

  _displaySnackBar(String msg) {
    final snackbar = SnackBar(
        backgroundColor: Color(0xffffcc5c),
        content: Text(
          msg,
          style: TextStyle(color: Colors.black),
        ));
    _scaffoldKey.currentState.showSnackBar(snackbar);
  }

  _getMoreDates() {
    return StreamBuilder(
        stream: blocDates.circularEventSubject.stream,
        builder: (BuildContext context, AsyncSnapshot<List<dynamic>> snapshot) {
          Widget child;

          if (snapshot.hasData) {
            var data = snapshot.data;
            if (data.length > 0) {

              List<Widget> list = [];

              if(!_showDates)
                list.add(
                  Align(
                      alignment: Alignment.center,

                      child:
                IconButton(
                   icon:Icon(Icons.keyboard_arrow_down),
                  onPressed: () {
                    setState(() {
                      _showDates = true;
                    });
                  },
                )),
              );
              if(_showDates)
              list.add(Text("Todas las fechas: "));
              if(_showDates)
              data.forEach((element) {
                list.add(
                  getDetailDateFormat(DateTime.parse(element["initial"])),
                );
              });
              if(_showDates)
              list.add(

                  Align(
                      alignment: Alignment.center,
                  child:
                  IconButton(
                  icon: const Icon(Icons.keyboard_arrow_up),
                  onPressed: () {
                    setState(() {
                    _showDates = false;
                    });
                  })
              )
              );

              child = Container(
                    margin: EdgeInsets.only(top: 20.0),
                    child:
                  Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: list,
              ));
            }
          } else {
            child = Container();
          }

          return child;
        });
  }


  Future<void> _verifyInternet() async {
    connectivityResult = await Connectivity().checkConnectivity();
  }

  void prompt(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }



  Widget getImageWigget(context)  {

    if (!this.hasInternet) {
      // Si No hay coneccion a internet, carga los eventos de la db local


      /* Image.memory(
          base64Decode(circularEvent.imgBase64),
          fit: BoxFit.cover,
          height: double.infinity,
          width: double.infinity,
          alignment: Alignment.center,
        ),*/

      return  Image.memory(
        base64Decode(widget.circularEvent.imgBase64),
        fit: BoxFit.cover,
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        alignment: Alignment.center,
      );

    } else  {
      // Si hay coneccion por wifi o Si hay coneccion por datos

      return Image.network(
        widget.circularEvent.imgUrl,
        fit: BoxFit.fill,
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        alignment: Alignment.center,
      );

    }





  }

  Future<bool>  askForInternet() async {
    var connectivityResult = await Connectivity().checkConnectivity();

    if (connectivityResult == ConnectivityResult.none) {
      return false;
    }
    return true;
  }

}
