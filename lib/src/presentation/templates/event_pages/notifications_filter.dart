import 'package:circular/src/presentation/partials/commons/notifications_item.dart';
import 'package:circular/src/presentation/partials/commons/radio_item.dart';
import 'package:circular/src/presentation/templates/home.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:circular/src/data/models/radio_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NotificatiosFilter extends StatefulWidget {
  @override
  _NotificatiosFilterState createState() => _NotificatiosFilterState();
}

class _NotificatiosFilterState extends State<NotificatiosFilter> {
  List<dynamic> sampleData = new List<dynamic>();
  bool hasFilters = false;
  List<RadioModel> filterList = new List<RadioModel>();
  List<String> strfilterList = new List<String>();
  SharedPreferences prefs;
  final FirebaseMessaging _fcm = FirebaseMessaging();

  @override
  initState() {
    super.initState();

    sampleData = getNotificationsItem();

    loadPreference();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
            automaticallyImplyLeading: false,
            title: Text(
              "Subcribirse a",
              style: TextStyle(color: Colors.white),
            ),
            backgroundColor: Colors.grey[600],
            actions: <Widget>[
              IconButton(
                color: Colors.white,
                icon: Icon(Icons.clear),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ]),
        body: Stack(children: <Widget>[
          Container(
              margin:
                  new EdgeInsets.only(bottom: (this.hasFilters) ? 50.0 : 0.0),
              child: new ListView.builder(
                itemCount: sampleData.length,
                itemBuilder: (BuildContext context, int index) {
                  return new InkWell(
                    splashColor: Color(0xffffcc5c),
                    onTap: () {
                      setState(() {
                        if (!(sampleData[index] is Divider)) {
                          if (sampleData[index].type == "ALL" ||
                              sampleData[index].type == "NOTHING") {
                            clearFilters();
                          } else {
                            var tempAll = sampleData
                                .firstWhere((element) => element.type == "ALL");
                            tempAll.isSelected = false;
                            var tempNothing = sampleData.firstWhere(
                                (element) => element.type == "NOTHING");
                            tempNothing.isSelected = false;
                          }

                          if (sampleData[index].uid == 3000) {
                            //Todas las sedes
                            sampleData.forEach((element) {
                              if (!(element is Divider) &&
                                  element.type == "CAMPUS") {
                                element.isSelected = true;
                              }
                            });
                          } else if (sampleData[index].type == "CAMPUS") {
                            var temp = sampleData.firstWhere((element) =>
                                (!(element is Divider) && element.uid == 3000));
                            temp.isSelected = false;
                          }

                          if (sampleData[index].uid == 4000) {
                            //Todas las áreas
                            sampleData.forEach((element) {
                              if (!(element is Divider) &&
                                  element.type == "AREA") {
                                element.isSelected = true;
                              }
                            });
                          } else if (sampleData[index].type == "AREA") {
                            var temp = sampleData.firstWhere((element) =>
                                (!(element is Divider) && element.uid == 4000));
                            temp.isSelected = false;
                          }

                          if (sampleData[index].uid == 5000) {
                            //Todas los tipos
                            sampleData.forEach((element) {
                              if (!(element is Divider) &&
                                  element.type == "TYPE") {
                                element.isSelected = true;
                              }
                            });
                          } else if (sampleData[index].type == "TYPE") {
                            var temp = sampleData.firstWhere((element) =>
                                (!(element is Divider) && element.uid == 5000));
                            temp.isSelected = false;
                          }

                          sampleData[index].isSelected = true;
                          hasFilters = true;
                        }
                      });
                    },
                    child: getListItem(sampleData[index]),
                  );
                },
              )),
          if (hasFilters)
            Positioned(
                bottom: -6.0,
                child: Container(
                    height: 50.0,
                    width: MediaQuery.of(context).size.width,
                    child: RaisedButton(
                      onPressed: () async {
                        filterList = [];
                        strfilterList = [];
                        this.sampleData.forEach((element) {
                          if (!(element is Divider)) {
                            if (element.isSelected) {
                              this.filterList.add(element);
                              this.strfilterList.add("${element.type}-${element.uid.toString()}");
                            }
                          }
                        });

                        //Asigna la suscripción a FireBase

                        sampleData.forEach((element) {
                          if (!(element is Divider) &&
                              (element.type == "CAMPUS" ||
                                  element.type == "AREA" ||
                                  element.type == "TYPE")) {
                            _fcm.unsubscribeFromTopic(
                                "${element.type}-${element.uid.toString()}");
                            _fcm.unsubscribeFromTopic('SPECIAL');
                          }
                        });

                        for (var value in filterList) {
                          if (value.type == "SPECIAL") {
                            _fcm.subscribeToTopic("SPECIAL");
                          } else if (value.type == "ALL") {
                            sampleData.forEach((element) {
                              if (!(element is Divider) &&
                                  (element.type == "CAMPUS" ||
                                      element.type == "AREA" ||
                                      element.type == "TYPE")) {
                                _fcm.subscribeToTopic(
                                    "${element.type}-${element.uid.toString()}");
                              }
                            });
                            _fcm.subscribeToTopic('SPECIAL');
                          } else if (value.type == "NOTHING") {
                            sampleData.forEach((element) {
                              if (!(element is Divider) &&
                                  (element.type == "CAMPUS" ||
                                      element.type == "AREA" ||
                                      element.type == "TYPE")) {
                                _fcm.unsubscribeFromTopic(
                                    "${element.type}-${element.uid.toString()}");
                              }
                            });

                            _fcm.unsubscribeFromTopic('SPECIAL');
                          } else if (value.uid != 3000 &&
                              value.uid != 4000 &&
                              value.uid != 5000 &&
                              value.type != "SPECIAL") {
                            _fcm.subscribeToTopic(
                                "${value.type}-${value.uid.toString()}");
                          }
                        }

                        //Guarda los items seleccionados en las preferencias
                        prefs.setStringList(
                            'notificationsFilter', strfilterList);
                        Navigator.pop(context);
                      },
                      child: const Text('Aplicar cambios',
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold)),
                      color: Color(0xffffcc5c),
                      textColor: Colors.black,
                      elevation: 5,
                    )))
        ]),
        floatingActionButton: (hasFilters)
            ? Container(
                margin: new EdgeInsets.only(bottom: 50.0),
                child: FloatingActionButton(
                  heroTag: 'btnCleanFilters',
                  backgroundColor: Color(0xffffcc5c),
                  child: Icon(Icons.delete),
                  onPressed: () {
                    clearFilters();
                  },
                ),
              )
            : Container());
  }

  Widget getListItem(dynamic item) {
    Widget temp;

    if (item is RadioModel) {
      temp = new RadioItem(item);
    } else if (item is Divider) {
      temp = Container(
        margin: EdgeInsets.only(top: 20.0, bottom: 20.0),
        color: Colors.grey,
        width: MediaQuery.of(context).size.width / 2,
        height: 1,
        child: Text(""),
      );
    }

    return temp;
  }

  void clearFilters() {
    setState(() {
      sampleData.forEach((element) {
        if (!(element is Divider)) {
          element.isSelected = false;
        }
      });

      this.hasFilters = false;
    });
  }

  Future<void> loadPreference() async {
    prefs = await SharedPreferences.getInstance();

    strfilterList = prefs.getStringList('notificationsFilter');
    if (strfilterList != null) {
      strfilterList.forEach((element) {

        var cat = "";
        var id = "";
        var arr = element.split('-');
        cat = arr[0];
        id = arr[1];



        setState(() {
          var item = sampleData.firstWhere((value) =>
              (!(value is Divider) && (value.uid == int.parse(id)) && (value.type == cat)));

          item.isSelected = true;
        });
      });
    }
  }
}
