import 'package:circular/src/presentation/partials/commons/radio_item.dart';
import 'package:circular/src/presentation/templates/home.dart';
import 'package:flutter/material.dart';
import 'package:circular/src/data/models/radio_model.dart';

class EventFilter extends StatefulWidget {
  @override
  _EventFilterState createState() => _EventFilterState();
}

class _EventFilterState extends State<EventFilter> {
  List<dynamic> sampleData = new List<dynamic>();
  bool hasFilters = false;
  List<RadioModel> filterList = new List<RadioModel>();

  @override
  void initState() {
    super.initState();
    sampleData.add(new RadioModel(400, false, 'B', 'Ver todos', 'ALL'));
    sampleData.add(new Divider(
      color: Colors.red,
      thickness: 20,
    ));
    sampleData.add(new RadioModel(7, false, 'B', 'Amazonia', 'CAMPUS'));
    sampleData.add(new RadioModel(2, false, 'B', 'Bogotá', 'CAMPUS'));
    sampleData.add(new RadioModel(8, false, 'B', 'Caribe', 'CAMPUS'));
    sampleData.add(new RadioModel(1248, false, 'B', 'De La Paz', 'CAMPUS'));
    sampleData.add(new RadioModel(4, false, 'B', 'Manizales', 'CAMPUS'));
    sampleData.add(new RadioModel(3, false, 'B', 'Medellín', 'CAMPUS'));
    sampleData.add(new RadioModel(1, false, 'B', 'Nacional', 'CAMPUS'));
    sampleData.add(new RadioModel(6, false, 'B', 'Orinoquia', 'CAMPUS'));
    sampleData.add(new RadioModel(5, false, 'B', 'Palmira', 'CAMPUS'));
    sampleData.add(new RadioModel(9, false, 'B', 'Tumaco', 'CAMPUS'));
    sampleData.add(new Divider(
      color: Colors.red,
      thickness: 20,
    ));
    sampleData.add(new RadioModel(4, false, 'B', 'Arte y Cultura', 'AREA'));
    sampleData
        .add(new RadioModel(5, false, 'B', 'Ciencia y Tecnología', 'AREA'));
    sampleData
        .add(new RadioModel(31, false, 'B', 'Ciudad y Territorio', 'AREA'));
    sampleData.add(new RadioModel(6, false, 'B', 'Desarrollo Rural', 'AREA'));
    sampleData.add(
        new RadioModel(7, false, 'B', 'Economia y Organizaciones', 'AREA'));
    sampleData.add(new RadioModel(8, false, 'B', 'Educación', 'AREA'));
    sampleData.add(new RadioModel(10, false, 'B', 'Espiritualidad', 'AREA'));
    sampleData.add(new RadioModel(14, false, 'B', 'Medio Ambiente', 'AREA'));
    sampleData
        .add(new RadioModel(11, false, 'B', 'Política y Sociedad', 'AREA'));
    sampleData
        .add(new RadioModel(13, false, 'B', 'Recreación y Deporte', 'AREA'));
    sampleData.add(new RadioModel(12, false, 'B', 'Salud', 'AREA'));
    sampleData.add(new Divider(
      color: Colors.red,
      thickness: 20,
    ));
    sampleData
        .add(new RadioModel(3, false, 'B', 'Todos los especiales', 'SPECIAL'));
    sampleData.add(new Divider(
      color: Colors.red,
      thickness: 100,
    ));
    sampleData.add(new RadioModel(21, false, 'B', 'Cátedra', 'TYPE'));
    sampleData.add(new RadioModel(15, false, 'B', 'Charla', 'TYPE'));
    sampleData.add(new RadioModel(16, false, 'B', 'Cine Foro', 'TYPE'));
    sampleData.add(new RadioModel(17, false, 'B', 'Coloquio', 'TYPE'));
    sampleData.add(new RadioModel(20, false, 'B', 'Concierto', 'TYPE'));
    sampleData.add(new RadioModel(18, false, 'B', 'Conferencia', 'TYPE'));
    sampleData.add(new RadioModel(23, false, 'B', 'Congreso', 'TYPE'));
    sampleData.add(new RadioModel(19, false, 'B', 'Conversatorio', 'TYPE'));
    sampleData.add(new RadioModel(22, false, 'B', 'Curso', 'TYPE'));
    sampleData.add(new RadioModel(24, false, 'B', 'Diplomado', 'TYPE'));
    sampleData.add(new RadioModel(25, false, 'B', 'Exposiciones', 'TYPE'));
    sampleData.add(new RadioModel(26, false, 'B', 'Feria', 'TYPE'));
    sampleData.add(new RadioModel(27, false, 'B', 'Foro', 'TYPE'));
    sampleData.add(new RadioModel(29, false, 'B', 'Seminario', 'TYPE'));
    sampleData.add(new RadioModel(28, false, 'B', 'Taller', 'TYPE'));
    sampleData.add(new Divider(
      color: Colors.red,
      thickness: 20,
    ));
    sampleData.add(new RadioModel(100, false, 'B', 'Hoy', 'DATE'));
    sampleData.add(new RadioModel(200, false, 'B', 'En 7 días', 'DATE'));
    sampleData.add(new RadioModel(300, false, 'B', 'En 30 días', 'DATE'));
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
            automaticallyImplyLeading: false,
            title: Text(
              "Filtrar por",
              style: TextStyle(color: Colors.white),
            ),
            backgroundColor: Colors.grey[600],
            actions: <Widget>[
              IconButton(
                color: Colors.white,
                icon: Icon(Icons.clear),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ]),
        body: Stack(children: <Widget>[
          Container(
              margin:
                  new EdgeInsets.only(bottom: (this.hasFilters) ? 50.0 : 0.0),
              child: new ListView.builder(
                itemCount: sampleData.length,
                itemBuilder: (BuildContext context, int index) {
                  return new InkWell(
                    splashColor: Color(0xffffcc5c),
                    onTap: () {
                      setState(() {
                        if (!(sampleData[index] is Divider)) {

                          if (sampleData[index].type == "CAMPUS") {
                            sampleData.forEach((element) {
                              if (!(element is Divider))
                                element.isSelected = false;
                            });
                          }

                          if (sampleData[index].type == "ALL") {
                            sampleData.forEach((element) {
                              if (!(element is Divider))
                                element.isSelected = false;
                            });
                          }

                          sampleData[index].isSelected = true;

                          hasFilters = true;
                        }
                      });
                    },
                    child: getListItem(sampleData[index]),
                  );
                },
              )),
          if (hasFilters)
            Positioned(
                bottom: -6.0,
                child: Container(
                    height: 50.0,
                    width: MediaQuery.of(context).size.width,
                    child: RaisedButton(
                      onPressed: () {


                          this.sampleData.forEach((element) {
                            if (!(element is Divider)) {
                              if (element.isSelected) {
                                this.filterList.add(element);
                              }
                            }
                          }
                          );

                          if(this.filterList[0]!=null && this.filterList[0].type == "ALL"){
                            Navigator.pop(context);
                          }else {
                            Navigator.pop(context, this.filterList);
                          }

                      },
                      child: const Text('Aplicar filtros',
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold)),
                      color: Color(0xffffcc5c),
                      textColor: Colors.black,
                      elevation: 5,
                    )))
        ]),
        floatingActionButton: (hasFilters)
            ? Container(
                margin: new EdgeInsets.only(bottom: 50.0),
                child: FloatingActionButton(
                  heroTag: 'btnCleanFilters',
                  backgroundColor: Color(0xffffcc5c),
                  child: Icon(Icons.delete),
                  onPressed: () {
                    clearFilters();
                  },
                ),
              )
            : Container());
  }

  Widget getListItem(dynamic item) {
    Widget temp;

    if (item is RadioModel) {
      temp = new RadioItem(item);
    } else if (item is Divider) {
      temp = Container(
        margin: EdgeInsets.only(top: 20.0, bottom: 20.0),
        color: Colors.grey,
        width: MediaQuery.of(context).size.width / 2,
        height: 1,
        child: Text(""),
      );
    }

    return temp;
  }

  void clearFilters() {
    setState(() {
      sampleData.forEach((element) {
        if (!(element is Divider)) {
          element.isSelected = false;
        }
      });

      this.hasFilters = false;
    });
  }
}
