import 'package:calendarro/default_day_tile.dart';
import 'package:circular/src/business_logic/blocs/calendar_events_bloc.dart';
import 'package:circular/src/business_logic/blocs/elastic_search_bloc.dart';
import 'package:circular/src/business_logic/blocs/event_detail_bloc.dart';
import 'package:circular/src/business_logic/blocs/local_search_bloc.dart';
import 'package:circular/src/data/models/circular_event_model.dart';
import 'package:circular/src/data/models/result_calendar_item.dart';
import 'package:circular/src/presentation/partials/commons/loading_animation.dart';
import 'package:circular/src/presentation/templates/event_pages/preload_detail.dart';
import 'package:flutter/material.dart';
import 'package:calendarro/date_utils.dart';
import 'package:calendarro/calendarro.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';

import 'event_detail_page.dart';

class BrowserPage extends StatefulWidget {
  @override
  _BrowserPageState createState() => _BrowserPageState();
}

class _BrowserPageState extends State<BrowserPage> {
  var bloc = ElasticSearchBloc();
  var blocLocalSearch = LocalSearchBloc();


  String query;

  @override
  void initState() {
    initializeDateFormatting('es_CO', null);
    super.initState();
  }

  @override
  void dispose() {
    bloc.dispose();
    blocLocalSearch.dispose();

    this.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: AppBar(
          leading: Builder(
            builder: (BuildContext context) {
              return IconButton(
                icon: const Icon(Icons.arrow_back_ios),
                onPressed: () {
                  Navigator.pop(context);
                },
              );
            },
          ),
          centerTitle: true,
          title: Container(
            height: 23,
            child: Image.asset('assets/images/logoCircular.png',
                fit: BoxFit.cover),
          ),
        ),
        body: Stack(children: [
          Container(
              child: Column(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(left: 10.0, right: 10.0),
                child: TextFormField(
                    style: TextStyle(color: Colors.white),
                    textInputAction: TextInputAction.search,
                    decoration: InputDecoration(
                      labelText: "Buscar en Circluar UNAL",
                      //labelStyle: TextStyle(color: Colors.white),
                      enabledBorder: new UnderlineInputBorder(
                          borderSide: new BorderSide(color: Color(0xffffcc5c))),

                      prefixStyle: TextStyle(color: Colors.black),
                      suffix: GestureDetector(
                          child: IconButton(
                              icon:
                                  Icon(Icons.search, color: Color(0xffffcc5c))),
                          onTap: () {
                            blocLocalSearch.searchEventsByString(query);
                          }),
                    ),
                    onChanged: (value) {
                      if (value != "") {
                        query = value;
                      }
                    },
                    onFieldSubmitted: (term) {
                      blocLocalSearch.searchEventsByString(query);
                    }),
              ),
              Expanded(
                child: Container(
                    child: StreamBuilder(
                        stream: blocLocalSearch.circularEventSubject.stream,
                        builder: (BuildContext context,
                            AsyncSnapshot<List<CircularEventModel>> snapshot) {
                          Widget child;
                          if (snapshot.hasData) {
                            if (snapshot.data.length > 0) {

                              child = buildList(snapshot);
                            } else {
                              child = Center(
                                  child: Text(
                                "No se encuentran resultados",
                                style: TextStyle(color: Colors.white),
                              ));
                            }
                          } else if (snapshot.hasError) {
                            child = Container(
                              child: Column(
                                children: <Widget>[
                                  Icon(
                                    Icons.error_outline,
                                    color: Colors.red,
                                    size: 60,
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 16),
                                    child: Text('Error: ${snapshot.error}'),
                                  )
                                ],
                              ),
                            );
                          } else {
                            child = Container(
                                //child: Text("en progreso..."),
                                );
                          }

                          return child;
                        })),
              )
            ],
          )),

        ]));
  }

  Widget buildList(AsyncSnapshot<List<CircularEventModel>> snapshot) {
    List<CircularEventModel> resultList = snapshot.data;
    CircularEventModel item;

    return ListView.builder(
        padding: EdgeInsets.only(bottom: 20.0),
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemCount: resultList.length,
        itemBuilder: (BuildContext context, int index) {
          item = resultList[index];


          return
            InkWell(
           child:
            Container(
            margin: EdgeInsets.only(top: 20.0, left: 20.0, right: 20.0),
            padding: EdgeInsets.all(15.0),
            decoration: BoxDecoration(color: Colors.white, boxShadow: [
              BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 3,
                  blurRadius: 7
                  //offset: Offset(0,3)
                  )
            ]),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(bottom: 10.0),
                          child: Text(
                            "${item.campus.toUpperCase()}",
                            style: TextStyle(
                                color: Color(0xff848484),
                                fontSize: 13.0,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(bottom: 10.0),
                          child: Text(
                            item.title,
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 24.0,
                            ),
                          ),
                        ),
                        Text(
                          "Fecha del evento: ${convertTimeFormat(item.initialDate)}"
                              .toUpperCase(),
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 13.0,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),



                Container(
                  child: IconButton(
                    icon: Icon(
                      Icons.keyboard_arrow_right,
                      color: Color(0xffffcc5c),
                    ),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  EventDetailPage(circularEvent: resultList[index])
                          )
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
              onTap: () {

                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            EventDetailPage(circularEvent: resultList[index])
                    )
                );

              },

          );
        });
  }

  convertTimeFormat(DateTime date) {
    return "${(date.day < 10) ? "0" : ""}${date.day}/"
        "${(date.month < 10) ? "0" : ""}${date.month}/"
        "${date.year} "
        "${(date.hour < 10) ? "0" : ""}${date.hour}:"
        "${(date.minute < 10) ? "0" : ""}${date.minute}";
  }
}
