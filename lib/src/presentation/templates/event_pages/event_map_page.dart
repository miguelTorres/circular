import 'package:circular/src/data/models/circular_event_model.dart';
import 'package:circular/src/data/models/location_model.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../home.dart';


class EventMapPage extends StatefulWidget {
  CircularLocation circularLocation;

  EventMapPage(CircularLocation circularLocation){
    this.circularLocation = circularLocation;
  }

  @override
  _EventMapPageState createState() => _EventMapPageState(this.circularLocation);
}

class _EventMapPageState extends State<EventMapPage> {
  CircularLocation circularLocation;
  GoogleMapController mapController;
  LatLng _center =  LatLng(4.618324, -74.1619939);

  _EventMapPageState(CircularLocation circularLocation){
    this.circularLocation = circularLocation;
    this._center =  LatLng(circularLocation.latitude, circularLocation.longitude);
  }


  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;

  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(

      home: Scaffold(

        appBar: AppBar(

          leading: Builder(
            builder: (BuildContext context) {
              return IconButton(
                icon: const Icon(Icons.arrow_back_ios,
                color: Colors.black,
                ),
                onPressed: () {
                 Navigator.of(context, rootNavigator: true).pop(context);
                },
              );
            },
          ),
          centerTitle: true,
          title:
              Container(
                height: 23,
                child:  Image.asset(
                    'assets/images/logoCircular.png', fit: BoxFit.cover),
              ),
          backgroundColor: Color(0xffffcc5c),

        ),
        body: GoogleMap(
          onMapCreated: _onMapCreated,
          initialCameraPosition: CameraPosition(
            target: _center,
            zoom: 17.0,
          ),
          markers: Set.from([Marker(
            markerId: MarkerId("Ubicación del evento"),
            draggable: false,
            position: _center,

          ),]),
        ),
      ),
    );
  }
}
