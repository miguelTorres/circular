import 'package:circular/src/data/database/database.dart';
import 'package:circular/src/presentation/partials/commons/loading_animation.dart';
import 'package:circular/src/presentation/templates/home.dart';
import 'package:flutter/material.dart';
import 'package:circular/src/business_logic/sync_database.dart';
import 'dart:math';

class Preload extends StatefulWidget {
  @override
  _PreloadState createState() => _PreloadState();
}

class _PreloadState extends State<Preload> {
  SyncDataBase syncDB = new SyncDataBase();

  @override
  void initState() {
    super.initState();


    Future<String> state = syncDB.syncApiToDataBase();

    state.then((value) {

      Navigator.pop(context);

      //value puede ser igual a NO_CONECTION o COMPLETED
      //en cualquier caso se redirige al home en donde podra cargar eventos desde
      //la base de datos local.
      Navigator.pushNamed(context, '/home');

    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xff636363),
        appBar: AppBar(
          title: Center(
              child: Container(
            height: 23,
            child: Image.asset('assets/images/logoCircular.png',
                fit: BoxFit.cover),
          )),
        ),
        body: LoadingAnimation()


    );
  }
}
