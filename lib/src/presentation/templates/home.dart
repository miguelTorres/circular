import 'package:circular/src/business_logic/blocs/event_detail_bloc.dart';
import 'package:circular/src/data/models/radio_model.dart';
import 'package:circular/src/presentation/partials/commons/notifications_item.dart';
import 'package:circular/src/presentation/templates/event_pages/browser_page.dart';
import 'package:circular/src/presentation/templates/event_pages/calendar_page.dart';
import 'package:circular/src/presentation/templates/event_pages/event_filter.dart';
import 'package:circular/src/presentation/templates/event_pages/notifications_filter.dart';
import 'package:circular/src/presentation/templates/event_pages/event_list_page.dart';
import 'package:circular/src/presentation/templates/museum_pages/museum_list_page.dart';
import 'package:circular/src/presentation/templates/user_pages/user_configurations_page.dart';
import 'package:circular/src/presentation/templates/user_pages/user_create_event_page.dart';
import 'package:circular/src/presentation/templates/user_pages/user_events_page.dart';
import 'package:circular/src/presentation/templates/user_pages/user_favorites_page.dart';
import 'package:circular/src/presentation/templates/user_pages/user_login_page.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'event_pages/about.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {


  int _selectedIndex = 0;
  final _selectedItemColor = Colors.black;
  final _unselectedItemColor = Color(0xffffcc5c);
  final _selectedBgColor = Color(0xffffcc5c);
  final _unselectedBgColor = Colors.black;
  var redrawObject;


  String _name;
  String _email;
  bool _isSession;

  List<RadioModel> params;
  List<Widget> _widgetOptions = <Widget>[new EventListPage(null), new MuseumListPage()];

  final FirebaseMessaging _fcm = FirebaseMessaging();



  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  void initState() {
    super.initState();
    _isSession = false;
    initializePreferences();

  }



  Future<void> initializePreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //Verifica si el uid del usuario está almacenado en las preferencias
    //es decir, si ya tenia una sesión abierta
    if (prefs.getInt("uid") != null) {
      setState(() {
        _isSession = true;
        _name = prefs.getString("name");
        _email = prefs.getString("mail");
      });
    }




    List<String> strfilterList = [];
    var temp = prefs.getStringList('notificationsFilter');
    if (temp == null) {


      //Si es la primera vez que se abre la app, se suscribe  a todos los
      // topics de firebase para recibir notificaciones de todas las categorias.
      List<dynamic> notificationsItems = getNotificationsItem();
      notificationsItems.forEach((element) {

        if(!(element is Divider)) {
          _fcm.subscribeToTopic(
              "${element.type}-${element.uid.toString()}");
        }
      });


      //Si es la primera vez que se abre la app, guarda en preferencias el id=1000 que
      // indica el filtro de no notificacones subscrito "A TODOS"
      strfilterList.add('ALL-1000');
      prefs.setStringList('notificationsFilter', strfilterList);
    }

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: Drawer(
          child: ListView(
            // Importante: elimine cualquier padding del ListView.
            padding: EdgeInsets.zero,
            children: <Widget>[
              DrawerHeader(
                child: getDrawerHeaderChild(),
                decoration: BoxDecoration(
                  color: Color(0xffffcc5c),
                ),
              ),
              if (!_isSession)
                ListTile(
                    leading: Icon(Icons.exit_to_app, color: Color(0xffffcc5c)),
                    title: Text('Iniciar sesión'),
                    onTap: () {
                      Navigator.pop(context);
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => UserLoginPage()),
                      );
                    }),
              ListTile(
                  leading: Icon(Icons.search,
                      color:Color(0xffffcc5c)),
                  title: Text('Buscar'),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => BrowserPage()),
                    );
                  }),
              ListTile(
                leading: Icon(Icons.calendar_today,
                    color:Color(0xffffcc5c)),
                title: Text('Calendario'),
                onTap: () {
                  Navigator.pop(context);
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => CalendarPage()),
                  );
                },
              ),

              ListTile(
                leading: Icon(Icons.list,
                    color:Color(0xffffcc5c)),
                title: Text('Mis eventos'),
                onTap: () {
                  Navigator.pop(context);
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => UserEventsPage()),
                  );
                },
              ),
              ListTile(
                leading: Icon(Icons.create,
                    color:Color(0xffffcc5c)),
                title: Text('Crear Evento'),
                onTap: () {
                  Navigator.pop(context);
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => UserCreateEventPage()),
                  );
                },
              ),
              ListTile(
                leading: Icon(Icons.favorite,
                    color:Color(0xffffcc5c)),
                title: Text('Mis favoritos'),
                onTap: () {
                  Navigator.pop(context);
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => UserFavoritesPage()),
                  );
                },
              ),
              ListTile(
                  leading: Icon(Icons.settings,
                      color:Color(0xffffcc5c)),
                  title: Text('Configuraciones'),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => UserConfigurationsPage()),
                    );
                  }),
              if (_isSession)
                ListTile(
                    leading: Icon(Icons.exit_to_app,
                        color:Color(0xffffcc5c)),
                    title: Text('Cerrar sesión'),
                    onTap: () {
                      cleanPreference();

                      Navigator.pop(context);
                      Navigator.pushNamed(context, '/home');
                    }),
            ],
          ), // Poblaremos el Drawer en el siguiente paso!
        ),
        appBar: AppBar(
          //automaticallyImplyLeading: false,
          title: Center(
              child: Container(
            height: 23,
            child: Image.asset('assets/images/logoCircular.png',
                fit: BoxFit.cover),
          )),

          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.filter_list),
              onPressed: () {
                _redirectToFilter(context);

              },
            ),
            // action button
            IconButton(
              icon: Icon(Icons.notifications_none),
              onPressed: () {

                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => NotificatiosFilter()),
                );

              },
            ),
            IconButton(
              icon: Icon(Icons.info_outline),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => About()),
                );
              },
            )
          ],
        ),
        body: Center(
            key: ValueKey<Object>(redrawObject),

            child: _widgetOptions.elementAt(_selectedIndex)),
        bottomNavigationBar: Column(

          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            BottomNavigationBar(
              selectedFontSize: 0,
              items: <BottomNavigationBarItem>[
                BottomNavigationBarItem(
                  icon: _buildIcon('Eventos', 0),
                  title: SizedBox.shrink(),
                ),
                BottomNavigationBarItem(
                  icon: _buildIcon('Museos', 1),
                  title: SizedBox.shrink(),
                ),
              ],
              currentIndex: _selectedIndex,
              selectedItemColor: _selectedItemColor,
              unselectedItemColor: _unselectedItemColor,
            ),
            Container(
                color: Colors.black,
                width: MediaQuery.of(context).size.width,
                child: Center(

                    child:
                    Container(
                      margin: EdgeInsets.only(top: 5.0),
                      child:  Text(
                        "Universidad Nacional de Colombia",
                        style: TextStyle(
                            color: Color(0xffffcc5c), fontStyle: FontStyle.italic),
                      ),
                    )

                ))
          ],
        )

    );
  }



  _redirectToFilter(BuildContext context) async {
    //Abre el listado de los filtros y espera la respuesta con el
    //listado de filtros seleccionado
    List<RadioModel> result = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => EventFilter()),
    );

    //Actualiza el estado de los parametros
      params = result;

      this._widgetOptions = <Widget>[
        new EventListPage(
            params), //envia los filtros selccionados al screen del listado de eventos
        new MuseumListPage()
      ];

      _onItemTapped(0); //Selecciona el teb del listado de eventos

    setState(() {
      redrawObject = Object();
    });
  }

  Color _getBgColor(int index) =>
      _selectedIndex == index ? _selectedBgColor : _unselectedBgColor;

  Color _getItemColor(int index) =>
      _selectedIndex == index ? _selectedItemColor : _unselectedItemColor;

  Widget _buildIcon(String text, int index) => Container(
        width: double.infinity,
        height: kBottomNavigationBarHeight,
        child: Material(
          color: _getBgColor(index),
          child: InkWell(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                //Icon(iconData),
                Text(text, style: TextStyle(color: _getItemColor(index),
                fontWeight: FontWeight.bold
                )),
              ],
            ),
            onTap: () => _onItemTapped(index),
          ),
        ),
      );

  Widget getDrawerHeaderChild() {
    Widget child;

    if (_isSession) {
      child = Container(
        margin: EdgeInsets.only(left: 15.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(_name,
              style: TextStyle(
                color: Color(0xff333333),
                fontSize: 16.0,
                fontWeight: FontWeight.bold
              ),
            ),
            Text(_email,
              style: TextStyle(
                  color: Color(0xff333333),
                fontSize: 12.0,

              ),
            )
          ],
        ),
      );
    } else {
      child =
          Center(
          child:
              Image(
                height: 23,
                image: AssetImage("assets/images/logoCircular.png"),

              )

      );
    }

    return child;
  }

  cleanPreference() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.clear();
  }
}
