

import 'package:circular/src/data/models/radio_model.dart';
import 'package:flutter/material.dart';

List<dynamic> getNotificationsItem(){
  List<dynamic> sampleData = new List<dynamic>();
  sampleData.add(new RadioModel(1000, false, 'B', 'A todo', 'ALL'));
  sampleData.add(new RadioModel(2000, false, 'B', 'A nada', 'NOTHING'));
  sampleData.add(new Divider(
    color: Colors.white,
    thickness: 20,
  ));
  sampleData
      .add(new RadioModel(3000, false, 'B', 'Todas las sedes', 'CAMPUS'));
  sampleData.add(new RadioModel(7, false, 'B', 'Amazonia', 'CAMPUS'));
  sampleData.add(new RadioModel(2, false, 'B', 'Bogotá', 'CAMPUS'));
  sampleData.add(new RadioModel(8, false, 'B', 'Caribe', 'CAMPUS'));
  sampleData.add(new RadioModel(1248, false, 'B', 'De La Paz', 'CAMPUS'));
  sampleData.add(new RadioModel(4, false, 'B', 'Manizales', 'CAMPUS'));
  sampleData.add(new RadioModel(3, false, 'B', 'Medellín', 'CAMPUS'));
  sampleData.add(new RadioModel(1, false, 'B', 'Nacional', 'CAMPUS'));
  sampleData.add(new RadioModel(6, false, 'B', 'Orinoquia', 'CAMPUS'));
  sampleData.add(new RadioModel(5, false, 'B', 'Palmira', 'CAMPUS'));
  sampleData.add(new RadioModel(9, false, 'B', 'Tumaco', 'CAMPUS'));
  sampleData.add(new Divider(
    color: Colors.white,
    thickness: 20,
  ));
  sampleData.add(new RadioModel(4000, false, 'B', 'Todas las áreas', 'AREA'));
  sampleData.add(new RadioModel(4, false, 'B', 'Arte y Cultura', 'AREA'));
  sampleData
      .add(new RadioModel(5, false, 'B', 'Ciencia y Tecnología', 'AREA'));
  sampleData
      .add(new RadioModel(31, false, 'B', 'Ciudad y Territorio', 'AREA'));
  sampleData.add(new RadioModel(6, false, 'B', 'Desarrollo Rural', 'AREA'));
  sampleData.add(
      new RadioModel(7, false, 'B', 'Economia y Organizaciones', 'AREA'));
  sampleData.add(new RadioModel(8, false, 'B', 'Educación', 'AREA'));
  sampleData.add(new RadioModel(10, false, 'B', 'Espiritualidad', 'AREA'));
  sampleData.add(new RadioModel(14, false, 'B', 'Medio Ambiente', 'AREA'));
  sampleData
      .add(new RadioModel(11, false, 'B', 'Política y Sociedad', 'AREA'));
  sampleData
      .add(new RadioModel(13, false, 'B', 'Recreación y Deporte', 'AREA'));
  sampleData.add(new RadioModel(12, false, 'B', 'Salud', 'AREA'));
  sampleData.add(new Divider(
    color: Colors.red,
    thickness: 20,
  ));
  sampleData
      .add(new RadioModel(3, false, 'B', 'Todos los especiales', 'SPECIAL'));
  sampleData.add(new Divider(
    color: Colors.red,
    thickness: 100,
  ));
  sampleData.add(new RadioModel(5000, false, 'B', 'Todos los tipos', 'TYPE'));
  sampleData.add(new RadioModel(21, false, 'B', 'Cátedra', 'TYPE'));
  sampleData.add(new RadioModel(15, false, 'B', 'Charla', 'TYPE'));
  sampleData.add(new RadioModel(16, false, 'B', 'Cine Foro', 'TYPE'));
  sampleData.add(new RadioModel(17, false, 'B', 'Coloquio', 'TYPE'));
  sampleData.add(new RadioModel(20, false, 'B', 'Concierto', 'TYPE'));
  sampleData.add(new RadioModel(18, false, 'B', 'Conferencia', 'TYPE'));
  sampleData.add(new RadioModel(23, false, 'B', 'Congreso', 'TYPE'));
  sampleData.add(new RadioModel(19, false, 'B', 'Conversatorio', 'TYPE'));
  sampleData.add(new RadioModel(22, false, 'B', 'Curso', 'TYPE'));
  sampleData.add(new RadioModel(24, false, 'B', 'Diplomado', 'TYPE'));
  sampleData.add(new RadioModel(25, false, 'B', 'Exposiciones', 'TYPE'));
  sampleData.add(new RadioModel(26, false, 'B', 'Feria', 'TYPE'));
  sampleData.add(new RadioModel(27, false, 'B', 'Foro', 'TYPE'));
  sampleData.add(new RadioModel(29, false, 'B', 'Seminario', 'TYPE'));
  sampleData.add(new RadioModel(28, false, 'B', 'Taller', 'TYPE'));

  return sampleData;
}