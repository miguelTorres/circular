import 'package:circular/src/data/models/radio_model.dart';
import 'package:flutter/material.dart';


class RadioItem extends StatelessWidget {
  final RadioModel _item;
  RadioItem(this._item);

  @override
  Widget build(BuildContext context) {
    return new Container(
      margin:
      new EdgeInsets.only(left: 10.0, right: 2.0, top: 2.0, bottom: 2.0),
      child:

      new Row(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          new

          Container(
            height: 14.0,
            width: 15.0,
            child: Text(""),
            decoration: new BoxDecoration(
              color: _item.isSelected ? Color(0xffffcc5c) : Color(0xffffcc5c).withOpacity(0.6),
              border: new Border.all(
                  width: 1.0,
                  color:
                  _item.isSelected ? Color(0xffffcc5c) : Color(0x33ffcc5c)),
              borderRadius: const BorderRadius.all(const Radius.circular(40.0)),
            ),
          ),

          Flexible(
          child:
          new Container(
            margin: new EdgeInsets.only(left: 10.0),
            child: Column(

              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                if(_item.campus != null && _item.campus != "" && _item.topId > 0)
                Text(_item.campus,
                  textAlign: TextAlign.left,
                  style: TextStyle(fontSize: 12.0,
                      color: Colors.white,
                      fontWeight: (_item.isSelected)?FontWeight.bold:null
                  ),
                ),
                new Text(

                  _item.text,
                  style: TextStyle(fontSize: 20.0,
                      color: Colors.white,
                      fontWeight: (_item.isSelected)?FontWeight.bold:null
                  ),
                ),
              ],
            )


          )
          )
        ],
      ),
    );
  }
}
