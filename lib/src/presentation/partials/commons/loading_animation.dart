

import 'dart:math';

import 'package:flutter/material.dart';



class LoadingAnimation extends StatefulWidget {
  @override
  _LoadingAnimationState createState() => _LoadingAnimationState();
}

class _LoadingAnimationState extends State<LoadingAnimation> with SingleTickerProviderStateMixin{


  AnimationController _controller;


  @override
  void initState() {
    super.initState();

    _controller =
    AnimationController(  vsync: this, duration: Duration(seconds: 1))
      ..repeat();

  }


  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Container(
            color: Color(0xff636363),
            child: AnimatedBuilder(
                animation: _controller,
                builder: (_, child) {
                  return Transform.rotate(
                    angle: _controller.value * 2 * pi,
                    child: child,
                  );
                },

                child: Image.asset('assets/icon/icon.png',
                    height: 100, width: 100)))
    );
  }

}
