import 'dart:convert';

import 'package:circular/src/data/models/circular_event_model.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart'; //for date locale

class ItemEventPartial extends StatefulWidget {
  CircularEventModel circularEvent;
  String _dateBarPosition;

  bool isHome;


  ItemEventPartial(this.circularEvent, this._dateBarPosition, this.isHome);



  @override
  _ItemEventPartialState createState() => _ItemEventPartialState(circularEvent, _dateBarPosition, this.isHome);
}

class _ItemEventPartialState extends State<ItemEventPartial> {

  bool hasInternet=true;
  CircularEventModel circularEvent;
  String _dateBarPosition = "BOTTOM";
  bool isHome;


  _ItemEventPartialState(this.circularEvent, this._dateBarPosition, this.isHome);

  @override
  void initState() {

    askForInternet().then((value) {
      setState(() {
        this.hasInternet = value;
      });

    });

  }


  @override
  Widget build(BuildContext context) {
    Intl.defaultLocale = 'es_ES';

    return Container(
      margin: EdgeInsets.only(left: 3.0, right: 3.0),
      child: Stack(
        children: <Widget>[

          getImageWigget(context),

          Positioned(
            top: 0.0,
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  if (widget._dateBarPosition == "TOP")
                    Container(
                        width: (this.isHome)?MediaQuery.of(context).size.width*0.49:MediaQuery.of(context).size.width,
                        color: Color(0xff666666),
                        child: Row(children: <Widget>[
                          Container(
                            width: 50.0,
                            child: Text(""),
                          ),
                          Expanded(
                            child: getDateFormat(this.circularEvent.currentDate),
                          ),
                          Container(
                              //color: Colors.red,
                              child: getHourFormat(this.circularEvent.currentDate))
                        ])),
                  Container(
                      width: MediaQuery.of(context).size.width,
                      color: Color(0x00313130).withOpacity(0.95),
                      child: Padding(
                        padding: EdgeInsets.only(left: 5.0, right: 5.0),
                        child: Text(
                          "${widget.circularEvent.campus}",
                          style: TextStyle(color: Colors.white),
                        ),
                      )),
                  Container(
                      width: (this.isHome)?MediaQuery.of(context).size.width/2:MediaQuery.of(context).size.width,
                      color: Color(0x00313130).withOpacity(0.95),
                      child: Padding(
                        padding: EdgeInsets.only(left: 5.0, right: 5.0),
                        child: Text(
                          "${widget.circularEvent.title}",
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      )),
                  Container(
                    color: Colors.white.withOpacity(0.0),
                    child: ClipPath(
                      clipper: TrapeziumClipper(),
                      child: Container(
                        color: Color(0x00313130).withOpacity(0.95),
                        padding: EdgeInsets.all(8.0),
                        width: MediaQuery.of(context).size.width,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            ConstrainedBox(
                              constraints: BoxConstraints(
                                  maxWidth: MediaQuery.of(context).size.width *
                                      6 /
                                      15),
                              child: Text(''),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          if (widget._dateBarPosition == "BOTTOM")
            Positioned(
                bottom: 0.0,
                child: Container(
                    width: (this.isHome)?MediaQuery.of(context).size.width*0.55:MediaQuery.of(context).size.width,
                    color: Color(0xff666666),
                    child: Row(children: <Widget>[
                      Container(
                        width: 50.0,
                        child: Text(""),
                      ),
                      Expanded(
                        child: getDateFormat(this.circularEvent.currentDate),
                      ),
                      Container(
                          margin: EdgeInsets.only(right: 40.0),
                          child: getHourFormat(this.circularEvent.currentDate))
                    ])))
        ],
      ),
    );
  }

  Widget getDateFormat(DateTime dateTime) {
    final df = new DateFormat('MMM dd');
    var date = df.format(dateTime);

    return Text(
      date,
      style: TextStyle(color: Colors.white),
    );
  }

  Widget getHourFormat(DateTime dateTime) {
    final df = new DateFormat('hh:mm a');
    var date = df.format(dateTime);

    return Text(
      "$date",
      textAlign: TextAlign.right,
      style: TextStyle(
        color: Colors.white,
      ),
    );
  }

  Widget getImageWigget(context)  {

    if (!this.hasInternet) {
      // Si No hay coneccion a internet, carga los eventos de la db local

      return  Image.memory(
            base64Decode(widget.circularEvent.imgBase64),
            fit: BoxFit.cover,
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            alignment: Alignment.center,
          );

    } else  {
      // Si hay coneccion por wifi o Si hay coneccion por datos


      return Image.network(
        widget.circularEvent.imgUrl,
        fit: BoxFit.fill,
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        alignment: Alignment.center,
      );

    }





  }

  Future<bool>  askForInternet() async {
    var connectivityResult = await Connectivity().checkConnectivity();

    if (connectivityResult == ConnectivityResult.none) {
      return false;
    }
    return true;
  }


}

class TrapeziumClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = Path();
    path.lineTo(0.0, 0.0);
    path.lineTo(0.0, size.height);
    path.lineTo(size.width, 0.0);

    path.close();

    return path;
  }

  @override
  bool shouldReclip(TrapeziumClipper oldClipper) => false;
}
