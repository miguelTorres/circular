import 'dart:convert';
import 'dart:io' as Io;
import 'dart:developer' as dev;
import 'package:circular/src/presentation/partials/commons/loading_animation.dart';
import 'package:flutter/cupertino.dart';
import 'package:image/image.dart' as img;
import 'package:circular/src/business_logic/blocs/event_create_bloc.dart';
import 'package:circular/src/business_logic/blocs/event_edit_bloc.dart';
import 'package:circular/src/data/models/circular_event_model.dart';
import 'package:circular/src/data/models/date_time_range.dart';
import 'package:circular/src/data/models/location_model.dart';
import 'package:circular/src/data/models/organizer_model.dart';
import 'package:circular/src/presentation/partials/event_partials/form_category_selector.dart';
import 'package:circular/src/presentation/templates/user_pages/user_events_page.dart';
import 'package:circular/src/presentation/templates/user_pages/user_login_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:circular/src/presentation/partials/event_partials/form_dependences_selector.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:html/parser.dart';

import 'date_picker_selector.dart';

// Define un widget de formulario personalizado
class EventForm extends StatefulWidget {
  String operation;
  //Este evento se carga si la operación es EDIT
  CircularEventModel circularEvent;

  EventForm(operation, circularEvent) {
    this.operation = operation;

    if (operation == "CREATE") {
      this.circularEvent = null;
    }

    if (operation == "EDIT") {
      this.circularEvent = circularEvent;
    }
  }

  @override
  EventFormState createState() {
    return EventFormState(operation, circularEvent);
  }
}

// Define una clase de estado correspondiente. Esta clase contendrá los datos
// relacionados con el formulario.
class EventFormState extends State<EventForm> {
  String _operation;
  //Este evento se carga si la operación es EDIT
  CircularEventModel _circularEvent;

  // Crea una clave global que identificará de manera única el widget Form
  // y nos permita validar el formulario
  //
  // Nota: Esto es un GlobalKey<FormState>, no un GlobalKey<MyCustomFormState>!

  final _formKey = GlobalKey<FormState>();

  final bloc = EventCreateBloc();
  final blocEdit = EventEditBloc();
  var isLoading = false;

  int _userUid;
  String _username;
  String _userPass;
  String _userType;

  PickedFile _imageFile;
  final ImagePicker _picker = ImagePicker();
  bool _flagImage = false;
  String _dependenceText = "";
  String _areaText = "";
  String _typeText = "";
  String _specialText = "";
  String _responsableDependenceText = "";

  // ### Variables del evento ###

  String _title;
  String _description;
  String _guest;
  String _numInstitucionalImage;
  String _keywords;

  DateTime _inscriptionDateInitial;
  DateTime _inscriptionDateFinal;
  String _inscriptionWay;
  CircularLocation _newCircularLocation;
  int dependenceUidSelected;
  int areaUidSelected;
  int typeUidSelected;
  int specialUidSelected;
  String _typeTyckets;
  String _tycketsUrl;
  String _tycketsCost;
  String eventImage64 = "";
  String _imageCredits = "";
  String _address = "";
  String _responsableFirstName = "";
  String _responsableLastName = "";
  String _responsablePosition = "";
  String _responsableInstitutionalEmail = "";
  String _responsablePhone = "";
  String _responsableExtension = "";
  int _responsableDependence;

  List<DateTimeRange> _eventDatesList = new List();

  // ### evento nuevo a asignar valores
  CircularEventModel _newCircularEvent;

  //#### controladores para cada campo del Fromulario
  //Nota: con los controladores se puede limpiar los campos y editarlos

  TextEditingController _titleController;
  TextEditingController _dependenceController;
  TextEditingController _areaController;
  TextEditingController _typeController;
  TextEditingController _specialController;
  TextEditingController _descriptionController;
  TextEditingController _guestController;
  TextEditingController _numInstitucionalImageController;
  TextEditingController _keywordsController;
  TextEditingController _typeTycketsController;
  TextEditingController _tycketsUrlController;
  TextEditingController _tycketsCostController;
  TextEditingController _imageCreditsController;
  TextEditingController _inscriptionDateInitialController;
  TextEditingController _inscriptionDateFinalController;
  TextEditingController _inscriptionWayController;
  TextEditingController _addressController;

  TextEditingController _responsableFirstNameController;
  TextEditingController _responsableLastNameController;
  TextEditingController _responsablePositionController;
  TextEditingController _responsableInstitutionalEmailController;
  TextEditingController _responsablePhoneController;
  TextEditingController _responsableExtensionController;
  TextEditingController _responsableDependenceController;

  EventFormState(operation, circularEvent) {
    this._operation = operation;
    this._circularEvent = circularEvent;

  }

  @override
  Future<void> initState() {
    verifySesion();
    _inscriptionDateInitial = null;
    _inscriptionDateFinal = null;
    initializeDateFormatting('es_ES');

    super.initState();
    _titleController = TextEditingController();
    _descriptionController = TextEditingController();
    _dependenceController = TextEditingController();
    _areaController = TextEditingController();
    _typeController = TextEditingController();
    _specialController = TextEditingController();
    _guestController = TextEditingController();
    _numInstitucionalImageController = TextEditingController();
    _keywordsController = TextEditingController();
    _typeTycketsController = TextEditingController();
    _tycketsUrlController = TextEditingController();
    _tycketsCostController = TextEditingController();
    _imageCreditsController = TextEditingController();
    _inscriptionDateInitialController = TextEditingController();
    _inscriptionDateFinalController = TextEditingController();
    _addressController = TextEditingController();

    _responsableFirstNameController = TextEditingController();
    _responsableLastNameController = TextEditingController();
    _responsablePositionController = TextEditingController();
    _responsableInstitutionalEmailController = TextEditingController();
    _responsablePhoneController = TextEditingController();
    _responsableDependenceController = TextEditingController();
    _responsableExtensionController = TextEditingController();
    _inscriptionWayController = TextEditingController();

    _newCircularEvent = new CircularEventModel();
    _newCircularLocation =
        new CircularLocation(null, 4.6381938, -74.0840464, "");

    //Si es edición carga la información en el formjulario
    if (this._operation == "EDIT") {
      if (_circularEvent != null) {
        _title = _titleController.text = _circularEvent.title;
        _description = _descriptionController.text = _parseHtmlString(_circularEvent.description);
        _dependenceText =
            _dependenceController.text = _circularEvent.dependenciaName;
        dependenceUidSelected = _circularEvent.dependenciaUid;

        areaUidSelected = _circularEvent.areaUid;
        _areaText = _areaController.text = _circularEvent.areaName;
        typeUidSelected = _circularEvent.typeUid;
        _typeText = _typeController.text = _circularEvent.typeName;
        specialUidSelected = _circularEvent.specialUid;
        _specialText = _specialController.text = _circularEvent.specialName;

        _guest = _guestController.text = _circularEvent.invitados;

        _numInstitucionalImage = _numInstitucionalImageController.text =
            _circularEvent.numInstitutionalImage;
        _keywords = _keywordsController.text = _circularEvent.keywords;
        _typeTyckets = _typeTycketsController.text = _circularEvent.ticketsType;
        _tycketsUrl = _tycketsUrlController.text = _circularEvent.ticketsUrl;
        _tycketsCost = _tycketsCostController.text = _circularEvent.ticketsCost;

        eventImage64 = _circularEvent.imgBase64;
        _imageCredits =
            _imageCreditsController.text = _circularEvent.imageCredits;

        if (_circularEvent.inscriptionDateInitial != null) {
          _inscriptionDateInitial = _circularEvent.inscriptionDateInitial;

          String loadFormattedDateInitial =
              DateFormat('d/MMM/yyyy - HH:mm').format(_inscriptionDateInitial);

          _inscriptionDateInitialController.text = loadFormattedDateInitial;
        }

        if (_circularEvent.inscriptionDateInitial != null) {
          _inscriptionDateFinal = _circularEvent.inscriptionDateFinal;

          String loadFormattedDateFinal =
              DateFormat('d/MMM/yyyy - HH:mm').format(_inscriptionDateFinal);

          _inscriptionDateFinalController.text = loadFormattedDateFinal;
        }

        _inscriptionWay =
            _inscriptionWayController.text = _parseHtmlString(_circularEvent.inscriptionWay);
        _address =
            _addressController.text = _circularEvent.circularLocation.address;

        _responsableFirstName = _responsableFirstNameController.text =
            _circularEvent.organizer.firstName;
        _responsableLastName = _responsableLastNameController.text =
            _circularEvent.organizer.lastName;
        _responsablePosition = _responsablePositionController.text =
            _circularEvent.organizer.position;
        _responsableInstitutionalEmail =
            _responsableInstitutionalEmailController.text =
                _circularEvent.organizer.institutionalEmail;
        _responsablePhone =
            _responsablePhoneController.text = _circularEvent.organizer.phone;
        _responsableExtension = _responsableExtensionController.text =
            _circularEvent.organizer.extension;
        _responsableDependenceText = _responsableDependenceController.text =
            _circularEvent.organizer.dependenceName;
        _responsableDependence = _circularEvent.organizer.uidDependence;

        _eventDatesList = _circularEvent.eventDates;

        _flagImage = true;
      }
    }
  }

  @override
  void dispose() {
    _titleController.dispose();
    _dependenceController.dispose();
    _areaController.dispose();
    _typeController.dispose();
    _specialController.dispose();
    _guestController.dispose();
    _numInstitucionalImageController.dispose();
    _keywordsController.dispose();
    _descriptionController.dispose();
    _typeTycketsController.dispose();
    _tycketsUrlController.dispose();
    _tycketsCostController.dispose();
    _imageCreditsController.dispose();
    _inscriptionDateInitialController.dispose();
    _inscriptionDateFinalController.dispose();
    _addressController.dispose();

    _responsableFirstNameController.dispose();
    _responsableLastNameController.dispose();
    _responsablePositionController.dispose();
    _responsableInstitutionalEmailController.dispose();
    _responsablePhoneController.dispose();
    _responsableDependenceController.dispose();
    _responsableExtensionController.dispose();
    _inscriptionWayController.dispose();

    bloc.dispose();
    blocEdit.dispose();

    super.dispose();
  }

  Future<void> verifySesion() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    if (prefs.getInt("uid") != null) {
      _userUid = prefs.getInt("uid");
      _username = prefs.getString("username");
      _userPass = prefs.getString("pass");
      _userType = prefs.getString("type");
    } else {
      //No hay sesión de usuario lo redirige
      Navigator.pop(context);
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => UserLoginPage()),
      );
    }
  }

  void _onImageButtonPressed(ImageSource source) async {
    try {
      final pickedFile = await _picker.getImage(source: source);
      setState(() async {
        _imageFile = pickedFile;

        var result = _imageFile.path;

        final bytes = Io.File(result).readAsBytesSync();

        if(bytes.length > 500000){
          setState(() {
            _flagImage = false;
          });

          showDialog(
              context: context,
              builder: (_) => new AlertDialog(
                backgroundColor: Color(0xffffcc5c),
                title: new Text(
                    "Ingrese una imagen menor a 800 Kb"),
                actions: <Widget>[
                  FlatButton(
                    child:
                    Text('Continuar',
                      style: TextStyle(
                          color: Colors.black
                      ),),
                    //color: Colors.black,
                    onPressed: () {
                      Navigator.of(context).pop(); // Cierra popup de confirmación

                    },
                  ),

                ],
              ));
        }else{
          eventImage64 = base64Encode(bytes);
          setState(() {
            _flagImage = true;
          });
        }


      });
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    // Crea un widget Form usando el _formKey que creamos anteriormente
    return Stack(
        children:[
          Form(
              key: _formKey,
              child: Container(
                  margin: EdgeInsets.only(top:30.0,left: 30.0, right: 30.0),
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "Información básica".toUpperCase(),
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 20.0,
                              color: Color(0xffffcc5c),
                              fontWeight: FontWeight.bold
                          ),
                        ),
                        TextFormField(
                          controller: _titleController,
                          decoration: InputDecoration(
                            labelText: "Título *",
                            labelStyle: TextStyle(color: Color(0xffffcc5c)),
                      /*enabledBorder: new UnderlineInputBorder(
                          borderSide: new BorderSide(
                              color: Color(0xff333333)
                          )
                      ),*/
                            suffix: GestureDetector(
                                child: IconButton(icon: Icon(Icons.clear)),
                                onTap: () {
                                  _titleController.clear();
                                }),
                          ),
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Ingrese un texto';
                            }
                            return null;
                          },
                          onChanged: (value) {
                            if (value != "") {
                              setState(() {
                                _title = value;
                              });
                            }
                          },
                        ),
                        TextFormField(
                          readOnly: true,
                          controller: _dependenceController,
                          decoration: InputDecoration(
                            labelText: "Dependencia *",
                           labelStyle: TextStyle(color: Color(0xffffcc5c)),
                            /*enabledBorder: new UnderlineInputBorder(
                                borderSide: new BorderSide(
                                    color: Color(0xff333333)
                                )
                            ),*/
                          ),

                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Ingrese un texto';
                            }
                            return null;
                          },
                          onTap: () async {
                            final result1 = await Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      FormDependencesSelector(dependenceUidSelected)),
                            );

                            if (result1 != null) {
                              setState(() {
                                dependenceUidSelected =
                                result1['dependenceUidSelected'];
                                _dependenceText = result1['dependenceSelectedText'];
                                _dependenceController.text = _dependenceText;
                              });
                            }
                          },
                        ),
                        TextFormField(
                          readOnly: true,
                          controller: _areaController,
                          decoration: InputDecoration(
                            labelText: "Área temática *",
                           labelStyle: TextStyle(color: Color(0xffffcc5c)),
                            /*enabledBorder: new UnderlineInputBorder(
                                borderSide: new BorderSide(
                                    color: Color(0xff333333)
                                )
                            ),*/
                          ),
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Ingrese un texto';
                            }
                            return null;
                          },
                          onTap: () async {
                            final result2 = await Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      FormCategorySelector("area", areaUidSelected)),
                            );

                            if (result2 != null) {
                              setState(() {
                                areaUidSelected = result2['categoryUidSelected'];
                                _areaText = result2['categorySelectedText'];
                                _areaController.text = _areaText;
                              });
                            }
                          },
                        ),
                        TextFormField(
                          readOnly: true,
                          controller: _typeController,
                          decoration: InputDecoration(
                            labelText: "Tipo *",
                           labelStyle: TextStyle(color: Color(0xffffcc5c)),
                            /*enabledBorder: new UnderlineInputBorder(
                                borderSide: new BorderSide(
                                    color: Color(0xff333333)
                                )
                            ),*/
                          ),
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Ingrese un texto';
                            }
                            return null;
                          },
                          onTap: () async {
                            final result3 = await Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      FormCategorySelector("tipo", typeUidSelected)),
                            );

                            if (result3 != null) {
                              setState(() {
                                typeUidSelected = result3['categoryUidSelected'];
                                _typeText = result3['categorySelectedText'];
                                _typeController.text = _typeText;
                              });
                            }
                          },
                        ),
                        TextFormField(
                          readOnly: true,
                          controller: _specialController,
                          decoration: InputDecoration(
                            labelText: "Categoría especial",
                           labelStyle: TextStyle(color: Color(0xffffcc5c)),
                            /*enabledBorder: new UnderlineInputBorder(
                                borderSide: new BorderSide(
                                    color: Color(0xff333333)
                                )
                            ),*/
                          ),
                          onTap: () async {
                            final result3 = await Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => FormCategorySelector(
                                      "especial", specialUidSelected)),
                            );

                            if (result3 != null) {
                              setState(() {
                                specialUidSelected = result3['categoryUidSelected'];
                                _specialText = result3['categorySelectedText'];
                                _specialController.text = _specialText;
                              });
                            }
                          },
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        TextFormField(
                          maxLines: null,
                          controller: _descriptionController,
                          decoration: InputDecoration(
                              labelText: "Información adicional *",
                              labelStyle: TextStyle(color: Color(0xffffcc5c),),
                              /*enabledBorder: new UnderlineInputBorder(
                                  borderSide: new BorderSide(
                                      color: Color(0xff333333)
                                  )
                              ),*/
                              suffix: GestureDetector(
                                  child: IconButton(icon: Icon(Icons.clear)),
                                  onTap: () {
                                    _descriptionController.clear();
                                  }),
                          /*    border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(2.0))
                          */
                          ),
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Ingrese un texto';
                            }
                            return null;
                          },
                          onChanged: (value) {
                            if (value != "") {
                              setState(() {
                                _description = value;
                              });
                            }
                          },
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        TextFormField(
                          maxLines: null,
                          controller: _guestController,
                          decoration: InputDecoration(
                              labelText: "Invitados",
                              labelStyle: TextStyle(color: Color(0xffffcc5c)),

                              /*enabledBorder: new UnderlineInputBorder(
                                  borderSide: new BorderSide(
                                      color: Color(0xff333333)
                                  )
                              ),*/
                              suffix: GestureDetector(
                                  child: IconButton(icon: Icon(Icons.clear)),
                                  onTap: () {
                                    _guestController.clear();
                                  }),
                            /*  border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(2.0))
                          */
                          ),
                          onChanged: (value) {
                            if (value != "") {
                              setState(() {
                                _guest = value;
                              });
                            }
                          },
                        ),
                        TextFormField(
                          controller: _numInstitucionalImageController,
                          decoration: InputDecoration(
                            labelText: "Nº de aval de imagen institucional",
                            labelStyle: TextStyle(color: Color(0xffffcc5c),),

                            /*enabledBorder: new UnderlineInputBorder(
                                borderSide: new BorderSide(
                                    color: Color(0xff333333)
                                )
                            ),*/
                            suffix: GestureDetector(
                                child: IconButton(icon: Icon(Icons.clear)),
                                onTap: () {
                                  _numInstitucionalImageController.clear();
                                }),
                          ),
                          onChanged: (value) {
                            if (value != "") {
                              setState(() {
                                _numInstitucionalImage = value;
                              });
                            }
                          },
                        ),
                        TextFormField(
                          controller: _keywordsController,
                          decoration: InputDecoration(
                            labelText: "Palabras clave *",
                            hintText: "palabra1;palabra2;palabra3 ...",
                            labelStyle: TextStyle(color: Color(0xffffcc5c)),
                            /*enabledBorder: new UnderlineInputBorder(
                                borderSide: new BorderSide(
                                    color: Color(0xff333333)
                                )
                            ),*/
                            suffix: GestureDetector(
                                child: IconButton(icon: Icon(Icons.clear)),
                                onTap: () {
                                  _keywordsController.clear();
                                }),
                          ),
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Ingrese un texto';
                            }
                            return null;
                          },
                          onChanged: (value) {
                            if (value != "") {
                              setState(() {
                                _keywords = value;
                              });
                            }
                          },
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        DropdownButton<String>(
                          //dropdownColor: Colors.white,
                          isExpanded: true,
                          hint: Text(
                            'Opciones de boletería *',
                            style: TextStyle(color: Color(0xffffcc5c)),
                          ),
                          value: _typeTyckets,
                          items: <String>['Entrada libre', 'Boleta', 'Url']
                              .map((String value) {
                            return new DropdownMenuItem(
                                value: value.toUpperCase().replaceAll(' ', '_'),
                                child: Text(value,
                                  style: TextStyle(
                                      color: Colors.white
                                  ),
                                ));
                          }).toList(),
                          onChanged: (data) {
                            setState(() {
                              _typeTyckets = data;
                            });
                          },
                        ),
                        if (_typeTyckets == "BOLETA")
                          TextFormField(
                            keyboardType: TextInputType.number,
                            controller: _tycketsCostController,
                            decoration: InputDecoration(
                              labelText: "Ingrese el costo de la boleta *",
                              labelStyle: TextStyle(color: Color(0xffffcc5c)),
                             /*enabledBorder: new UnderlineInputBorder(
                                  borderSide: new BorderSide(
                                      color: Color(0xff333333)
                                  )
                              ),*/
                              suffix: GestureDetector(
                                  child: IconButton(icon: Icon(Icons.clear)),
                                  onTap: () {
                                    _tycketsCostController.clear();
                                  }),
                            ),
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Ingrese un valor';
                              }
                              return null;
                            },
                            onChanged: (value) {
                              if (value != "") {
                                setState(() {
                                  _tycketsCost = value;
                                });
                              }
                            },
                          ),
                        if (_typeTyckets == "URL")
                          TextFormField(
                            keyboardType: TextInputType.url,
                            controller: _tycketsUrlController,
                            decoration: InputDecoration(
                              hintText: "https://www.google.com",
                              labelText: "Ingrese la url de la boletería *",
                             labelStyle: TextStyle(color: Color(0xffffcc5c)),
                              /*enabledBorder: new UnderlineInputBorder(
                                  borderSide: new BorderSide(
                                      color: Color(0xff333333)
                                  )
                              ),*/
                              suffix: GestureDetector(
                                  child: IconButton(icon: Icon(Icons.clear)),
                                  onTap: () {
                                    _tycketsUrlController.clear();
                                  }),
                            ),
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Ingrese una url';
                              }

                              if (!validateUrl(value)) {
                                return 'Url inválida';
                              }

                              return null;
                            },
                            onChanged: (value) {
                              if (value != "") {
                                setState(() {
                                  _tycketsUrl = value;
                                });
                              }
                            },
                          ),
                        SizedBox(
                          height: 10.0,
                        ),
                        SizedBox(
                          width: double.maxFinite, // set width to maxFinite

                          child: RaisedButton(
                            color: Color(0xffffcc5c),
                            child: Text(
                              _flagImage
                                  ? "Cambiar imagen del evento"
                                  : "Seleccione la imagen del evento *",
                              style: TextStyle(color: Colors.black),
                            ),
                            onPressed: () {
                              _onImageButtonPressed(ImageSource.gallery);
                            },
                          ),
                        ),
                        TextFormField(
                          controller: _imageCreditsController,
                          decoration: InputDecoration(
                            labelText: "Créditos de la imagen",
                           labelStyle: TextStyle(color: Color(0xffffcc5c)),
                            /*enabledBorder: new UnderlineInputBorder(
                                borderSide: new BorderSide(
                                    color: Color(0xff333333)
                                )
                            ),*/
                            suffix: GestureDetector(
                                child: IconButton(icon: Icon(Icons.clear)),
                                onTap: () {
                                  _imageCreditsController.clear();
                                }),
                          ),
                          onChanged: (value) {
                            if (value != "") {
                              setState(() {
                                _imageCredits = value;
                              });
                            }
                          },
                        ),
                        Divider(),
                        Text(
                          "Periodo de inscripción".toUpperCase(),
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 20.0,
                              fontWeight: FontWeight.bold,
                              color:Color(0xffffcc5c)
                          ),
                        ),
                        Container(
                            width: MediaQuery.of(context).size.width,
                            child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Flexible(
                                    child: TextFormField(
                                      readOnly: true,
                                      controller: _inscriptionDateInitialController,
                                      decoration: InputDecoration(
                                        labelText: "Inicio de inscripción",
                                        labelStyle:
                                        TextStyle(color: Color(0xffffcc5c)),
                                        /*enabledBorder: new UnderlineInputBorder(
                                            borderSide: new BorderSide(
                                                color: Color(0xff333333)
                                            )
                                        ),*/
                                      ),

                                      onTap: () async {
                                        showDatePicker(
                                            context: context,
                                            locale: const Locale("es", "ES"),
                                            initialDate: DateTime.now(),
                                            firstDate: DateTime(2001),
                                            lastDate: DateTime(2222))
                                            .then((selectedDate) {
                                          if (selectedDate != null) {
                                            var dateNow = DateTime.now();
                                            showTimePicker(
                                                context: context,
                                                initialTime: TimeOfDay(
                                                    hour: dateNow.hour,
                                                    minute: dateNow.minute))
                                                .then((selectedTime) {
                                              if (selectedTime != null) {
                                                setState(() {
                                                  _inscriptionDateInitial = DateTime(
                                                    selectedDate.year,
                                                    selectedDate.month,
                                                    selectedDate.day,
                                                    selectedTime.hour,
                                                    selectedTime.minute,
                                                  );
                                                  String formattedDate = DateFormat(
                                                      'd/MMM/yyyy - HH:mm')
                                                      .format(
                                                      _inscriptionDateInitial);

                                                  _inscriptionDateInitialController
                                                      .text = formattedDate;
                                                });
                                              }
                                            });
                                          }
                                        });
                                      },
                                      validator: (value) {
                                        if (value.isNotEmpty) {
                                          if (_inscriptionDateInitial
                                              .millisecondsSinceEpoch >
                                              _inscriptionDateFinal
                                                  .millisecondsSinceEpoch) {
                                            return "Fecha mayor que la fecha final";
                                          }
                                        }
                                        return null;
                                      },
                                    ),
                                  ),
                                  if (_inscriptionDateInitial != null)
                                    IconButton(
                                        icon: Icon(Icons.clear),
                                        onPressed: () {
                                          _inscriptionDateInitialController.clear();
                                          setState(() {
                                            _inscriptionDateInitial = null;
                                          });
                                        })
                                ])),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Flexible(
                                child: TextFormField(
                                  readOnly: true,
                                  controller: _inscriptionDateFinalController,
                                  decoration: InputDecoration(
                                    labelText: "Fin de inscripción",
                                    labelStyle:
                                    TextStyle(color: Color(0xffffcc5c)),
                                    /*enabledBorder: new UnderlineInputBorder(
                                        borderSide: new BorderSide(
                                            color: Color(0xff333333)
                                        )
                                    ),*/
                                  ),
                                  onTap: () async {
                                    showDatePicker(
                                        context: context,
                                        locale: const Locale("es", "ES"),
                                        initialDate: DateTime.now(),
                                        firstDate: DateTime(2001),
                                        lastDate: DateTime(2222))
                                        .then((selectedDate) {
                                      if (selectedDate != null) {
                                        var dateNow = DateTime.now();
                                        showTimePicker(
                                            context: context,
                                            initialTime: TimeOfDay(
                                                hour: dateNow.hour,
                                                minute: dateNow.minute))
                                            .then((selectedTime) {
                                          if (selectedTime != null) {
                                            setState(() {
                                              _inscriptionDateFinal = DateTime(
                                                selectedDate.year,
                                                selectedDate.month,
                                                selectedDate.day,
                                                selectedTime.hour,
                                                selectedTime.minute,
                                              );
                                              String formattedDate =
                                              DateFormat('d/MMM/yyyy - HH:mm')
                                                  .format(_inscriptionDateFinal);
                                              _inscriptionDateFinalController.text =
                                                  formattedDate;
                                            });
                                          }
                                        });
                                      }
                                    });
                                  },
                                  validator: (value) {
                                    if (value.isNotEmpty) {
                                      if (_inscriptionDateInitial
                                          .millisecondsSinceEpoch >
                                          _inscriptionDateFinal
                                              .millisecondsSinceEpoch) {
                                        return "Fecha menor que la fecha inicial";
                                      }
                                    }
                                    return null;
                                  },
                                ),
                              ),
                              if (_inscriptionDateFinal != null)
                                IconButton(
                                    icon: Icon(Icons.clear),
                                    onPressed: () {
                                      _inscriptionDateFinalController.clear();

                                      setState(() {
                                        _inscriptionDateFinal = null;
                                      });
                                    })
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        TextFormField(
                          maxLines: 3,
                          controller: _inscriptionWayController,
                          decoration: InputDecoration(
                            labelText: "Forma de inscripción",
                           labelStyle: TextStyle(color: Color(0xffffcc5c)),
                            /*enabledBorder: new UnderlineInputBorder(
                                borderSide: new BorderSide(
                                    color: Color(0xff333333)
                                )
                            ),*/
                            suffix: GestureDetector(
                                child: IconButton(icon: Icon(Icons.clear)),
                                onTap: () {
                                  _inscriptionWayController.clear();
                                }),
                            /*border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(2.0)),
                            */
                          ),
                          onChanged: (value) {
                            if (value != "") {
                              setState(() {
                                _inscriptionWay = value;
                              });
                            }
                          },
                        ),



                        Container(
                          margin: EdgeInsets.only(top: 5.0, bottom: 5.0),
                          child: SizedBox(
                            width: double.maxFinite,
                            child: RaisedButton(
                              color: Color(0xffffcc5c),
                              child: Text(
                                (_eventDatesList.length > 0)
                                    ? "Cambiar fechas del evento"
                                    : "Seleccione las fechas del evento *",
                                style: TextStyle(color: Colors.black),
                              ),
                              onPressed: () async {
                                var datesListResult = await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          DatePickerSelecetor(_eventDatesList)),
                                );
                                if (datesListResult != null) {
                                  setState(() {
                                    _eventDatesList = datesListResult;
                                  });
                                }
                              },
                            ),
                          ),
                        ),

                        Divider(),
                        Text(
                          "Ubicación".toUpperCase(),
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 20.0,
                              fontWeight: FontWeight.bold,
                              color:Color(0xffffcc5c)
                          ),
                        ),
                        TextFormField(
                          initialValue: "Universidad Nacional de Colombia",
                          readOnly: true,
                          decoration: InputDecoration(
                            labelText: "Mapa",
                           labelStyle: TextStyle(color: Color(0xffffcc5c)),
                            /*enabledBorder: new UnderlineInputBorder(
                                borderSide: new BorderSide(
                                    color: Color(0xff333333)
                                )
                            ),*/
                          ),
                          onTap: () {
                            AlertDialog alert = AlertDialog(
                              content: Text(
                                  "Se visualizará en el Mapa a la Universidad Nacional "
                                      "de Colombia, sede Bogotá como ubicación por defecto, "
                                      "si desea especificar un edificio de la universidad o "
                                      "cambiar de ubiciación, especifique los detalles en "
                                      "el campo de dirección y la cambiaremos en el mapa "
                                      "tan pronto validemos la información de su evento. "),
                              actions: [
                                FlatButton(
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                  child: Text("OK"),
                                ),
                              ],
                            );

                            showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return alert;
                                });
                          },
                        ),
                        TextFormField(
                          controller: _addressController,
                          decoration: InputDecoration(
                            labelText: "Dirección *",
                           labelStyle: TextStyle(color: Color(0xffffcc5c)),
                            /*enabledBorder: new UnderlineInputBorder(
                                borderSide: new BorderSide(
                                    color: Color(0xff333333)
                                )
                            ),*/
                            suffix: GestureDetector(
                                child: IconButton(icon: Icon(Icons.clear)),
                                onTap: () {
                                  _addressController.clear();
                                }),
                          ),
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Ingrese un texto';
                            }
                            return null;
                          },
                          onChanged: (value) {
                            if (value != "") {
                              setState(() {
                                _address = value;
                              });
                            }
                          },
                        ),
                        Divider(),
                        Text(
                          "Datos del responsable".toUpperCase(),
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 20,
                              fontWeight: FontWeight.bold,
                              color: Color(0xffffcc5c)

                          ),
                        ),
                        TextFormField(
                          controller: _responsableFirstNameController,
                          decoration: InputDecoration(
                            labelText: "Nombres *",
                           labelStyle: TextStyle(color: Color(0xffffcc5c)),
                            /*enabledBorder: new UnderlineInputBorder(
                                borderSide: new BorderSide(
                                    color: Color(0xff333333)
                                )
                            ),*/
                            suffix: GestureDetector(
                                child: IconButton(icon: Icon(Icons.clear)),
                                onTap: () {
                                  _responsableFirstNameController.clear();
                                }),
                          ),
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Ingrese un texto';
                            }
                            return null;
                          },
                          onChanged: (value) {
                            if (value != "") {
                              setState(() {
                                _responsableFirstName = value;
                              });
                            }
                          },
                        ),
                        TextFormField(
                          controller: _responsableLastNameController,
                          decoration: InputDecoration(
                            labelText: "Apellidos *",
                            labelStyle: TextStyle(color: Color(0xffffcc5c)),
                            /*enabledBorder: new UnderlineInputBorder(
                                borderSide: new BorderSide(
                                    color: Color(0xff333333)
                                )
                            ),*/
                            suffix: GestureDetector(
                                child: IconButton(icon: Icon(Icons.clear)),
                                onTap: () {
                                  _responsableLastNameController.clear();
                                }),
                          ),
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Ingrese un texto';
                            }
                            return null;
                          },
                          onChanged: (value) {
                            if (value != "") {
                              setState(() {
                                _responsableLastName = value;
                              });
                            }
                          },
                        ),
                        TextFormField(
                          readOnly: true,
                          controller: _responsableDependenceController,
                          decoration: InputDecoration(
                            labelText: "Dependencia *",
                            labelStyle: TextStyle(color: Color(0xffffcc5c)),

                            /*enabledBorder: new UnderlineInputBorder(
                                borderSide: new BorderSide(
                                    color: Color(0xff333333)
                                )
                            ),*/
                          ),

                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Ingrese un texto';
                            }
                            return null;
                          },
                          onTap: () async {
                            final result = await Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => FormDependencesSelector(
                                      _responsableDependence)),
                            );

                            if (result != null) {
                              setState(() {
                                _responsableDependence =
                                result['dependenceUidSelected'];
                                _responsableDependenceText =
                                result['dependenceSelectedText'];
                                _responsableDependenceController.text =
                                    _responsableDependenceText;
                              });
                            }
                          },
                        ),
                        TextFormField(
                          keyboardType: TextInputType.emailAddress,
                          controller: _responsableInstitutionalEmailController,
                          decoration: InputDecoration(
                            labelText: "Correo institucional *",
                            labelStyle: TextStyle(color: Color(0xffffcc5c)),
                            /*enabledBorder: new UnderlineInputBorder(
                                borderSide: new BorderSide(
                                    color: Color(0xff333333)
                                )
                            ),*/
                            suffix: GestureDetector(
                                child: IconButton(icon: Icon(Icons.clear)),
                                onTap: () {
                                  _responsableInstitutionalEmailController.clear();
                                }),
                          ),
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Ingrese un texto';
                            }

                            if (!validateEmail(value)) {
                              return 'Correo inválido';
                            }

                            return null;
                          },
                          onChanged: (value) {
                            if (value != "") {
                              setState(() {
                                _responsableInstitutionalEmail = value;
                              });
                            }
                          },
                        ),
                        TextFormField(
                          controller: _responsablePositionController,
                          decoration: InputDecoration(
                            labelText: "Cargo *",
                            labelStyle: TextStyle(color: Color(0xffffcc5c)),
                            /*enabledBorder: new UnderlineInputBorder(
                                borderSide: new BorderSide(
                                    color: Color(0xff333333)
                                )
                            ),*/
                            suffix: GestureDetector(
                                child: IconButton(icon: Icon(Icons.clear)),
                                onTap: () {
                                  _responsablePositionController.clear();
                                }),
                          ),
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Ingrese un texto';
                            }
                            return null;
                          },
                          onChanged: (value) {
                            if (value != "") {
                              setState(() {
                                _responsablePosition = value;
                              });
                            }
                          },
                        ),
                        TextFormField(
                          keyboardType: TextInputType.number,
                          controller: _responsablePhoneController,
                          decoration: InputDecoration(
                            labelText: "Teléfono *",
                           labelStyle: TextStyle(color: Color(0xffffcc5c)),
                            /*enabledBorder: new UnderlineInputBorder(
                                borderSide: new BorderSide(
                                    color: Color(0xff333333)
                                )
                            ),*/
                            suffix: GestureDetector(
                                child: IconButton(icon: Icon(Icons.clear)),
                                onTap: () {
                                  _responsablePhoneController.clear();
                                }),
                          ),
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Ingrese un texto';
                            }
                            return null;
                          },
                          onChanged: (value) {
                            if (value != "") {
                              setState(() {
                                _responsablePhone = value;
                              });
                            }
                          },
                        ),
                        TextFormField(
                          keyboardType: TextInputType.number,
                          controller: _responsableExtensionController,
                          decoration: InputDecoration(
                            labelText: "Extensión *",
                           labelStyle: TextStyle(color: Color(0xffffcc5c)),
                            /*enabledBorder: new UnderlineInputBorder(
                                borderSide: new BorderSide(
                                    color: Color(0xff333333)
                                )
                            ),*/
                            suffix: GestureDetector(
                                child: IconButton(icon: Icon(Icons.clear)),
                                onTap: () {
                                  _responsableExtensionController.clear();
                                }),
                          ),
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Ingrese un texto';
                            }
                            return null;
                          },
                          onChanged: (value) {
                            if (value != "") {
                              setState(() {
                                _responsableExtension = value;
                              });
                            }
                          },
                        ),
                        Center(
                          child:
                          RaisedButton(
                            color: Color(0xffffcc5c),
                            onPressed: () {

                              // devolverá true si el formulario es válido, o falso si
                              // el formulario no es válido.
                              if (!_formKey.currentState.validate()) {
                                // Si el formulario no es válido, queremos mostrar un Snackbar
                                Scaffold.of(context).showSnackBar(
                                    _displaySnackBar("Campos incompletos"));
                              } else if (_eventDatesList.length == 0) {
                                Scaffold.of(context).showSnackBar(
                                    _displaySnackBar("Ingrese las fechas del evento"));
                              } else if (!_flagImage) {
                                Scaffold.of(context).showSnackBar(
                                    _displaySnackBar("Ingrese la imagen del evento"));
                              } else {
                                //Crea un circularEvent

                                _newCircularEvent = buildCircularEvent();

                                setState(() {
                                  this.isLoading = true;
                                });


                                if(_operation == "CREATE") {
                                  _displaySnackBar("Creando envento, espere ...");

                                  bloc.createEvent(_userUid, _username, _userPass,
                                      _userType, _newCircularEvent);

                                  bloc.subject.stream.listen((value) {
                                    if (value != null) {
                                      if (value["status"] != null) {
                                        if (value["status"] == true) {
                                          Scaffold.of(context).showSnackBar(
                                              _displaySnackBar(
                                                  "Evento creado con éxito"));

                                          Navigator.popUntil(context, ModalRoute.withName("/home"));
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) => UserEventsPage()),
                                          );

                                        } else {
                                          Scaffold.of(context).showSnackBar(
                                              _displaySnackBar("Ha ocurrido un error"));
                                        }
                                      }
                                    } else {
                                      Scaffold.of(context).showSnackBar(
                                          _displaySnackBar(
                                              "Se ha producido un error: no se ha podido crear el evento, intentelo mas tarde o crealo desde nuestra versión web en http://circular.unal.edu.co"));
                                    }
                                  }


                                  );
                                }else if(_operation == "EDIT"){
                                  _displaySnackBar("Guardando cambios, espere ...");

                                  blocEdit.updateEvent(_userUid, _username, _userPass,
                                      _userType, _newCircularEvent);

                                  blocEdit.subject.stream.listen((value) {


                                    if (value != null) {
                                      if (value["status"] != null) {
                                        if (value["status"] == true) {
                                          Scaffold.of(context).showSnackBar(
                                              _displaySnackBar(
                                                  "Cambios guardados con éxito"));

                                          Navigator.pop(context);
                                          Navigator.pop(context);

                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) => UserEventsPage()),
                                          );

                                        } else {
                                          Scaffold.of(context).showSnackBar(
                                              _displaySnackBar("Ha ocurrido un error"));
                                        }
                                      }
                                    } else {
                                      Scaffold.of(context).showSnackBar(_displaySnackBar(
                                          "Se ha producido un error: no se han podido guardar cambios del evento, intentelo mas tarde o desde nuestra versión web en http://circular.unal.edu.co"));
                                    }
                                  });
                                }
                              }
                            },
                            child: Text(
                              "Guardar",
                              style: TextStyle(color: Colors.black),
                            ),
                          ),
                        )
                      ],
                    ),
                  )
              )
          ),

          if(isLoading)
            Container(
              color: Color(0xff636363),
              child:
              LoadingAnimation()

            ),
          if(isLoading)
            Positioned.fill(
              bottom: 2.0,
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Text("Guardando, espere ...")
              ),
            )


        ]




    );

  }

  //Crear el CircularEvent a ser enviado
  CircularEventModel buildCircularEvent() {
    CircularEventModel newEvent = CircularEventModel();

    if(_operation == "EDIT"){
      newEvent.uid = _circularEvent.uid;
    }

    newEvent.title = _title;
    newEvent.description = _description;
    newEvent.dependenciaUid = dependenceUidSelected;
    newEvent.areaUid = areaUidSelected;
    newEvent.typeUid = typeUidSelected;
    newEvent.specialUid = specialUidSelected;
    newEvent.invitados = _guest;
    newEvent.numInstitutionalImage = _numInstitucionalImage;
    newEvent.keywords = _keywords;
    newEvent.ticketsType = _typeTyckets;
    newEvent.ticketsUrl = _tycketsUrl;
    newEvent.ticketsCost = _tycketsCost;
    newEvent.imgBase64 = eventImage64;
    newEvent.imageCredits = _imageCredits;
    newEvent.inscriptionDateInitial = _inscriptionDateInitial;
    newEvent.inscriptionDateFinal = _inscriptionDateFinal;
    newEvent.inscriptionWay = _inscriptionWay;
    //Se deja la ubicacion de la universidad nacional de colombia, sede bogota como ubiccion por defecto
    _newCircularLocation.latitude = 4.633766;
    _newCircularLocation.longitude = -74.084553;
    _newCircularLocation.address = _address;
    newEvent.circularLocation = _newCircularLocation;

    //Responsable del evento

    OrganizerModel organizer = new OrganizerModel();

    organizer.firstName = _responsableFirstName;
    organizer.lastName = _responsableLastName;
    organizer.position = _responsablePosition;
    organizer.institutionalEmail = _responsableInstitutionalEmail;
    organizer.phone = _responsablePhone;
    organizer.extension = _responsableExtension;
    organizer.uidDependence = _responsableDependence;

    newEvent.eventDates = _eventDatesList; //Se agregan fechas del evento
    newEvent.organizer = organizer; // Se agrega el responsable del evento

    return newEvent;
  }

  Widget _displaySnackBar(String msg) {
    return SnackBar(
        backgroundColor: Color(0xffffcc5c),
        content: Text(
          msg,
          style: TextStyle(color: Colors.black),
        ));
  }

  bool validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    return (!regex.hasMatch(value)) ? false : true;
  }

  bool validateUrl(String value) {
    Pattern pattern = r'(((https?|http)://?)|(www.?))';
    RegExp regex = new RegExp(pattern);
    return (!regex.hasMatch(value)) ? false : true;
  }

  //here goes the function
  String _parseHtmlString(String htmlString) {
    final document = parse(htmlString);
    final String parsedString = parse(document.body.text).documentElement.text;

    return parsedString;
  }
}
