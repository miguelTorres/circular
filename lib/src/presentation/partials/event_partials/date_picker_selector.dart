import 'dart:io';

import 'package:circular/src/data/models/date_time_range.dart';
import 'package:flutter/material.dart';
import 'package:date_range_picker/date_range_picker.dart' as DateRagePicker;
import 'package:flutter/rendering.dart';
import 'package:intl/intl.dart';

class DatePickerSelecetor extends StatefulWidget {
  List<DateTimeRange> _datesList = new List();

  DatePickerSelecetor(this._datesList);

  @override
  _DatePickerSelecetorState createState() =>
      _DatePickerSelecetorState(_datesList);
}

class _DatePickerSelecetorState extends State<DatePickerSelecetor> {
  List<DateTimeRange> _eventDatesList = new List();
  final _formKey = GlobalKey<FormState>();
  bool hasDates = false;
  List<DateTime> picked;

  String _tempTitle;
  String _tempDescription;
  TimeOfDay _tempInitialTime;
  TimeOfDay _tempFinalTime;

  TextEditingController _initialTimeController;
  TextEditingController _finalTimeController;

  _DatePickerSelecetorState(eventDatesList) {
    this._eventDatesList = eventDatesList;
    if (this._eventDatesList.length > 0) {
      this.hasDates = true;
    }
  }

  @override
  void initState() {
    _tempInitialTime = TimeOfDay(hour: 08, minute: 00);
    _tempFinalTime = TimeOfDay(hour: 13, minute: 00);

    _initialTimeController = TextEditingController();
    _finalTimeController = TextEditingController();

    String tempHinitial = (_tempInitialTime.hour < 10)
        ? "0${_tempInitialTime.hour}"
        : "${_tempInitialTime.hour}";
    String tempMinitial = (_tempInitialTime.minute < 10)
        ? "0${_tempInitialTime.minute}"
        : "${_tempInitialTime.minute}";

    _initialTimeController.text = "${tempHinitial}:${tempMinitial}";

    String tempHfinal = (_tempFinalTime.hour < 10)
        ? "0${_tempFinalTime.hour}"
        : "${_tempFinalTime.hour}";
    String tempMfinal = (_tempFinalTime.minute < 10)
        ? "0${_tempFinalTime.minute}"
        : "${_tempFinalTime.minute}";

    _finalTimeController.text = "${tempHfinal}:${tempMfinal}";

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.arrow_back_ios),
              onPressed: () {
                Navigator.pop(context);
              },
            );
          },
        ),
        centerTitle: true,
        title: Container(
          height: 23,
          child:
              Image.asset('assets/images/logoCircular.png', fit: BoxFit.cover),
        ),
      ),
      body: buildBody(),
      floatingActionButton: FloatingActionButton.extended(
        heroTag: "btnCreateEvent",
        icon: Icon(Icons.add),
        label: Text('Agregar fechas'),
        onPressed: () {
          addDatesOnPressed();
        },
      ),
      bottomNavigationBar: Container(
        height: 60.0,
        child: RaisedButton(
          color: Color(0xffffcc5c),
          child: Text(
            "GUARDAR",
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
          ),
          onPressed: () {
            //Retorna el listado de fechas seleccionadas(DateRange)
            Navigator.pop(context, _eventDatesList);
          },
        ),
      ),
    );
  }

  Widget getItemDateList(DateTimeRange range, int index) {
    String formattedDateInitial =
        DateFormat('E d/MMM/yyyy - HH:mm').format(range.start);

    String formattedDateFinal =
        DateFormat('E d/MMM/yyyy - HH:mm').format(range.end);

    return DefaultTextStyle(
        style: TextStyle(color: Colors.black),
        child: Container(
            margin: EdgeInsets.only(top: 20.0, left: 20.0, right: 20.0),
            padding: EdgeInsets.all(15.0),
            decoration: BoxDecoration(
                color: Colors.white,
                boxShadow:[ BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 3,
                    blurRadius: 7
                  //offset: Offset(0,3)
                )]
            ),            child: Stack(children: <Widget>[
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("Inicio: $formattedDateInitial"),
                    Text("Fin: $formattedDateFinal"),
                    (range.title != null)
                        ? Text("Título: ${range.title}")
                        : Text(""),
                    (range.description != null)
                        ? Text("Descripción: ${range.description}")
                        : Text(""),
                  ],
                ),
              ),
              Positioned(
                top: 3.0,
                right: 3.0,
                child: IconButton(
                    color: Colors.black,
                    icon: Icon(Icons.clear),
                    onPressed: () {
                      setState(() {
                        _eventDatesList.removeAt(index);
                        if (_eventDatesList.isEmpty) {
                          setState(() {
                            hasDates = false;
                          });
                        }
                      });
                    }),
              )
            ])));
  }

  List<DateTime> generateDatesFromRange(DateTime startDate, DateTime stopDate) {
    List<DateTime> list = [];

    DateTime currentDate = startDate;
    list.add(currentDate);

    while (
        currentDate.millisecondsSinceEpoch < stopDate.millisecondsSinceEpoch) {
      currentDate = new DateTime(
          currentDate.year, currentDate.month, currentDate.day + 1);
      list.add(currentDate);
    }

    return list;
  }

  Future<Null> selectInitialTime(BuildContext context) async {
    await showTimePicker(context: context, initialTime: _tempInitialTime)
        .then((value) {
      setState(() {
        _tempInitialTime = value;

        String tempH = (_tempInitialTime.hour < 10)
            ? "0${_tempInitialTime.hour}"
            : "${_tempInitialTime.hour}";
        String tempM = (_tempInitialTime.minute < 10)
            ? "0${_tempInitialTime.minute}"
            : "${_tempInitialTime.minute}";

        _initialTimeController.text = "${tempH}:${tempM}";
      });
    });
  }

  Future<Null> selectFinalTime(BuildContext context) async {
    await showTimePicker(context: context, initialTime: _tempFinalTime)
        .then((value) {
      setState(() {
        _tempFinalTime = value;

        String tempH = (_tempFinalTime.hour < 10)
            ? "0${_tempFinalTime.hour}"
            : "${_tempFinalTime.hour}";
        String tempM = (_tempFinalTime.minute < 10)
            ? "0${_tempFinalTime.minute}"
            : "${_tempFinalTime.minute}";

        _finalTimeController.text = "${tempH}:${tempM}";
      });
    });
  }

  Widget getForm() {
    return Form(
      key: _formKey,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[

          Padding(

              padding: EdgeInsets.all(8.0),
              child: Row(
                children: <Widget>[

                  Flexible(
                    child: Container(
                      margin: EdgeInsets.only(left: 5.0, right: 10.0),
                      child: Icon(Icons.alarm),
                    ),
                  ),
                  Flexible(
                    child: TextFormField(
                      controller: _initialTimeController,
                      readOnly: true,
                      decoration: InputDecoration(
                        labelText: "Hora inicial *",
                        labelStyle: TextStyle(color: Color(0xffffcc5c)),
                      ),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Ingrese un hora';
                        }

                        if ((_tempInitialTime.hour +
                                (_tempInitialTime.minute / 60.0)) >
                            (_tempFinalTime.hour +
                                (_tempFinalTime.minute / 60.0))) {
                          return "Hora inicial es mayor";
                        }
                      },
                      onTap: () async {
                        await selectInitialTime(context);
                      },
                    ),
                  )
                ],
              )),
          Padding(
              padding: EdgeInsets.all(8.0),
              child: Row(
                children: <Widget>[
                  Flexible(
                    child: Container(
                      margin: EdgeInsets.only(left: 5.0, right: 10.0),
                      child: Icon(Icons.alarm),
                    ),
                  ),
                  Flexible(
                    child: TextFormField(
                      controller: _finalTimeController,
                      readOnly: true,
                      decoration: InputDecoration(
                        labelText: "Hora final *",
                        labelStyle: TextStyle(color: Color(0xffffcc5c)),
                      ),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Ingrese una hora';
                        }

                        if ((_tempInitialTime.hour +
                                (_tempInitialTime.minute / 60.0)) >
                            (_tempFinalTime.hour +
                                (_tempFinalTime.minute / 60.0))) {
                          return "Hora final es menor";
                        }
                      },
                      onTap: () async {
                        await selectFinalTime(context);
                      },
                    ),
                  )
                ],
              )),
          Padding(
            padding: EdgeInsets.all(8.0),
            child: TextFormField(
              decoration: InputDecoration(
                  labelText: "Título (opcional)",
                  labelStyle: TextStyle(color: Color(0xffffcc5c))),
              onChanged: (value) {
                _tempTitle = value;
              },
            ),
          ),
          Padding(
            padding: EdgeInsets.all(8.0),
            child: TextFormField(
              decoration: InputDecoration(
                  labelText: "Descripción (opcional)",
                  labelStyle: TextStyle(color: Color(0xffffcc5c))),
              onChanged: (value) {
                _tempDescription = value;
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: RaisedButton(
              color: Color(0xffffcc5c),
              child: Text(
                "Guardar",
                style: TextStyle(color: Colors.black),
              ),
              onPressed: () {
                if (_formKey.currentState.validate()) {
                  if (picked.length == 1) {
                    setState(() {
                      final date = picked[0];
                      _eventDatesList.add(DateTimeRange(
                          _tempTitle,
                          _tempDescription,
                          DateTime(
                            date.year,
                            date.month,
                            date.day,
                            _tempInitialTime.hour,
                            _tempFinalTime.minute,
                          ),
                          DateTime(
                            date.year,
                            date.month,
                            date.day,
                            _tempFinalTime.hour,
                            _tempFinalTime.minute,
                          )));
                      hasDates = true;
                    });
                  }

                  if (picked.length == 2) {
                    List<DateTime> pickedList =
                        generateDatesFromRange(picked[0], picked[1]);

                    pickedList.forEach((date) {
                      setState(() {
                        _eventDatesList.add(DateTimeRange(
                            _tempTitle,
                            _tempDescription,
                            DateTime(
                              date.year,
                              date.month,
                              date.day,
                              _tempInitialTime.hour,
                              _tempInitialTime.minute,
                            ),
                            DateTime(
                              date.year,
                              date.month,
                              date.day,
                              _tempFinalTime.hour,
                              _tempFinalTime.minute,
                            )));

                        hasDates = true;
                      });
                    });
                  }

                  Navigator.of(context).pop();
                }
              },
            ),
          )
        ],
      ),
    );
  }

  buildBody() {
    if (!hasDates) {
      return Container(
          color: Color(0xff636363),
          child: Center(
              child: Text(
            "Aún no ha agregado fechas",
            style: TextStyle(color: Colors.white),
          )));
    } else {
      return Container(
          color: Color(0xff636363),
          height: MediaQuery.of(context).size.height,
          child: ListView.builder(
            padding: EdgeInsets.only(bottom: 50.0),
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              itemCount: _eventDatesList.length,
              itemBuilder: (BuildContext ctxt, int index) {
                return getItemDateList(_eventDatesList[index], index);
              }));
    }
  }

  Future<void> addDatesOnPressed() async {

    //Abre el selector de Fecha
    picked = await DateRagePicker.showDatePicker(
      locale: const Locale("es", "ES"),



      context: context,
      initialFirstDate: new DateTime.now(),
      initialLastDate: (new DateTime.now()).add(new Duration(days: 7)),
      firstDate: new DateTime(2020),
      lastDate: new DateTime(2050),
    );

    if (picked != null) {
      showDialog(
          context: context,
          builder: (BuildContext context) {

            return AlertDialog(
              content: Stack(
                overflow: Overflow.visible,
                children: <Widget>[
                  Positioned(
                    right: -40.0,
                    top: -40.0,
                    child: InkResponse(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: CircleAvatar(
                        child: Icon(
                          Icons.close,
                          color: Colors.black,
                        ),
                        backgroundColor: Color(0xffffcc5c),
                      ),
                    ),
                  ),
                  SingleChildScrollView(child: getForm()),
                ],
              ),
            );
          });
    }
  }
}

class CustomWeekdayLabelsRow extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Expanded(child: Text("M", textAlign: TextAlign.center)),
        Expanded(child: Text("T", textAlign: TextAlign.center)),
        Expanded(child: Text("W", textAlign: TextAlign.center)),
        Expanded(child: Text("T", textAlign: TextAlign.center)),
        Expanded(child: Text("F", textAlign: TextAlign.center)),
        Expanded(child: Text("S", textAlign: TextAlign.center)),
        Expanded(child: Text("S", textAlign: TextAlign.center)),
      ],
    );
  }
}
