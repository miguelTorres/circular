import 'package:circular/src/business_logic/blocs/dependences_bloc.dart';
import 'package:circular/src/data/models/radio_model.dart';
import 'package:circular/src/presentation/partials/commons/loading_animation.dart';
import 'package:circular/src/presentation/partials/commons/radio_item.dart';
import 'package:flutter/material.dart';

class FormDependencesSelector extends StatefulWidget {
  int dependenceUid = null;

  FormDependencesSelector(this.dependenceUid);

  @override
  _FormDependencesSelectorState createState() =>
      _FormDependencesSelectorState(dependenceUid);
}

class _FormDependencesSelectorState extends State<FormDependencesSelector> {
  final bloc = DependencesBloc();
  int currentIndexSelected = null;
  List<RadioModel> sampleData;
  int dependenceUid = null;
  bool firstTime = true;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  String query;

  _FormDependencesSelectorState(this.dependenceUid);

  @override
  void initState() {
    super.initState();
    //Carga todas las dependencias
    bloc.findDependences("");
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        //backgroundColor: Colors.white,
        appBar: new AppBar(
            automaticallyImplyLeading: false,
            title: Text(
              "Dependencias",
              style: TextStyle(color: Colors.white),
            ),
            backgroundColor: Colors.grey[600],
            actions: <Widget>[
              IconButton(
                color: Colors.white,
                icon: Icon(Icons.clear),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ]),
        body: buildBody());
  }

  Widget buildBody() {
    return Container(
      color: Color(0xff636363),
        height: MediaQuery.of(context).size.height,
        child: Stack(children: <Widget>[
          StreamBuilder(
              stream: bloc.subject.stream,
              builder: (BuildContext context,
                  AsyncSnapshot<List<RadioModel>> snapshot) {
                Widget child;

                if (snapshot.data == null) {
                  child = LoadingAnimation();
                } else if (snapshot.hasData) {
                  if (snapshot.data.length == 0) {
                    child =

                    Center(
                     child:
                        Text("No se encuentran resultados",

                          style: TextStyle(
                            color: Colors.white
                          ),

                        )
                    );
                  } else {
                    child = Container(
                        margin: new EdgeInsets.only(bottom: 50.0, top: 80.0),
                        child: buildList(snapshot));
                  }
                } else if (snapshot.hasError) {
                  child = Container(
                    child: Column(
                      children: <Widget>[
                        Icon(
                          Icons.error_outline,
                          color: Colors.red,
                          size: 60,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 16),
                          child: Text('Error: ${snapshot.error}'),
                        )
                      ],
                    ),
                  );
                } else {
                  child = Center(
                    child: Text("Cargando dependencias ..."),
                  );
                }

                return child;
              }),
          Positioned(
              child: Container(
            margin: EdgeInsets.all(10.0),
            child: TextFormField(
              style: TextStyle(
                //color: Colors.black
              ),
                textInputAction: TextInputAction.search,

              decoration: InputDecoration(

                  suffixIcon: IconButton(
                      icon: Icon(Icons.search),
                      onPressed: () {
                        bloc.subject.sink.add(null);
                        bloc.findDependences(query);
                      }),

                  labelText: 'Escriba aquí',
                //labelStyle: TextStyle(color: Color(0xff939393)),
               /* enabledBorder: new UnderlineInputBorder(
                    borderSide: new BorderSide(
                        color: Color(0xff333333)
                    )
                ),*/

              ),

              onChanged: (value) {
                setState(() {
                  query = value;
                });
              },
                onFieldSubmitted: (term){
                  bloc.subject.sink.add(null);
                  bloc.findDependences(query);
              }
            ),

          )),
          Positioned(
              bottom: -6.0,
              child: Container(
                  height: 50.0,
                  width: MediaQuery.of(context).size.width,
                  child: Row(
                    children: <Widget>[
                      Expanded(
                          child: RaisedButton(
                        onPressed: () {
                          Navigator.pop(
                            context,
                          );
                        },
                        child: const Text('Cancelar',
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold)),
                        color: Color(0xffffcc5c),
                        textColor: Colors.black,
                        elevation: 5,
                      )),
                      Expanded(
                        child: RaisedButton(
                          onPressed: () {
                            if (currentIndexSelected != null) {
                              Navigator.pop(context, {
                                "dependenceUidSelected":
                                    this.sampleData[currentIndexSelected].uid,
                                "dependenceSelectedText":
                                    this.sampleData[currentIndexSelected].text
                              });
                            }
                          },
                          child: const Text('Guardar',
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.bold)),
                          color: (currentIndexSelected != null)
                              ? Color(0xffffcc5c)
                              : Color(0x33ffcc5c),
                          textColor: Colors.black,
                          elevation: 5,
                        ),
                      ),
                    ],
                  )))
        ]));
  }

  Widget buildList(AsyncSnapshot<List<RadioModel>> snapshot) {
    this.sampleData = snapshot.data;

    //Si llega una dependencia por primera vez, la activa
    return new ListView.builder(
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      itemCount: sampleData.length,
      itemBuilder: (BuildContext context, int index) {
        if (this.dependenceUid != null) {
          if (sampleData[index].uid == this.dependenceUid) {
            if (firstTime) {
              sampleData[index].isSelected = true;
              currentIndexSelected = index;

              firstTime = false;
            }
          }
        }

        return new InkWell(
            splashColor: Color(0xffffcc5c),
            onTap: () {
              setState(() {
                if (currentIndexSelected != null)
                  sampleData[currentIndexSelected].isSelected = false;
                sampleData[index].isSelected = true;
                currentIndexSelected = index;
              });
            },
            child: getListItem(sampleData[index]));
      },
    );
  }

  Widget getListItem(RadioModel item) {
    Widget temp;

    temp = new RadioItem(item);

    return temp;
  }
}
