import 'package:circular/src/business_logic/blocs/categories_bloc.dart';
import 'package:circular/src/data/models/radio_model.dart';
import 'package:circular/src/presentation/partials/commons/radio_item.dart';
import 'package:flutter/material.dart';

class FormCategorySelector extends StatefulWidget {
  String type = "";
  int categoryUid = null;

  FormCategorySelector(this.type, this.categoryUid);

  @override
  _FormCategorySelectorState createState() =>
      _FormCategorySelectorState(this.type, this.categoryUid);
}

class _FormCategorySelectorState extends State<FormCategorySelector> {
  final bloc = CategoriesBloc();
  int currentIndexSelected = null;
  String type = "";
  List<RadioModel> sampleData;
  int categoryUid = null;
  bool firstTime = true;
  _FormCategorySelectorState(this.type, this.categoryUid);

  @override
  void initState() {
    super.initState();
    bloc.findCategories(type);
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //backgroundColor: Colors.white,
      appBar: new AppBar(
          automaticallyImplyLeading: false,
          title: Text(
            "${this.type[0].toUpperCase()}${this.type.substring(1)}",
            style: TextStyle(color: Colors.white),
          ),
          backgroundColor: Colors.grey[600],
          actions: <Widget>[
            IconButton(
              color: Colors.white,
              icon: Icon(Icons.clear),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ]),
      body: Container(
        child: StreamBuilder(
            stream: bloc.subject.stream,
            builder: (BuildContext context,
                AsyncSnapshot<List<RadioModel>> snapshot) {

              Widget child;

              if (snapshot.hasData) {
                child = Stack(children: <Widget>[
                  Container(
                      margin: EdgeInsets.only(left: 15.0, top: 30.0, bottom: 50.0),
                      child: buildList(snapshot)),
                  Positioned(
                      bottom: -6.0,
                      child: Container(
                          height: 50.0,
                          width: MediaQuery.of(context).size.width,
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                  child: RaisedButton(
                                onPressed: () {
                                  Navigator.pop(
                                    context,
                                  );
                                },
                                child: const Text('Cancelar',
                                    style: TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold)),
                                color: Color(0xffffcc5c),
                                textColor: Colors.black,
                                elevation: 5,
                              )),
                              Expanded(
                                child: RaisedButton(

                                  onPressed: () {
                                    if (currentIndexSelected != null) {
                                      Navigator.pop(context, {
                                        "categoryUidSelected": this
                                            .sampleData[currentIndexSelected]
                                            .uid,
                                        "categorySelectedText": this
                                            .sampleData[currentIndexSelected]
                                            .text
                                      });
                                    }
                                  },

                                  child: const Text('Guardar',
                                      style: TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold)),
                                  color: (currentIndexSelected != null)
                                      ? Color(0xffffcc5c)
                                      : Color(0x33ffcc5c),
                                  textColor: Colors.black,
                                  elevation: 5,
                                ),
                              ),
                            ],
                          )))
                ]);
              } else if (snapshot.hasError) {
                child = Container(
                  child: Column(
                    children: <Widget>[
                      Icon(
                        Icons.error_outline,
                        color: Colors.red,
                        size: 60,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 16),
                        child: Text('Error: ${snapshot.error}'),
                      )
                    ],
                  ),
                );
              } else {

                child = Container(
                  //child: Text("en progreso..."),
                );

              }

              return child;
            }),
      ),
    );
  }

  Widget buildList(AsyncSnapshot<List<RadioModel>> snapshot) {
    sampleData = snapshot.data;


    return new ListView.builder(
      itemCount: sampleData.length,
      itemBuilder: (BuildContext context, int index) {


        //Si llega una categoria por primera vez, la activa

          if (this.categoryUid != null) {
            if (sampleData[index].uid == this.categoryUid) {

              if(firstTime) {
                sampleData[index].isSelected = true;
                currentIndexSelected = index;

                firstTime = false;

              }
            }

        }

        return new InkWell(
            splashColor: Color(0xffffcc5c),
            onTap: () {
              setState(() {

                if (currentIndexSelected != null) {
                  sampleData[currentIndexSelected].isSelected = false;
                }
                sampleData[index].isSelected = true;
                currentIndexSelected = index;

              });
            },
            child: getListItem(sampleData[index], index)
        );
      },
    );
  }

  Widget getListItem(RadioModel item, int index) {
    Widget temp;

    temp = new RadioItem(item);
    return temp;
  }
}
