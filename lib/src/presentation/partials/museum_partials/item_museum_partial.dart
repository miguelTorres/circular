import 'dart:convert';

import 'package:circular/src/data/models/circular_event_model.dart';
import 'package:circular/src/data/models/museum_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';  //for date locale


class ItemMuseumPartial extends StatelessWidget {
  MuseumModel museum;
  String _dateBarPosition = "BOTTOM" ;

  ItemMuseumPartial(this.museum, this._dateBarPosition);


  @override
  Widget build(BuildContext context) {
    Intl.defaultLocale = 'es_ES';


    return Container(
      margin: EdgeInsets.only(left: 3.0, right: 3.0),
      child: Stack(
        children: <Widget>[

          Image.memory(
            base64Decode(museum.imgBase64),
            fit: BoxFit.cover,
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            alignment: Alignment.center,
          ),
          Positioned(
            top: 0.0,
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[

                  if(_dateBarPosition=="TOP")
                        Container(
                            width: MediaQuery.of(context).size.width,
                            color: Color(0xff666666),
                            child: Row(children: <Widget>[
                              Container(
                                width: 50.0,
                                child: Text(""),
                              ),
                              
                              
                            ]))
                  ,
                  Container(
                    width: MediaQuery.of(context).size.width,

                    color: Color(0x00313130).withOpacity(0.95),
                    child:

                    Padding(
                      padding: EdgeInsets.only(left: 5.0, right: 5.0),
                    child:
                    Text(
                      "${museum.campus}",
                      style: TextStyle(color: Colors.white),
                    ),
                    )


                  ),
                  Flexible(
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      color: Color(0x00313130).withOpacity(0.95),
                      child:

                      Padding(
                        padding: EdgeInsets.only(left: 5.0, right: 5.0),
                        child:
                      Text(
                        "${museum.name}",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                      )


                    ),
                  ),
                  Container(
                    color: Colors.white.withOpacity(0.0),
                    child: ClipPath(
                      clipper: TrapeziumClipper(),
                      child: Container(
                        color: Color(0x00313130).withOpacity(0.95),
                        padding: EdgeInsets.all(8.0),
                        width: MediaQuery.of(context).size.width * 3 / 5,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            ConstrainedBox(
                              constraints: BoxConstraints(
                                  maxWidth: MediaQuery.of(context).size.width *
                                      6 /
                                      15),
                              child: Text(''),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          if(_dateBarPosition=="BOTTOM")
          Positioned(
              bottom: 0.0,
              child: Container(
                  width: MediaQuery.of(context).size.width,
                  color: Color(0xff666666),
                  child: Row(children: <Widget>[
                    Container(
                      width: 50.0,
                      child: Text(""),
                    ),
                  
                  ])))
        ],
      ),
    );
  }
  
}

class TrapeziumClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = Path();
    path.lineTo(0.0, 0.0);
    path.lineTo(0.0, size.height);
    path.lineTo(size.width, 0.0);

    path.close();

    return path;
  }

  @override
  bool shouldReclip(TrapeziumClipper oldClipper) => false;
}
